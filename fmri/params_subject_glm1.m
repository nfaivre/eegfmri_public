%-----------------------------------------------------------------------
% Job saved on 29-May-2017 13:23:41 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6685)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
function [matlabbatch] = params_subject_glm1(params)

fprintf('params.outdir is %s\n',params.outdir);
matlabbatch{1}.spm.stats.fmri_spec.dir = {params.outdir};
matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = params.tr;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = params.microtime;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = params.microtime/2;
%%
%% get functional images for each session
%
niftidir = [params.dicomdir '/' params.subject '/ses-001/func_preproc/' ...
    params.subject '_ses-001_task-main_run-00*.nii'];
fprintf('Looking for Nifti files in %s\n',niftidir);
d1 = dir(niftidir);

irun = 0;
for f1=1:length(d1)
    file = [d1(f1).folder '/' d1(f1).name];
    dir_orig = d1(f1).folder(1:end-8);
    % count the number of volumes
    hdr = spm_data_hdr_read(file);
    
    split = strsplit(d1(f1).name,'_run-00');
    run = str2double(split{2}(1));
    
    fprintf('[run %d]',run);

        irun = irun+1;
        % preallocate
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).scans = cell(length(hdr),1);
        
        % add files
        for f2 = 1:min(length(hdr),387)
            matlabbatch{1}.spm.stats.fmri_spec.sess(irun).scans{f2} = [file ',' num2str(f2) ];
        end
        
        %% create events
        d3 = dir([dir_orig '/' params.subject '_ses-001_task-main_run-00' num2str(run) '_events.tsv']);
        eventfile = [d3(1).folder '/' d3(1).name];
        copyfile(eventfile,[eventfile(1:end-3) '.csv']);
        events = readtable([eventfile(1:end-3) '.csv']);
        delete([eventfile(1:end-3) '.csv']);
        
        name = unique(events.trial_type);
         
        nreg = length(name);
        durations = cell(1,nreg);
        onsets = cell(1,nreg);
        
        act_trials = strcmp(events.trial_type,'act');
        names = {'act','obs'};
        onsets{1} = events.onset(act_trials);
        durations{1} = events.duration(act_trials);
        pmod = [];
        pmod(1).name{1} = 'conf-act';
        pmod(1).param{1} = events.confidence(act_trials);
        pmod(1).poly{1} = 1;
        pmod(1).name{2} = 'rt1-act';
        pmod(1).param{2} = events.response_time(act_trials);
        pmod(1).poly{2} = 1;
        pmod(1).name{3} = 'dots-act';
        pmod(1).param{3} = events.dots_difference(act_trials);
        pmod(1).poly{3} = 1;
        
        obs_trials = strcmp(events.trial_type,'obs');
        onsets{2} = events.onset(obs_trials);
        durations{2} = events.duration(obs_trials);
        pmod(2).name{1} = 'conf-act';
        pmod(2).param{1} = events.confidence(obs_trials);
        pmod(2).poly{1} = 1;
        pmod(2).name{2} = 'rt1-act';
        pmod(2).param{2} = events.response_time(obs_trials);
        pmod(2).poly{2} = 1;
        pmod(2).name{3} = 'dots-act';
        pmod(2).param{3} = events.dots_difference(obs_trials);
        pmod(2).poly{3} = 1;
       
        badact_trials = strcmp(events.trial_type,'act-bad');
        if sum(badact_trials) > 0
            names{3} = 'act-bad';
            onsets{3} =  events.onset(badact_trials);
            durations{3} = events.duration(badact_trials);
       end
         
        badobs_trials = strcmp(events.trial_type,'obs-bad');
        if sum(badobs_trials) > 0
             names{nreg} = 'act-bad';
            onsets{nreg} =  events.onset(badobs_trials);
            durations{nreg} = events.duration(badobs_trials);
                  end
        condfile = [params.eventdir '/' params.subject '_ses-001_task-main_run-00' num2str(run) '_events.mat'];
        save(condfile,'names','onsets','durations','pmod');
       
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).multi = {condfile};
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).regress = struct( ...
            'name', {}, 'val', {});

        d4 = dir([dir_orig '/' params.subject '_ses-001_task-main_run-00' num2str(run) '_headmove.txt']);
        filereg = [d4(1).folder '/' d4(1).name];
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).multi_reg = {filereg};
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).hpf = 128;        
        
end
fprintf('\n');

matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = params.deriv;
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
matlabbatch{1}.spm.stats.fmri_spec.mask = {''};
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';

matlabbatch{2}.spm.stats.review.spmmat(1) = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.review.display.matrix = 1;
matlabbatch{2}.spm.stats.review.print = 'ps';
matlabbatch{3}.spm.stats.fmri_est.spmmat(1) = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{3}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{3}.spm.stats.fmri_est.method.Classical = 1;
matlabbatch{4}.spm.stats.con.spmmat(1) = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));

%%
w = cell(1,13);
contrasts = cell(1,13);
%      mot c rt mon c rt
w{1} = [1  0  0  0  0  0  0  0 ]; contrasts{1} = 'act > 0';
w{2} = [0  0  0  0  1  0  0  0 ]; contrasts{2} = 'obs > 0';
w{3} = [1  0  0  0 -1  0  0  0 ]; contrasts{3} = 'act > obs';
w{4} = [-1 0  0  0  1  0  0  0 ]; contrasts{4} = 'obs > act';
w{5} = [0  1  0  0  0  0  0  0 ]; contrasts{5} = 'conf act > 0';
w{6} = [0  0  0  0  0  1  0  0 ]; contrasts{6} = 'conf obs > 0';
w{7} = [0  0  1  0  0  0  0  0 ]; contrasts{7} = 'rt act > 0';
w{8} = [0  0  0  0  0  0  1  0 ]; contrasts{8} = 'rt obs > 0';
w{9} = [0  0  0  1  0  0  0  0 ]; contrasts{9} = 'dots act > 0';
w{10} =[0  0  0  0  0  0  0  1 ]; contrasts{10} = 'dots obs > 0';
w{11} =[0  1  0  0  0 -1  0  0 ]; contrasts{11} = 'conf act > obs';
w{12} =[0  0  1  0  0  0 -1  0 ]; contrasts{12} = 'rt act > obs';
w{13} =[0  0  0  1  0  0  0  -1 ]; contrasts{13} = 'dots act > obs';

for i=1:length(w)
    ctype{i} = 'tcon';
end
for c = 1:length(w)
    weights = upsample(w{c}.',length(params.deriv)+1).';
    matlabbatch{4}.spm.stats.con.consess{c}.(ctype{c}).weights = weights;
    matlabbatch{4}.spm.stats.con.consess{c}.(ctype{c}).sessrep = 'replsc';
    matlabbatch{4}.spm.stats.con.consess{c}.(ctype{c}).name = contrasts{c};
end
matlabbatch{4}.spm.stats.con.delete = 1;
