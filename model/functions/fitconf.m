%========================================================
%   fitconf.m fits mapping from accumulated evidence to observed confidence
%   ratings
%
%
%   input arguments
%       - dv, resp: the output from evacc.m
%       - params: the set of parameters
%       - opts: the set of config
%       - resp_covert: (optional) 
%
%   Pereira, Faivre, Iturrate et al., 2019, 
%   Disentangling the origins of confidence in speeded 
%      perceptual judgments through multimodal imaging
%
%   preregistration: https://osf.io/a5qmv/  
%   gitlab: https://gitlab.com/meta-eegfmri/  
%      
%   Michael Pereira <michael.pereira@epfl.ch>
%   25/09/2018
%========================================================
function [ params,stat,out] = fitconf( rt,resp,conf, simrt, simresp, simdv,  params, opts, resp_covert)

if nargin < 9
    resp_covert = [];
end
out = [];

% remove bad trials and censor
bad = isnan(resp) | rt < opts.cens2(1) | rt > opts.cens2(2) | conf == -1;
resp(bad) = [];
rt(bad) = [];
conf(bad) = []; 
       
% invert r
rt(resp==0) = -rt(resp==0);
conf(resp==0) = -conf(resp==0);
simrt(simresp==0) = -simrt(simresp==0);



%% Find which parameters to fit
fields = {'confslope','confbias','conftime'};
p = 0;
param_val = [];
param_id = {};
for f=1:length(fields)
    if params.(fields{f}).fit
        p = p+1;
        param_val(p) = params.(fields{f}).itrans(params.(fields{f}).val);
        param_id{p} = fields{f};
    end
end

%% Fitting procedure
options = optimset('Display',opts.optimizationdisplay2,'FinDiffRelStep',1e-2);
[p, stat] = fminsearch(@fitfunc,param_val,options);

%% Transform parameters back
for f=1:length(param_id)
    params.(param_id{f}).val = params.(param_id{f}).trans(p(f));
end

    function stat = fitfunc(p)
        
        % Reset random number generator
        rng('default');
        newparams = params;
        
        % transform parameters back to usuable values       
        for f=1:length(param_id)
            newparams.(param_id{f}).val = newparams.(param_id{f}).trans(p(f));
        end
    
        % get transform d.v. to confience
        simconf = dv2conf( simdv,simresp, newparams, opts,resp_covert);

        % invert conf for errors
        simconf(simresp==0) = -simconf(simresp==0);

       
        % compute log-likelihood
        [~,ks] = kstest2(simconf,conf);
        stat = -log(ks);
        
        % save stats
        out.ll = stat;
        out.negllconf = -log(ks);
        out.params = params;
        
        
        %% plot some stuff
        if opts.doplot
            
            figure(1); clf; hold on
         
            [k,s] = ksdensity(-conf(resp==0));
            plot(s,nanmean(resp==0).*k,'r');
            [k,s] = ksdensity(conf(resp==1));
            plot(s,nanmean(resp==1).*k,'g');
            if sum(simresp==0) && ~all(isnan(simrt(simresp==0)))
                [k,s] = ksdensity(-simconf(simresp==0));
                plot(s,nanmean(simresp==0).*k,'r--');
            end
            if sum(simresp==1) && ~all(isnan(simrt(simresp==1)))
                [k,s] = ksdensity(simconf(simresp==1));
                plot(s,nanmean(simresp==1).*k,'g--');
            end
            xlim([0,1]);
            xlabel('Confidence (early RT) [s]');
            ylabel('Probability');
            grid on
            title(sprintf('%s: ll=%.2f, x=%.2f, b=%.2f, t=%.3f',opts.confmodel,out.negllconf,newparams.confslope.val,newparams.confbias.val,newparams.conftime.val));
            
            
            pause(0.01);
        end
    end

end

