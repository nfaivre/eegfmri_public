%========================================================
%   dvconf.m computes the confidence readout and maps to [0,1]
%
%
%   input arguments
%       - dv, resp: the output from evacc.m
%       - params: the set of parameters
%       - opts: the set of config
%       - resp_covert: (optional) 
%
%   Pereira, Faivre, Iturrate et al., 2019, 
%   Disentangling the origins of confidence in speeded 
%      perceptual judgments through multimodal imaging
%
%   preregistration: https://osf.io/a5qmv/  
%   gitlab: https://gitlab.com/meta-eegfmri/  
%      
%   Michael Pereira <michael.pereira@epfl.ch>
%   25/09/2018
%========================================================
function [ conf, dvconf ] = dv2conf( dv,resp, params, opts,  resp_covert)

if nargin < 5
    resp_covert = nan(size(resp));
end
w = [];
% linear interpolation in case of non-integer time
dv_interp = interpdv( dv, params.conftime.val, opts);

% prepare
dvconf = zeros(length(resp),1);

% define wining and loosing accumulators depeding on the first-order choice
dvconf_win(resp==0,:) = dv_interp(resp==0,:,1);
dvconf_win(resp==1,:) = dv_interp(resp==1,:,2);
dvconf_loose(resp==0,:) = dv_interp(resp==0,:,2);
dvconf_loose(resp==1,:) = dv_interp(resp==1,:,1);

% some models rely on a second-order decision occurring at the readout time
% we model this decision as which accumulator has the highest value
resp_2ndorder = diff(dv_interp,1,3) > 0;

if strcmp(opts.confmodel,'win')
    % confidence is readout from the accumulator corresponding to the first
    % order choice
    dvconf = dvconf_win;
elseif strcmp(opts.confmodel,'win-1st')
    % confidence is readout from the accumulator with highest value at the
    % time of readout
    
    dvconf(resp_covert==0,:) = dv_interp(resp_covert==0,:,1);
    dvconf(resp_covert==1,:) = dv_interp(resp_covert==1,:,2);
    w = 2*(resp_covert == resp)-1;

elseif strcmp(opts.confmodel,'win-2nd')
    % confidence is readout from the accumulator with highest value at the
    % time of readout
    resp_2ndorder = diff(dv_interp,1,3) > 0;
    
    dvconf(resp_2ndorder==0,:) = dv_interp(resp_2ndorder==0,:,1);
    dvconf(resp_2ndorder==1,:) = dv_interp(resp_2ndorder==1,:,2);
    w = 2*(resp_2ndorder == resp)-1;
elseif strcmp(opts.confmodel,'win_loose')
    % confidence is readout from the accumulator with highest value at the
    % time of readout
    chmind = (dvconf_loose > params.bound.val) & (dvconf_win < params.bound.val);

    dvconf(chmind==0,:) = dvconf_win(chmind==0,:);
    dvconf(chmind==1,:) = dvconf_loose(chmind==1,:);
    w = 2*(~chmind)-1;
    
elseif strcmp(opts.confmodel,'loose')
    % confidence is readout from the loosing accumulator (we invert the sign so
    % that low evidence for the opposite choice becomes high confidence in
    % the other choice and vice-versa
    dvconf = -dvconf_loose;
elseif strcmp(opts.confmodel,'balance')
    % confidence is readout from the "balance of evidence", i.e. the
    % difference between the winning and loosing accumulators
    dvconf = bsxfun(@times, 2*resp-1, diff(dv_interp,1,3));
end

% after we define the readout, we pass the results through a sigmoid and
% invert confidence for w==-1 (win_2nd models)
conf = sigmoid(dvconf,params.confslope.val, params.confbias.val, w);

end

