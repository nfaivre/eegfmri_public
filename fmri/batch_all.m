function batch_all(batch,maxbatch)
config;
if nargin == 0 || batch==0
    subjects = opts.subjects;
    batch = 0;
    fprintf('Launching batch all, subjects ');

else
    nsuj = length(opts.subjects);
    if nargin == 1
        maxbatch = 4;
    end
    n = ceil(nsuj/maxbatch);
    sel = (batch-1)*n + (1:n);
    sel(sel > nsuj) = [];
    subjects = opts.subjects(sel);
    fprintf('Launching batch %d/%d: subjects ',batch,maxbatch);
end
for s=1:length(subjects)
    fprintf('%s, ',subjects{s});
end
fprintf('\n');

for s=1:length(subjects)
    
    subj = subjects{s};
    batch_subject_glm2(subj);
    
end

