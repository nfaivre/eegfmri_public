%-----------------------------------------------------------------------
% Job saved on 09-Dec-2018 12:17:09 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6685)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
function matlabbatch = level2_glm2_job(opts,type)
sessrep = 'replsc';
if strcmp(type,'glm1')
    basedir = opts.glm1dir;
else
    basedir = opts.glm2dir;
end
nsuj = length(opts.subjects);
outdir = [basedir '/level2/con' opts.con1 opts.con2];
matlabbatch{1}.spm.stats.factorial_design.dir{1} = outdir;
if ~exist(outdir,'dir');
    mkdir(outdir);
end
fprintf('Output dir is %s\n',outdir);
%%
matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(1).scans = cell(nsuj,1);
matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(2).scans = cell(nsuj,1);
matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(3).scans = cell(nsuj,1);
matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(4).scans = cell(nsuj,1);

for s=1:nsuj
    file1 = [basedir '/all/' opts.subjects{s} '_con_' opts.con1 '.nii'];
    file2 = [basedir '/all/' opts.subjects{s} '_con_' opts.con2 '.nii'];
    file3 = [basedir '/all/' opts.subjects{s} '_con_' opts.con3 '.nii'];
    file4 = [basedir '/all/' opts.subjects{s} '_con_' opts.con4 '.nii'];
    matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(1).scans{s} = [file1 ',1'];
    matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(2).scans{s} = [file2 ',1'];
    matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(3).scans{s} = [file3 ',1'];
    matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(4).scans{s} = [file4 ',1'];
    if ~exist(file1,'file')
        fprintf('File not found: %s\n',file1);
    end
     if ~exist(file2,'file')
        fprintf('File not found: %s\n',file2);
     end
     if ~exist(file3,'file')
        fprintf('File not found: %s\n',file3);
     end
     if ~exist(file4,'file')
        fprintf('File not found: %s\n',file4);
    end
end

matlabbatch{1}.spm.stats.factorial_design.des.anova.dept = 0;
matlabbatch{1}.spm.stats.factorial_design.des.anova.variance = 1;
matlabbatch{1}.spm.stats.factorial_design.des.anova.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.anova.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {''};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
matlabbatch{2}.spm.stats.review.spmmat(1) = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.review.display.matrix = 1;
matlabbatch{2}.spm.stats.review.print = 'ps';
matlabbatch{3}.spm.stats.fmri_est.spmmat(1) = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{3}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{3}.spm.stats.fmri_est.method.Classical = 1;
matlabbatch{4}.spm.stats.con.spmmat(1) = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{4}.spm.stats.con.consess{1}.tcon.name = 'eegconf1-act+';
matlabbatch{4}.spm.stats.con.consess{1}.tcon.weights = [1];
matlabbatch{4}.spm.stats.con.consess{1}.tcon.sessrep = sessrep
matlabbatch{4}.spm.stats.con.consess{2}.tcon.name = 'eegconf1-act-';
matlabbatch{4}.spm.stats.con.consess{2}.tcon.weights = [-1];
matlabbatch{4}.spm.stats.con.consess{2}.tcon.sessrep = sessrep
matlabbatch{4}.spm.stats.con.consess{3}.tcon.name = 'eegconf1-obs+';
matlabbatch{4}.spm.stats.con.consess{3}.tcon.weights = [0 1];
matlabbatch{4}.spm.stats.con.consess{3}.tcon.sessrep = sessrep
matlabbatch{4}.spm.stats.con.consess{4}.tcon.name = 'eegconf1-obs-';
matlabbatch{4}.spm.stats.con.consess{4}.tcon.weights = [0 -1];
matlabbatch{4}.spm.stats.con.consess{4}.tcon.sessrep = sessrep
matlabbatch{4}.spm.stats.con.consess{5}.tcon.name = 'eegconf2-act+';
matlabbatch{4}.spm.stats.con.consess{5}.tcon.weights = [0 0 1];
matlabbatch{4}.spm.stats.con.consess{5}.tcon.sessrep = sessrep
matlabbatch{4}.spm.stats.con.consess{6}.tcon.name = 'eegconf2-act-';
matlabbatch{4}.spm.stats.con.consess{6}.tcon.weights = [0 0 -1];
matlabbatch{4}.spm.stats.con.consess{6}.tcon.sessrep = sessrep
matlabbatch{4}.spm.stats.con.consess{7}.tcon.name = 'eegconf2-obs+';
matlabbatch{4}.spm.stats.con.consess{7}.tcon.weights = [0 0 0 1];
matlabbatch{4}.spm.stats.con.consess{7}.tcon.sessrep = sessrep
matlabbatch{4}.spm.stats.con.consess{8}.tcon.name = 'eegconf2-obs-';
matlabbatch{4}.spm.stats.con.consess{8}.tcon.weights = [0 0 0 -1];
matlabbatch{4}.spm.stats.con.consess{8}.tcon.sessrep = sessrep

matlabbatch{4}.spm.stats.con.consess{9}.tcon.name = 'eegconf1-act > eegconf1-obs';
matlabbatch{4}.spm.stats.con.consess{9}.tcon.weights = [1 -1];
matlabbatch{4}.spm.stats.con.consess{9}.tcon.sessrep = sessrep
matlabbatch{4}.spm.stats.con.consess{10}.tcon.name = 'eegconf1-obs > eegconf1-act';
matlabbatch{4}.spm.stats.con.consess{10}.tcon.weights = [-1 1];
matlabbatch{4}.spm.stats.con.consess{10}.tcon.sessrep = sessrep
matlabbatch{4}.spm.stats.con.consess{11}.tcon.name = 'eegconf2-act > eegconf2-obs';
matlabbatch{4}.spm.stats.con.consess{11}.tcon.weights = [0 0 1 -1];
matlabbatch{4}.spm.stats.con.consess{11}.tcon.sessrep = sessrep
matlabbatch{4}.spm.stats.con.consess{12}.tcon.name = 'eegconf2-obs > eegconf2-act';
matlabbatch{4}.spm.stats.con.consess{12}.tcon.weights = [0 0 -1 1];
matlabbatch{4}.spm.stats.con.consess{12}.tcon.sessrep = sessrep
matlabbatch{4}.spm.stats.con.delete = 0;
matlabbatch{5}.spm.stats.results.spmmat(1) = cfg_dep('Contrast Manager: SPM.mat File', substruct('.','val', '{}',{4}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{5}.spm.stats.results.conspec.titlestr = '';
matlabbatch{5}.spm.stats.results.conspec.contrasts = 1;
matlabbatch{5}.spm.stats.results.conspec.threshdesc = 'none';
matlabbatch{5}.spm.stats.results.conspec.thresh = 0.001;
matlabbatch{5}.spm.stats.results.conspec.extent = 0;
matlabbatch{5}.spm.stats.results.conspec.conjunction = 1;
matlabbatch{5}.spm.stats.results.conspec.mask.none = 1;
matlabbatch{5}.spm.stats.results.units = 1;
matlabbatch{5}.spm.stats.results.print = 'ps';
matlabbatch{5}.spm.stats.results.write.none = 1;
