%%%%%%%%%%%%%%%%%%%%%%%
%% Set path and subjects
%%%%%%%%%%%%%%%%%%%%%%%

close all, clear all, clc;

addpath(genpath('./internal'));


data_path = % path to the data

subjects = [2 4 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 24 26];

elecs = {'Fp1','Fp2','F3','F4','C3','C4','P3','P4','O1','O2','F7','F8','T7','T8',...
    'P7','P8','Fz','Cz','Pz','Oz','FC1','FC2','CP1','CP2','FC5','FC6','CP5','CP6',...
    'TP9','TP10','POz','FCz','F1','F2','C1','C2','P1','P2','AF3','AF4','FC3','FC4',...
    'CP3','CP4','PO3','PO4','F5','F6','C5','C6','P5','P6','AF7','AF8','FT7','FT8',...
    'TP7','TP8','PO7','PO8','FT9','FT10','Fpz','CPz'};


%%%%%%%%%%%%%%%%%%%%%%%
%% Set parameters
%%%%%%%%%%%%%%%%%%%%%%%

% Number of permutations to compute
num_permutations = 4;

% 0/1: whether to decode confidence (0) or uncertainty (1)
analyze_uncertainty = 0;

% 0: perform decoding in the channel EEG space without ICA
% 1: perform decoding on the selected ICs
apply_ica = 1;

% 0: do not remove artifactual trials
% 1: remove artifactual trials
remove_bad_trials = 1;

% Apply PCA to the data or not
apply_PCA = 0;


if apply_PCA
    class.applyPCA = 'AfterNorm';
    class.optionsPCA = {'VarianceToRetain', 0.99};
else
    class.optionsPCA = {};
    class.applyPCA = 'NoPCA';
end

% Feature normalization MinMax (range 0-1)
class.typeNormalization = 'MinMax';

% Downsampling factor of the epochs
class.downsampling = 1;

% if there's ICA, this value will be overridden by the number of components for each subject
class.channels = 1:64; 

% List of time windows to be decoded
% rows: each of the time windows to be decoded
% columns: starting-ending time point in ms, with 0 the epoch onset
% In this case, we decode single samples every 4 ms.
time_windows = [];
time_windows(:,1) = [-200:4:600];
time_windows(:,2) = time_windows(:,1);

% suffix for the saved files.
filename_suffix = 'results_permutations';
if ~(exist(filename_suffix,'dir'))
    mkdir(filename_suffix);
end



%%%%%%%%%%%%%%%%%%%%%%%
%% Run decoding for both conditions and subjects
%%%%%%%%%%%%%%%%%%%%%%%

% cond. 0: active; 1: monitoring
for cond=0:1
    
    if (cond==0)
        condname = 'act';
    else
        condname = 'mon';
    end

    save_filename_suffix = [condname '_' filename_suffix];

    for s=subjects
        % In the IC decoding, we load the previously computed weights and
        % ICs to retain.
        if (apply_ica)
            load([data_path '/s' num2str(s) '/' 's' num2str(s) '_all_ICA.mat'], 'EEG');
            w_ica = EEG.icaweights * EEG.icasphere;
            new_data = zeros(size(EEG.icawinv,2), size(EEG.data,2), size(EEG.data,3));
            for trial=1:size(EEG.data,3)
                new_data(:,:,trial) = w_ica * EEG.data(:,:,trial);
            end
            EEG.data = new_data;
            load([data_path '/removed_ICs.mat']);
            EEG.data(removed_ICs{s}, :,:) = [];
            class.channels = 1:size(EEG.data,1);
            w_ica(removed_ICs{s},:) = [];
            inv_ica = pinv(w_ica);
        else
            load([data_path '/s' num2str(s) '/' 's' num2str(s) '_all_ICA_dipclean.mat'], 'EEG');
        end
       
        [epochs_to_use, confidence_values, labels_confidence, trialid, epochs_run, trialrun, epochs] = ...
            prepare_trials(EEG, data_path, s, cond, remove_bad_trials, analyze_uncertainty);
        
        pred_reg_xval = zeros(size(time_windows,1), length(epochs_to_use), num_permutations);
        weights_pred_reg_xval = {};
        pred_reg_train = zeros(size(time_windows,1), length(epochs_to_use), num_permutations);
        weights_pred_reg_train = zeros(size(time_windows,1), 64, num_permutations);
        r2_reg = zeros(size(time_windows,1),num_permutations);

        match = false(1,64);
        for e=1:length(EEG.chanlocs)
            el = find(strcmp(EEG.chanlocs(e).labels,elecs));
            match(el) = 1;
        end

        
        for perm=1:num_permutations
            tic
            fprintf('[%s] Subject %d (%d epochs, %d runs, %d/%d permutations)\n', upper(condname), s, length(trialid),max(epochs_run), perm, num_permutations);
            
            % Permute confidences
            confidence_values = confidence_values(randperm(length(confidence_values)));
            
            for t=1:size(time_windows,1)

                str = sprintf('[%s] Subject %d. Window %d/%d', upper(condname), s, t, length(time_windows)); fprintf(str);

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % Confidence regression - Leave one out cross validation
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    % Extract the desired features
                    [ dataset_features ] = extractFeatures_avg( EEG, epochs, time_windows(t,:), class.channels, class.downsampling, labels_confidence, confidence_values );   
                    % Cross-validate the data (in this case, leave one out)
                    [ train, test ] = crossValidation( dataset_features, length(dataset_features.labels), 1:length(dataset_features.labels) );
                    % Normalize and PCA the data -- if desired
                    [train, test, normalization, PCA] = func_postprocessFeatures(train, test, class.applyPCA, class.typeNormalization, 'OptionsPCA', class.optionsPCA);

                    % confidence ~ b + w*features
                    fields = fieldnames(train);

                    for f=1:length(fields)
                        featTrain = [ones(size(train.(fields{f}).features,1), 1) train.(fields{f}).features];
                        featTest= [ones(size(test.(fields{f}).features,1), 1) test.(fields{f}).features];
                        coeffs = regress(train.(fields{f}).conf', featTrain);
                        pred_reg_xval(t,f,perm) = coeffs(1) + sum(coeffs(2:end).*test.(fields{f}).features');
                        weights_pred_reg_xval{t,f, perm} = coeffs;
                    end
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                fprintf(repmat('\b', 1, length(str)));
            end

            % Compute predicted r2 from leave-one-out cross validation
            var = pred_reg_xval(:,:,perm);
            ssreg = sum(bsxfun(@minus,var,mean(confidence_values)).^2,2);
            sstot = sum((confidence_values - mean(confidence_values)).^2);
            ssres = sum(bsxfun(@minus,var,confidence_values).^2,2);
            r2_reg(:,perm) = ssreg/sstot;

            toc
        end
        fprintf('[%s] Subject %d. Window %d/%d... DONE\n', upper(condname), s, t, length(time_windows));
        
        % Save results
        save(['results_permutations\s' num2str(s) '_' save_filename_suffix '.mat'],...
            'pred_reg_xval','weights_pred_reg_xval', 'trialid','trialrun','r2_reg');

    end
end
