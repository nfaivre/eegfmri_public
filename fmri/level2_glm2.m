% List of open inputs



config();

type = 'glm2';
opts.con1 = '0005';
opts.con2 = '0006';
opts.con3 = '0007';
opts.con4 = '0008';

matlabbatch = level2_glm2_job(opts,type);
spm_jobman('run',matlabbatch);

