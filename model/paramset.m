%========================================================
%   Set parameters for the model
%   
%   Pereira, Faivre, Iturrate et al., 2019, 
%   Disentangling the origins of confidence in speeded 
%      perceptual judgments through multimodal imaging
%
%   preregistration: https://osf.io/a5qmv/  
%   gitlab: https://gitlab.com/meta-eegfmri/  
%      
%   Michael Pereira <michael.pereira@epfl.ch>
%   25/09/2018
%========================================================
function [ param ] = paramset(  )

param = struct();

%% FIRST STAGE FREE PARAMETERS
% The decision bound (constrained to >0)
param.bound.val = 10;
param.bound.fit = 1;
param.bound.trans = @(x) exp(x);
param.bound.itrans = @(x) log(x);

% The drift rate (constrained to >0)
param.drift.val = 0.05;
param.drift.fit = 1;
param.drift.trans = @(x) exp(x);
param.drift.itrans = @(x) log(x);

% The non-decision time (constrained to 0<ndtime<1)
param.ndtime.val = 0.3;
param.ndtime.fit = 1;
param.ndtime.trans = @(x) 1./(1+exp(-x));
param.ndtime.itrans = @(x) log(x./(1-x));

% The non-decision time variability
param.ndtime_std.val = 0.04;
param.ndtime_std.fit = 1;
param.ndtime_std.trans = @(x) 1./(1+exp(-x));
param.ndtime_std.itrans = @(x) log(x./(1-x));

% The starting point variability in units of the decision bound
% This value was fixed to the maximum variability to model early errors
param.startvar.val = 1;
param.startvar.fit = 1;
param.startvar.trans = @(x) exp(x);
param.startvar.itrans = @(x) log(x);

%% FIRST STAGE FIXED PARAMETERS

% The decay of the drift rate over time
% (constrained to >0)
param.decay.val = 0;%0.5;%.4;
param.decay.fit = 0;
param.decay.trans = @(x) exp(x);
param.decay.itrans = @(x) log(x);

% The urgency signal that pushes the accumulated evidence to the decision
% bound as time passes. 
% This parameter was not used for the sake of simplicity
param.urg.val = 0;
param.urg.fit = 0;
param.urg.trans = @(x) exp(x);
param.urg.itrans = @(x) log(x);

% The trial-by-trial variability in drift rate
% This parameter was not used for the sake of simplicity
param.drift_std.val = 0.0;
param.drift_std.fit = 0;
param.drift_std.trans = @(x) exp(x);
param.drift_std.itrans = @(x) log(x);

%% SECOND STAGE
% The slope of the confidence mapping (constrained to >0)
param.confslope.val = 0.05;
param.confslope.fit = 1;
param.confslope.trans = @(x) exp(x);
param.confslope.itrans = @(x) log(x);

% The bias of the confidence mapping (constrained to >0)
param.confbias.val = 0.5;
param.confbias.fit = 1;
param.confbias.trans = @(x) (x);
param.confbias.itrans = @(x) (x);

% The time at which the confidence is read-out 
% (constrained to 0<conftime<1)
param.conftime.val = 0.4;
param.conftime.fit = 0;
param.conftime.trans = @(x) 1./(1+exp(-x));
param.conftime.itrans = @(x) log((x)./(1-x));



end

