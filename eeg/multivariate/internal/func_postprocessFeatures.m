function [ trainData, testData, normalization, PCA ] = func_postprocessFeatures( trainData, testData, applyPCA, typeNormalization, varargin )

    optionsPCA = {};
    optionsNormalization = {};
    debug = 0;
    PCA = {};
    for v=1:length(varargin)
        if (strcmp(varargin{v},'OptionsPCA'))
            optionsPCA = varargin{v+1};
        end       
        if (strcmp(varargin{v},'PCA'))
            PCA = varargin{v+1};
        end       
        if (strcmp(varargin{v},'OptionsNormalization'))
            optionsNormalization = varargin{v+1};
        end   
        if (strcmp(varargin{v},'Debug'))
            debug = varargin{v+1};
        end
    end
    fields = fieldnames(trainData);
    if (strfind(fields{1}, 'fold'))
        if (isempty(PCA))
            for f=1:length(fields)
                [trainData.(fields{f}), testData.(fields{f}), normalization.(fields{f}), PCA.(fields{f})] = ...
                    func_performPostProcess(trainData.(fields{f}), testData.(fields{f}), ...
                applyPCA, typeNormalization, (debug & (f==1)), optionsPCA, optionsNormalization, {});
            end
        else
            for f=1:length(fields)
                [trainData.(fields{f}), testData.(fields{f}), normalization.(fields{f}), PCA.(fields{f})] = ...
                    func_performPostProcess(trainData.(fields{f}), testData.(fields{f}), ...
                applyPCA, typeNormalization, (debug & (f==1)), optionsPCA, optionsNormalization, PCA.(fields{f}));
            end
        end
    else
        [trainData, testData, normalization, PCA] = func_performPostProcess(...
            trainData, testData, applyPCA, typeNormalization, debug, optionsPCA, optionsNormalization, PCA);
    end
    if (debug)
        disp('End Filtering');
        disp('---------------------------------------------');   
    end    
end

function [trainData, testData, normalization, PCA] = func_performPostProcess(trainData, testData, applyPCA, typeNormalization, debug, optionsPCA, optionsNormalization, PCA)

    normalization = [];
    
    optionsPCA{end+1} = 'PCA';
    optionsPCA{end+1} = PCA;
    
    if (strcmp(applyPCA,'AfterNorm'))
        if (debug)
            disp('Applying Norm->PCA');
        end
        [trainData, testData, normalization] = func_normalizeData(trainData, testData, typeNormalization, debug, optionsNormalization);
        features_to_remove = (normalization.max == 0) | (normalization.min == 0);
        trainData.features(:,features_to_remove) = [];
        testData.features(:,features_to_remove) = [];
        [i,features_to_remove] = find(isnan(trainData.features));
        trainData.features(:,features_to_remove) = [];
        testData.features(:,features_to_remove) = [];
        [trainData, testData, PCA] = func_applyPCA(trainData, testData, debug, optionsPCA, 'PCA', PCA);
        
    elseif (strcmp(applyPCA,'BeforeNorm'))
        if (debug)        
            disp('Applying PCA->Norm');
        end
        [trainData, testData, PCA] = func_applyPCA(trainData, testData, debug, optionsPCA, 'PCA', PCA);
        [trainData, testData, normalization] = func_normalizeData(trainData, testData, typeNormalization, debug, optionsNormalization);
        features_to_remove = (normalization.max == 0) | (normalization.min == 0);
        trainData.features(:,features_to_remove) = [];
        testData.features(:,features_to_remove) = [];        
        
    elseif (strcmp(applyPCA, 'NoPCA'))
        if (debug)       
            disp('Not applying PCA');
        end
        [trainData, testData, normalization] = func_normalizeData(trainData, testData, typeNormalization, debug, optionsNormalization);
    else
        error('Unknown PCA!!!');
    end
end

