%========================================================
%   step2_fitconf.m
%
%   Reads behavioral data files and fits a race model to
%   response times, accuracy and confidence for high and low RT
%
%   Pereira, Faivre, Iturrate et al., 2019, 
%   Disentangling the origins of confidence in speeded 
%      perceptual judgments through multimodal imaging
%
%   preregistration: https://osf.io/a5qmv/  
%   gitlab: https://gitlab.com/meta-eegfmri/  
%      
%   Michael Pereira <michael.pereira@epfl.ch>
%   25/09/2018
%========================================================

clear
%close all
addpath functions

config();

data = cell(length(opts.subjects),3);

confparams_act = cell(1,4);
confparams_obs = cell(1,4);
confestimation_act = cell(1,4);
confestimation_obs = cell(1,4);
out_act = cell(1,4);
out_obs = cell(1,4);
ll_act = [];
ll_obs = [];

%% Here we set the way confidence will be computed.

options = cell(1,3);
% the first model assumes that subjects compute confidence from
% the accumulator corresponding to their choice or the computer's choice
options{1} = opts;
options{1}.confmodel = 'win';

% the second model assumes that subjects compute confidence from 
% the accumulator with highest value.
options{2} = opts;
options{2}.confmodel = 'win-2nd';

%% alternative models: 
% this model assumes that subjects compute confidence from 
% Their first-order decision. For active it is identical to the 'win' model
% but for observation it computes confidence based on a covert first-order
% decision
options{3} = opts;
options{3}.confmodel = 'win-1st';

% this model assumes that subjects compute confidence from the loosing
% accumulator
%options{4} = opts;
%options{4}.confmodel = 'loose';

% this model assumes that subjects compute confidence from the balance of
% evidence: the difference between the winning and loosing accumulator
%options{5} = opts;
%options{5}.confmodel = 'balance';

s = 0;
for subj=opts.subjects
    s = s+1;
    
    %========================================================
    %% load behav data
    %========================================================
    basedir = [opts.data_path '/derivatives/fmriprep/' ...
        cell2mat(subj) '/ses-001/func/'];
    files = dir([basedir '/' cell2mat(subj) ... 
        '_ses-001_task-main_run-*_events.tsv']);
    data = readbehav(files,basedir,s);
    %========================================================
    %% Process behav data for active condition
    %========================================================
    dat.rt_act = data{s,1}.rt;
    dat.resp_act = data{s,1}.cor;
    dat.conf_act = data{s,1}.conf;
    
    bad = dat.rt_act < opts.cens2(1) | dat.rt_act > opts.cens2(2) | dat.resp_act < 0 | dat.conf_act == -1;
    dat.rt_act(bad) = [];
    dat.resp_act(bad) = [];
    dat.conf_act(bad) = [];
    
    [~,~,~,auc] = perfcurve(dat.resp_act,dat.conf_act,1);
    
    %========================================================
    %% Process behav data for observation condition
    %========================================================
    dat.rt_obs = data{s,2}.rt;
    dat.resp_obs = data{s,2}.cor;
    dat.conf_obs = data{s,2}.conf;
    bad = dat.rt_obs < opts.cens2(1) | dat.rt_obs > opts.cens2(2) | dat.resp_obs < 0 | dat.conf_obs == -1;
    dat.rt_obs(bad) = [];
    dat.resp_obs(bad) = [];
    dat.conf_obs(bad) = [];
    
    
    %========================================================
    %% Simulate DV
    %========================================================
    
    % load parameters from the best model in terms of log-likelihood
    load(['models/' opts.type '/' opts.subjects{s} '_race_' opts.type '.mat'],'fitparams','out','ll');
    [~,best] = min(ll);
    %best = 5;
    fprintf('Subject: %s, ll = ',opts.subjects{s});
    for i=1:length(ll)
        if i==best
            fprintf('[%.3f]',ll(i));
        else
            fprintf(' %.3f ',ll(i));
        end
        
    end
    fprintf('\n');
    %%

    params = fitparams{best};
    
    if strcmp(opts.readout,'eeg')
        tbl = readtable('../misc/eegdecode.csv');
        id = strcmp(opts.subjects{s},{tbl.subj{:}});
        tact2 = tbl.tact2(id)+opts.motordelay;
        tobs2 = tbl.tobs2(id)+opts.motordelay;
    end


    % simulate one dataset for active
    opts2 = opts;
    opts2.N = opts.N2;
    [sim_act,dv_act,dvresp_act] = evacc(params,opts2);
    % simulate a second dataset for observation
    [sim_obs,dv_obs] = evacc(params,opts2);
    
    % remove bad and censor
    simbad = isnan(sim_act.resp) | sim_act.rt < opts.cens2(1) | sim_act.rt > opts.cens2(2);
    
    sim_act.irt(simbad) = [];
    sim_act.rt(simbad) = [];
    sim_act.resp(simbad) = [];
    dv_act.dv(simbad,:,:) = [];
    dv_act.dv_urg(simbad,:,:) = [];
    dvresp_act.dv(simbad,:,:) = [];
    dvresp_act.dv_urg(simbad,:,:) = [];
    
    sim_obs.irt(simbad) = [];
    sim_obs.rt(simbad) = [];
    sim_obs.resp(simbad) = [];
    dv_obs.dv(simbad,:,:) = [];
    dv_obs.dv_urg(simbad,:,:) = [];
    
    % we replace the choice accuracy and the rt
    sim_obs.resp_covert = sim_obs.resp;
    sim_obs.rt_covert = sim_obs.rt;
   
    sim_obs.resp = sim_act.resp;
    sim_obs.rt = sim_act.rt;
    sim_obs.irt = sim_act.irt;
    
    % prepare to replace the response-aligned d.v. from the observation
    % condition, as we neeed to realign them to the RTs from the active
    % condition (as in our paradigm)
    L2 = opts.L2/opts.dt;
    N = sum(~simbad);
    dvresp_obs.dv = nan(N,L2+1,2);
    dvresp_obs.dv_urg = nan(N,L2+1,2);
    for r=1:N
        dvresp_obs.dv(r,:,:) = dv_obs.dv(r,sim_obs.irt(r)+(0:L2),:);
        dvresp_obs.dv_urg(r,:,:) = dv_obs.dv_urg(r,sim_obs.irt(r)+(0:L2),:);
    end
   
    %========================================================
    %% Transform to confidence
    %========================================================
    % Here, we need to transform the state of two accumulators
    for modeltype = 1:length(options)
        rng(1234);
        if strcmp(opts.readout,'eeg')
            params.conftime.val = tact2;
            params.conftime.fit = 0;
        end
        [ confparams_act{modeltype}, ll_act(modeltype),out_act{modeltype}] = fitconf( dat.rt_act,dat.resp_act,dat.conf_act, sim_act.rt, sim_act.resp, dvresp_act.(opts.dvtype),  params, options{modeltype},sim_act.resp);
        [confestimation_act{modeltype},dvestimation_act{modeltype}] = dv2conf( dvresp_act.(opts.dvtype),sim_act.resp, confparams_act{modeltype}, options{modeltype},sim_act.resp);
        rng(1234);
        if strcmp(opts.readout,'eeg')
            params.conftime.val = tobs2;
            params.conftime.fit = 0;
        end
        [ confparams_obs{modeltype}, ll_obs(modeltype),out_obs{modeltype}] = fitconf( dat.rt_obs,dat.resp_obs,dat.conf_obs, sim_obs.rt, sim_obs.resp, dvresp_obs.(opts.dvtype),  params, options{modeltype},sim_obs.resp_covert);
        [confestimation_obs{modeltype},dvestimation_obs{modeltype}] = dv2conf( dvresp_obs.(opts.dvtype),sim_obs.resp, confparams_obs{modeltype}, options{modeltype},sim_obs.resp_covert);
        
        fprintf(' * Model %d loglikelihood: %.2f (active) %.2f (observation)\n',modeltype, ll_act(modeltype),ll_obs(modeltype));
        
    end
    
  
    save(['models/' opts.type '/' opts.subjects{s}  '_race_' opts.type '_ro-' opts.readout '_confsim.mat'],...
        'confparams_act','confparams_obs', ...
        'confestimation_act','confestimation_obs', ...
        'dvestimation_act','dvestimation_obs', ...
        'sim_act','sim_obs','out_act','out_obs','ll_act','ll_obs','options');
    
end