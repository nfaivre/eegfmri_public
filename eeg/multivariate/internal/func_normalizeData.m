function [ trainData, testData, normalization ] = func_normalizeData( trainData, testData, typeNormalization, debug,  varargin )

    if (iscell(varargin{1}))
        varargin = varargin{1};
    end

    normalizationRange = [0 1];
    normalization = [];
    for v=1:length(varargin)    
        if (strcmp(varargin{v},'NormalizationRange'))
            normalizationRange = varargin{v+1};
        end
    end    
    maximo = [];
    minimo = [];
    mu = [];
    sigma = [];
    if strcmp(typeNormalization,'MinMax')
        if (debug)
            disp('Applying Norm MinMax');     
        end
        for i=1:size(trainData.features,2)
            maximo(i) = max(trainData.features(:,i));
            minimo(i) = min(trainData.features(:,i));

            trainData.features(:,i) = ...
                (((trainData.features(:,i) - minimo(i)) / (maximo(i) - minimo(i))) * ...
                (normalizationRange(2) - normalizationRange(1))) + normalizationRange(1);

            testData.features(:,i) = ...
                (((testData.features(:,i) - minimo(i)) / (maximo(i) - minimo(i))) * ...
                (normalizationRange(2) - normalizationRange(1))) + normalizationRange(1);
        end 
        
    elseif strcmp(typeNormalization,'zScore')
        if (debug)
            disp('Applying Norm zScore');
        end
        
        for i=1:size(trainData.features,2)
            
            mu(i) = mean(trainData.features(:,i));
            sigma(i) = std(trainData.features(:,i));
            
            trainData.features(:,i) = (trainData.features(:,i) - mu(i)) ./ (sigma(i));
            testData.features(:,i) = (testData.features(:,i) - mu(i)) ./ (sigma(i));
        end
    elseif strcmp(typeNormalization,'MinMaxRows')
        if (debug)
            disp('Applying Norm MinMax');     
        end
        
        for i=1:size(trainData,1)
            
            maximo = max(trainData.features(i,:));
            minimo = min(trainData.features(i,:));

            trainData.features(i,:) = ...
                (((trainData.features(i,:) - minimo) / (maximo - minimo)) * ...
                (normalizationRange(2) - normalizationRange(1))) + normalizationRange(1);

        end 
        
        for i=1:size(testData,1)
            maximo = max(testData.features(i,:));
            minimo = min(testData.features(i,:));

            testData.features(i,:) = ...
                (((testData.features(i,:) - minimo) / (maximo - minimo)) * ...
                (normalizationRange(2) - normalizationRange(1))) + normalizationRange(1);
            
        end
        
        
    elseif strcmp(typeNormalization,'NoNormalization')
        if (debug)
            disp('No Normalization!!!');
        end
    else
        error('Unknown Normalization!');            
    end
    
    normalization.max = maximo;
    normalization.min = minimo;
    normalization.mu = mu;
    normalization.sigma = sigma;
    normalization.typeNormalization = typeNormalization;
    normalization.normalizationRange = normalizationRange;
end

