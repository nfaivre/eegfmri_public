%========================================================
%   sigmoid.m passes a value through a sigmoid function, adjusting bias (b) and
%   sensitivity (a) and optionally inverts confidence for all w==-1
%
%   Pereira, Faivre, Iturrate et al., 2019, 
%   Disentangling the origins of confidence in speeded 
%      perceptual judgments through multimodal imaging
%
%   preregistration: https://osf.io/a5qmv/  
%   gitlab: https://gitlab.com/meta-eegfmri/  
%      
%   Michael Pereira <michael.pereira@epfl.ch>
%   25/09/2018
%========================================================
function [ simconf ] = sigmoid( dv, a,b, w)

if nargin < 4 || isempty(w)
    w = ones(size(dv));
end
reg = dv*a - b;

mult = bsxfun(@times,w,reg);
simconf = exp(mult)./(1+exp(mult));

end

