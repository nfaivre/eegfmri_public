%%%%%%%%%%%%%%%%%%%%%%%
%% Set path and subjects
%%%%%%%%%%%%%%%%%%%%%%%

close all, clear all, clc;

addpath(genpath('./internal'));
addpath('/home/nfaivre/matlab/eeglab14_1_1b/');

%eeglab();

data_path = '/Scratch/meaperei/BIDS/eegfmri/derivatives/eegprep/';% path to the data

subjectsname = {'sub-02','sub-04','sub-07','sub-08','sub-09','sub-10','sub-11',...
    'sub-12','sub-13','sub-14','sub-15','sub-16','sub-17','sub-18','sub-19','sub-20',...
    'sub-21','sub-22','sub-24','sub-26'};
subjects = [2 4 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 24 26];

elecs = {'Fp1','Fp2','F3','F4','C3','C4','P3','P4','O1','O2','F7','F8','T7','T8',...
    'P7','P8','Fz','Cz','Pz','Oz','FC1','FC2','CP1','CP2','FC5','FC6','CP5','CP6',...
    'TP9','TP10','POz','FCz','F1','F2','C1','C2','P1','P2','AF3','AF4','FC3','FC4',...
    'CP3','CP4','PO3','PO4','F5','F6','C5','C6','P5','P6','AF7','AF8','FT7','FT8',...
    'TP7','TP8','PO7','PO8','FT9','FT10','Fpz','CPz'};


%%%%%%%%%%%%%%%%%%%%%%%
%% Set parameters
%%%%%%%%%%%%%%%%%%%%%%%


% 0/1: whether to decode confidence (0) or uncertainty (1)
analyze_uncertainty = 0;

% 0: perform decoding in the channel EEG space without ICA
% 1: perform decoding on the selected ICs
apply_ica = 1;

% 0: do not remove artifactual trials
% 1: remove artifactual trials
remove_bad_trials = 1;

% Apply PCA to the data or not
apply_PCA = 0;


if apply_PCA
    class.applyPCA = 'AfterNorm';
    class.optionsPCA = {'VarianceToRetain', 0.99};
else
    class.optionsPCA = {};
    class.applyPCA = 'NoPCA';
end

% Feature normalization MinMax (range 0-1)
class.typeNormalization = 'MinMax';

% Downsampling factor of the epochs
class.downsampling = 1;

% if there's ICA, this value will be overridden by the number of components for each subject
class.channels = 1:64;

% List of time windows to be decoded
% rows: each of the time windows to be decoded
% columns: starting-ending time point in ms, with 0 the epoch onset
% In this case, we decode single samples every 4 ms.
time_windows = [];
time_windows(:,1) = [-200:4:600];
time_windows(:,2) = time_windows(:,1);

% suffix for the saved files.
filename_suffix = 'eegdecode';
if ~(exist(filename_suffix,'dir'))
    mkdir(filename_suffix);
end

%%%%%%%%%%%%%%%%%%%%%%%
%% Run decoding for both conditions and subjects
%%%%%%%%%%%%%%%%%%%%%%%

% cond. 0: active; 1: monitoring
for cond=0:1
    
    if (cond==0)
        condname = 'act';
    else
        condname = 'obs';
    end
    
    save_filename_suffix = [condname '_' filename_suffix];
    
    
    for s=1:length(subjects)
        %%
        % In the IC decoding, we load the previously computed weights and
        % ICs to retain.
        if (apply_ica)
            %%
            eegfile = [data_path '/' subjectsname{s} '/ses-001/eeg/' ...
                subjectsname{s} '_ses-001_task-main_eeg_preproc.set'];
            fprintf('%s: opening %s\n',subjectsname{s},eegfile);
            %load(eegfile, 'EEG');
            EEG = pop_loadset(eegfile);
            w_ica = EEG.icaweights * EEG.icasphere;
            new_data = zeros(size(EEG.icawinv,2), size(EEG.data,2), size(EEG.data,3));
            for trial=1:size(EEG.data,3)
                new_data(:,:,trial) = w_ica * EEG.data(:,:,trial);
            end
            EEG.data = new_data;
            load(['../../misc/removed_ICs.mat']);
            EEG.data(removed_ICs{subjects(s)}, :,:) = [];
            class.channels = 1:size(EEG.data,1);
            w_ica(removed_ICs{subjects(s)},:) = [];
            inv_ica = pinv(w_ica);
        else
            load([data_path '/s' num2str(subjects(s)) '/' 's' num2str(subjects(s)) '_all_ICA_dipclean.mat'], 'EEG');
        end
        
        [epochs_to_use, confidence_values, labels_confidence, trialid, epochs_run, trialrun, epochs, err, rt, delta] = ...
            prepare_trials(EEG, data_path, subjects(s), cond, remove_bad_trials, analyze_uncertainty);
        
        pred_reg_xval = zeros(size(time_windows,1), length(epochs_to_use));
        weights_pred_reg_xval = {};
        pred_reg_train = zeros(size(time_windows,1), length(epochs_to_use));
        weights_pred_reg_train = zeros(size(time_windows,1), 64);
        
        match = false(1,64);
        for e=1:length(EEG.chanlocs)
            el = find(strcmp(EEG.chanlocs(e).labels,elecs));
            match(el) = 1;
        end
        
        
        fprintf('[%s] Subject %d (%d epochs, %d runs)\n', upper(condname), subjects(s),length(trialid),max(epochs_run));
        tic
        for t=1:size(time_windows,1)
            
            str = sprintf('[%s] Subject %d. Window %d/%d', upper(condname), subjects(s), t, length(time_windows)); fprintf(str);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Confidence regression - Leave one out cross validation
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Extract the desired features
            [ dataset_features ] = extractFeatures_avg( EEG, epochs, time_windows(t,:), class.channels, class.downsampling, labels_confidence, confidence_values );
            % Cross-validate the data (in this case, leave one out)
            [ train, test ] = crossValidation( dataset_features, length(dataset_features.labels), 1:length(dataset_features.labels) );
            % Normalize and PCA the data -- if desired
            [train, test, normalization, PCA] = func_postprocessFeatures(train, test, class.applyPCA, class.typeNormalization, 'OptionsPCA', class.optionsPCA);
            
            % confidence ~ b + w*features
            fields = fieldnames(train);
            
            for f=1:length(fields)
                featTrain = [ones(size(train.(fields{f}).features,1), 1) train.(fields{f}).features];
                featTest= [ones(size(test.(fields{f}).features,1), 1) test.(fields{f}).features];
                coeffs = regress(train.(fields{f}).conf', featTrain);
                pred_reg_xval(t,f) = coeffs(1) + sum(coeffs(2:end).*test.(fields{f}).features');
                weights_pred_reg_xval{t,f} = coeffs;
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Confidence regression - Modeling (using training set alone)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            [ train ] = extractFeatures_avg( EEG, epochs, time_windows(t,:), class.channels, class.downsampling, labels_confidence, confidence_values );
            [train, ~, ] = func_postprocessFeatures(train, train, class.applyPCA, class.typeNormalization, 'OptionsPCA', class.optionsPCA);
            featTrain = [ones(size(train.features,1), 1) train.features];
            br = regress(train.conf', featTrain);
            pred_reg_train(t,:) = br(1) + sum(br(2:end).*train.features');
            weights_pred_reg_train(t,match) = inv_ica*abs(br(2:end));
            weights_pred_reg_train(t,~match) = NaN;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            fprintf(repmat('\b', 1, length(str)));
        end
        
        % Compute predicted r2 from leave-one-out cross validation
        var = pred_reg_xval;
        ssreg = sum(bsxfun(@minus,var,mean(confidence_values)).^2,2);
        sstot = sum((confidence_values - mean(confidence_values)).^2);
        ssres = sum(bsxfun(@minus,var,confidence_values).^2,2);
        r2_reg = ssreg./sstot;
        
        toc
        fprintf('Subject %d. Window %d/%d... DONE\n', subjects(s), t, length(time_windows));
       
        [cor.all,cor.p_all] = corr(pred_reg_xval.',confidence_values.');
        [cor.err,cor.p_err] = corr(pred_reg_xval(:,err==1).',confidence_values(err==1).');
        [cor.cor,cor.p_cor] = corr(pred_reg_xval(:,err==0).',confidence_values(err==0).');
        %%
        erp.all = mean(var,2);
        erp.cor = mean(var(:,err==0),2);
        erp.corhigh = mean(var(:,err==0 & confidence_values >= median(confidence_values(err==0))),2);
        erp.corlow = mean(var(:,err==0 & confidence_values <= median(confidence_values(err==0))),2);
        erp.err = mean(var(:,err==1),2);
        erp.errhigh = mean(var(:,err==1 & confidence_values >= median(confidence_values(err==1))),2);
        erp.errlow = mean(var(:,err==1 & confidence_values <= median(confidence_values(err==1))),2);
         
        %figure(); hold on;
        %plot(time_windows(:,1),erp.all,'k','LineWidth',2);
        %plot(time_windows(:,1),erp.err,'r','LineWidth',2);
        %plot(time_windows(:,1),erp.cor,'g','LineWidth',2);
        %plot(time_windows(:,1),erp.corhigh,'b','LineWidth',2);
        %plot(time_windows(:,1),erp.corlow,'c','LineWidth',2);
        %plot([0 0],ylim(),'k--');
        %plot([200 200],ylim(),'k--');
        %plot([600 600],ylim(),'k--');
       % 
       % title([subjectsname{s} ' - ' condname]);
       % pause(0.1);
        %%
        % Save results
        save(['results_decoding/' subjectsname{s} '_' save_filename_suffix '.mat'],...
            'pred_reg_xval','weights_pred_reg_xval','pred_reg_train','erp',...
            'weights_pred_reg_train','confidence_values','err','rt','delta','trialid','trialrun','r2_reg','cor',...
            'time_windows');
        
    end
end
