askfile = 'masks/roi_contrastslope_ba10.nii'; tit = 'BA10';
%maskfile = 'masks/roi_contrastslope_dacc.nii'; tit = 'dACC';
%maskfile = 'masks/roi_contrastslope_lpfc.nii'; tit = 'LPFC';
%maskfile = 'masks/roi_contrastslope_lsma.nii'; tit = 'RSMA';
%maskfile = 'masks/roi_contrastslope_rsma.nii'; tit = 'SMA';

%maskfile = 'masks/roi_conj_err_ppc.nii'; tit = 'PPC';
%maskfile = 'masks/roi_conj_err_sma.nii'; tit = 'SMA';
%maskfile = 'masks/roi_conj_err_insula.nii'; tit = 'Insula';

%maskfile = 'masks/roi_conj_uncert_ifg.nii';  tit = 'IFC';
%maskfile = 'masks/roi_conj_uncert_sma.nii'; tit = 'SMA';

%maskfile = 'masks/roi_cert_act_occipital.nii'; tit = 'Occipital';
%maskfile = 'masks/roi_cert_obs_occipital.nii'; tit = 'Occipital';
maskfile = 'masks/roi_cert_obs_rhippo.nii'; tit = 'RHC';
maskfile = 'masks/roi_cert_obs_lhippo.nii'; tit = 'LHC';
maskfile = 'masks/roi_cert_obs_acc.nii'; tit = 'ACC';
%maskfile = 'masks/roi_cert_obs_cuneus.nii'; tit = 'Cuneus';
%maskfile = 'masks/roi_cert_obs_cuneus2.nii'; tit = 'Cuneus';
%maskfile = 'masks/roi_cert_obs_ltemp.nii'; tit = 'LTEMP';

%maskfile = 'masks/roi_actgtobs_dacc.nii'; tit = 'dACC';

%maskfile = 'masks/roi_taskact_lprecentral.nii'; tit = 'LMotor';
%maskfile = 'masks/roi_taskact_rprecentral.nii'; tit = 'RMotor';
%maskfile = 'masks/roi_taskact_lprecentral2.nii'; tit = '';
%maskfile = 'masks/roi_taskact_lsma.nii'; tit = 'LSMA';
maskfile = 'masks/roi_covmetaneg_ofc.nii'; tit = 'OFC';

%maskfile = 'masks/roi_uncert_obs_supramarg.nii'; tit = 'OFC';

%maskfile = 'masks/roi_obsgtact_rangular.nii'; tit = 'Angular';


betadir = 'E:/Data/eegfmri/spm/glm2a/conf_none/';type = {'C001','C002'};
%betadir = 'E:/Data/eegfmri/spm/glm2e/conf_none/';type = {'C005','C006'};
%betadir = 'E:/Data/eegfmri/spm/glm2f/conf_none/';type = {'C005','C006'};


subjects = {'s1','s2','s4','s7','s8','s9','s10','s11','s12','s13','s14','s15','s16','s17','s18','s19','s20','s21','s22','s24','s26'}; % ,'s22'

mask = spm_data_read(maskfile);

beta1 = zeros(1,length(subjects));
beta2 = zeros(1,length(subjects));
slope1 = zeros(1,length(subjects));
slope2 = zeros(1,length(subjects));
ic1 = zeros(1,length(subjects));
ic2 = zeros(1,length(subjects));
rt1 = zeros(1,length(subjects));
rt21 = zeros(1,length(subjects));
rt22 = zeros(1,length(subjects));
dif = zeros(1,length(subjects));

for s = 1:length(subjects)
    subj = subjects{s};
    sujid = str2double({subj(2:end)});
    id = find(suj==sujid);
    slope1(s) = slopes1(id(1));
    slope2(s) = slopes1(id(2));
    ic1(s) = intercept(id(1));
    ic2(s) = intercept(id(2));   
    rt1(s) = rt(id(1));
    rt21(s) = rt2(id(1));
    rt22(s) = rt2(id(2));
    dif(s) = diff1(id(2));
    
  
    fprintf('Processing subject %s\n',subj);
    
    d1 = dir([betadir subj '_' type{1} '*.nii']);
    d2 = dir([betadir subj '_' type{2} '*.nii']);
    
    b1 = spm_data_read([betadir d1(1).name]);
    b2 = spm_data_read([betadir d2(1).name]);

    beta1(s) = mean(b1(mask(:) > 0));
    beta2(s) = mean(b2(mask(:) > 0));
end
%beta1 = -beta1;
%beta2 = -beta2;
%%
%behav = (gavgmot.auc2-gavgmon.auc2).';
behav1 = (slope1 - slope2).';
behav2 = (ic1 - ic2).';

fmri = (beta1 - beta2).';
%behav(3) = [];
%fmri(3) = [];
[cr1,p1] = corr(behav1,fmri);
[cr2,p2] = corr(behav2,fmri);
fprintf('Corr: %.2f (p=%.3f) %.2f (p=%.3f)\n',cr1,p1,cr2,p2);

[cr1a,p1a] = corr(slope1.',beta1.');
[cr2a,p2a] = corr(ic1.',beta1.');
fprintf('Corr: Active: %.2f (p=%.3f) %.2f (p=%.3f)\n',cr1a,p1a,cr2a,p2a);
[cr1o,p1o] = corr(slope2.',beta2.');
[cr2o,p2o] = corr(ic2.',beta2.');
fprintf('Corr: Observation: %.2f (p=%.3f) %.2f (p=%.3f)\n',cr1o,p1o,cr2o,p2o);

% X = [gavgmot.peslow.corr1.' gavgmot.meanrt1 gavgmot.meandelta gavgmot.meanperf];
X = behav1;
y = beta1.'; 
Xsur = zeros(length(y),1000);
for i=1:1000
    Xsur(:,i) = X(randperm(length(y)));
end
%%
  if p1<0.05
    %%
     figure(); hold on; 
     plot(behav1,fmri,'ko','MarkerFaceColor','k');
     X = [zscore(behav1) zscore(dif.') zscore(rt1.') behav1*0+1];
     [b,bint,~,~,st] = regress(zscore(fmri),[behav1 dif.' behav1*0+1]);
     X = [behav1 behav1*0+1];
     b = regress(fmri,X);
     X = [-10 1;10 1];
    % [i,id] = sort(X(:,1));
     plot(X(:,1),X*b,'r');
     plot([0,0],ylim(),'k');
     plot(xlim(),[0,0],'k');
     title(['Corr for ' tit ' (diff)']);
     xlim([-1,7]);
     set(gcf,'Position',[2000,500,300,300])

  end
  if p1a<0.05
     %%
     figure(); hold on; 
     plot(slope1,beta1,'ko','MarkerFaceColor','k');
     b = regress(beta1.',[slope1.' slope1.'*0+1]);
     X = [-10 1;20 1];
     plot(X(:,1),X*b,'r');
     plot([0,0],ylim(),'k');
     plot(xlim(),[0,0],'k');
     title(['Corr for ' tit ' (active)']);
     xlim([0,20]);
     set(gcf,'Position',[2500,500,300,300])

  end
  if p1o<0.05
     %%
     figure(); hold on; 
     plot(slope2,beta2,'ko','MarkerFaceColor','k');
     b = regress(beta2.',[slope2.' slope2.'*0+1]);
     X = [-10 1;20 1];
     plot(X(:,1),X*b,'r');
     plot([0,0],ylim(),'k');
     plot(xlim(),[0,0],'k');
     title(['Corr for ' tit ' (observation)']);
     xlim([0,20]);
     set(gcf,'Position',[3000,500,300,300])
  end
%%
figure(); clf; hold on; 
bar(1,mean(beta1),'r');
bar(2,mean(beta2),'c');
errorbar(1,mean(beta1),std(beta1),'k.','LineWidth',2);
plot(1.3,beta1,'k+')
errorbar(2,mean(beta2),std(beta2),'k.','LineWidth',2);
plot(2.3,beta2,'k+')
set(gcf,'Position',[2000,400,200,300])
if 0
    set(gca,'XTick',[],'XTickLabel',{},'YTick',-10:2:10,'YTickLabel',{});
else
set(gca,'XTick',[],'XTickLabel',{},'FontSize',14);
ylabel('Confidence betas');

title(tit,'FontSize',16);
end
ylim([-6,4]);
xlim([0,3]);