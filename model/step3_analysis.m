%========================================================
%   step3_analysis.m
%   Reads behavioral data files and fits a race model to
%   response times, accuracy and confidence for high and low RT
%
%   Pereira, Faivre, Iturrate et al., 2019, 
%   Disentangling the origins of confidence in speeded 
%      perceptual judgments through multimodal imaging
%
%   preregistration: https://osf.io/a5qmv/  
%   gitlab: https://gitlab.com/meta-eegfmri/  
%      
%   Michael Pereira <michael.pereira@epfl.ch>
%   25/09/2018
%========================================================

clear
%close all
addpath functions

config;
%%
data = cell(length(opts.subjects),3);
s = 0;
for subj=opts.subjects
    s = s+1;
    
    %========================================================
    %% load behav data
    %========================================================
    basedir = [opts.data_path '/derivatives/fmriprep/' ...
        cell2mat(subj) '/ses-001/func/'];
    files = dir([basedir '/' cell2mat(subj) ... 
        '_ses-001_task-main_run-*_events.tsv']);
    data = readbehav(files,basedir,s);
    %========================================================
    %% Process behav data for active condition
    %========================================================
    dat_act.rt = data{s,1}.rt;
    dat_act.resp = data{s,1}.cor;
    dat_act.conf = data{s,1}.conf;
    
    bad = dat_act.rt < opts.cens2(1) | dat_act.rt > opts.cens2(2) | dat_act.resp < 0 | dat_act.conf == -1;
    dat_act.rt(bad) = [];
    dat_act.resp(bad) = [];
    dat_act.conf(bad) = [];
    
    dat_act.meanrtcor(s) = mean(dat_act.rt(dat_act.resp==1));
    dat_act.meanrterr(s) = mean(dat_act.rt(dat_act.resp==0));
    dat_act.meanacc(s) = mean(dat_act.resp);
    dat_act.meanchmind(s) = mean(dat_act.conf<0.5);
    
    [~,~,~,dat_act.auc(s)] = perfcurve(dat_act.resp,dat_act.conf,1,'TVals',0:0.25:1);
    
    dat_act.hist_err(:,s) = hist(dat_act.conf(dat_act.resp==0),opts.confsupport_hist);
    dat_act.hist_cor(:,s) = hist(dat_act.conf(dat_act.resp==1),opts.confsupport_hist);
    dat_act.hist_err(:,s) = mean(dat_act.resp==0).*dat_act.hist_err(:,s)./sum(dat_act.hist_err(:,s));
    dat_act.hist_cor(:,s) = mean(dat_act.resp==1).*dat_act.hist_cor(:,s)./sum(dat_act.hist_cor(:,s));
    
    dat_act.rtdistr_err(:,s) = hist(dat_act.rt(dat_act.resp==0),opts.rtsupport_hist);
    dat_act.rtdistr_cor(:,s) = hist(dat_act.rt(dat_act.resp==1),opts.rtsupport_hist);
    dat_act.rtdistr_err(:,s) = mean(dat_act.resp==0).*dat_act.rtdistr_err(:,s)./sum(dat_act.rtdistr_err(:,s));
    dat_act.rtdistr_cor(:,s) = mean(dat_act.resp==1).*dat_act.rtdistr_cor(:,s)./sum(dat_act.rtdistr_cor(:,s));
    
    
    dat_act.stat_conf(:,s) = [mean(dat_act.conf) std(dat_act.conf)];
    dat_act.stat_conf_err(:,s) = [mean(dat_act.conf(dat_act.resp==0)) std(dat_act.conf(dat_act.resp==0))];
    dat_act.stat_conf_cor(:,s) = [mean(dat_act.conf(dat_act.resp==1)) std(dat_act.conf(dat_act.resp==1))];
    
    
    %========================================================
    %% Process behav data for observation condition
    %========================================================
    dat_obs.rt = data{s,2}.rt;
    dat_obs.resp = data{s,2}.cor;
    dat_obs.conf = data{s,2}.conf;
    bad = dat_obs.rt < opts.cens2(1) | dat_obs.rt > opts.cens2(2) | dat_obs.resp < 0 | dat_obs.conf == -1;
    dat_obs.rt(bad) = [];
    dat_obs.resp(bad) = [];
    dat_obs.conf(bad) = [];
    
    dat_obs.meanchmind(s) = mean(dat_obs.conf<0.5);
    
    
    [~,~,~,dat_obs.auc(s)] = perfcurve(dat_obs.resp,dat_obs.conf,1,'TVals',0:0.25:1);
    dat_obs.hist_err(:,s) = hist(dat_obs.conf(dat_obs.resp==0),opts.confsupport_hist);
    dat_obs.hist_cor(:,s) = hist(dat_obs.conf(dat_obs.resp==1),opts.confsupport_hist);
    dat_obs.hist_err(:,s) = mean(dat_obs.resp==0).*dat_obs.hist_err(:,s)./sum(dat_obs.hist_err(:,s));
    dat_obs.hist_cor(:,s) = mean(dat_obs.resp==1).*dat_obs.hist_cor(:,s)./sum(dat_obs.hist_cor(:,s));
    
    
    dat_obs.stat_conf(:,s) = [mean(dat_obs.conf) std(dat_obs.conf)];
    dat_obs.stat_conf_err(:,s) = [mean(dat_obs.conf(dat_obs.resp==0)) std(dat_obs.conf(dat_obs.resp==0))];
    dat_obs.stat_conf_cor(:,s) = [mean(dat_obs.conf(dat_obs.resp==1)) std(dat_obs.conf(dat_obs.resp==1))];
    
    %========================================================
    %% Simulate DV
    %========================================================
    fprintf(' * Subject: %s\n',opts.subjects{s});
    
    file = ['models/' opts.type '/' opts.subjects{s}  '_race_' opts.type '_ro-' opts.readout '_confsim.mat'];
    
    load(file, 'confparams_act','confparams_obs', ...
        'confestimation_act','confestimation_obs', ...
        'out_act','out_obs','ll_act','ll_obs' ,...
        'sim_act','sim_obs');
    %load(['models/' opts.type{m} '/' opts.subjects{s} '_race_' opts.type{m} '_conf.mat'],'confparams_post','confparams_2nd','confparams_nodec','confparams_dec','ll_post','ll_2nd','ll_nodec','ll_dec','allloglik','sim_act','sim_obs');
    
    
    %========================================================
    %% stats for active
    %========================================================
    for model = 1:sum(cellfun(@length,confparams_act))
        
        [~,~,~,simul_act{model}.auc(s)] = perfcurve(sim_act.resp,confestimation_act{model},1,'TVals',0:0.25:1);
        
        simul_act{model}.meanrtcor(s) = mean(sim_act.rt(sim_act.resp==1));
        simul_act{model}.meanrterr(s) = mean(sim_act.rt(sim_act.resp==0));
        simul_act{model}.meanacc(s) = mean(sim_act.resp);
        
        simul_act{model}.rtdistr_err(:,s) = hist(sim_act.rt(sim_act.resp==0,:),opts.rtsupport_hist);
        simul_act{model}.rtdistr_cor(:,s) = hist(sim_act.rt(sim_act.resp==1,:),opts.rtsupport_hist);
        simul_act{model}.rtdistr_err(:,s) = mean(sim_act.resp==0).*simul_act{model}.rtdistr_err(:,s)./sum(simul_act{model}.rtdistr_err(:,s));
        simul_act{model}.rtdistr_cor(:,s) = mean(sim_act.resp==1).*simul_act{model}.rtdistr_cor(:,s)./sum(simul_act{model}.rtdistr_cor(:,s));
        
        simul_act{model}.hist_err(:,s) = ksdensity(confestimation_act{model}(sim_act.resp==0,:),opts.confsupport_hist);
        simul_act{model}.hist_cor(:,s) = ksdensity(confestimation_act{model}(sim_act.resp==1,:),opts.confsupport_hist);
        simul_act{model}.hist_err(:,s) = mean(sim_act.resp==0).*simul_act{model}.hist_err(:,s)./sum(simul_act{model}.hist_err(:,s));
        simul_act{model}.hist_cor(:,s) = mean(sim_act.resp==1).*simul_act{model}.hist_cor(:,s)./sum(simul_act{model}.hist_cor(:,s));
        
          simul_act{model}.stat_conf(:,s) = [mean(confestimation_act{model}) std(confestimation_act{model})];
        simul_act{model}.stat_conf_err(:,s) = [mean(confestimation_act{model}(sim_act.resp==0)) std(confestimation_act{model}(sim_act.resp==0))];
        simul_act{model}.stat_conf_cor(:,s) = [mean(confestimation_act{model}(sim_act.resp==1)) std(confestimation_act{model}(sim_act.resp==1))];
        %%
        [~,~,~,simul_obs{model}.auc(s)] = perfcurve(sim_obs.resp,confestimation_obs{model},1,'TVals',0:0.25:1);
        
        simul_obs{model}.meanrtcor(s) = mean(sim_obs.rt(sim_obs.resp==1));
        simul_obs{model}.meanrterr(s) = mean(sim_obs.rt(sim_obs.resp==0));
        simul_obs{model}.meanacc(s) = mean(sim_obs.resp);
        
        simul_obs{model}.rtdistr_err(:,s) = hist(sim_obs.rt(sim_obs.resp==0,:),opts.rtsupport_hist);
        simul_obs{model}.rtdistr_cor(:,s) = hist(sim_obs.rt(sim_obs.resp==1,:),opts.rtsupport_hist);
        simul_obs{model}.rtdistr_err(:,s) = mean(sim_obs.resp==0).*simul_obs{model}.rtdistr_err(:,s)./sum(simul_obs{model}.rtdistr_err(:,s));
        simul_obs{model}.rtdistr_cor(:,s) = mean(sim_obs.resp==1).*simul_obs{model}.rtdistr_cor(:,s)./sum(simul_obs{model}.rtdistr_cor(:,s));
        
        simul_obs{model}.hist_err(:,s) = ksdensity(confestimation_obs{model}(sim_obs.resp==0,:),opts.confsupport_hist);
        simul_obs{model}.hist_cor(:,s) = ksdensity(confestimation_obs{model}(sim_obs.resp==1,:),opts.confsupport_hist);
        simul_obs{model}.hist_err(:,s) = mean(sim_obs.resp==0).*simul_obs{model}.hist_err(:,s)./sum(simul_obs{model}.hist_err(:,s));
        simul_obs{model}.hist_cor(:,s) = mean(sim_obs.resp==1).*simul_obs{model}.hist_cor(:,s)./sum(simul_obs{model}.hist_cor(:,s));
      
        simul_obs{model}.stat_conf(:,s) = [mean(confestimation_obs{model}) std(confestimation_obs{model})];
        simul_obs{model}.stat_conf_err(:,s) = [mean(confestimation_obs{model}(sim_obs.resp==0)) std(confestimation_obs{model}(sim_obs.resp==0))];
        simul_obs{model}.stat_conf_cor(:,s) = [mean(confestimation_obs{model}(sim_obs.resp==1)) std(confestimation_obs{model}(sim_obs.resp==1))];
        
        fprintf(' ** Model %d [%s] AUC: %.2f / %.2f\n',model,opts.subjects{s},simul_act{model}.auc(s),simul_obs{model}.auc(s));
        
    end
    %%
    savemodel_act=1;
    savemodel_obs=2;
    
    l1 = length(confestimation_act{savemodel_act});
    l2 = length(confestimation_obs{savemodel_obs});
    side1 = round(rand(l1,1))==1;
    side2 = round(rand(l1,1))==1;
    resp1 = side1; resp1(sim_act.resp==0) = ~resp1(sim_act.resp==0);
    resp2 = side2; resp2(sim_obs.resp==0) = ~resp2(sim_obs.resp==0);
    
    d = zeros(l1+l2,18);
    d(:,1) = 1:(l1+l2);
    d(1:l1,2) = 0;
    d((l1+1):(l1+l2),2) = 1;
    d(1:l1,3) = side1+11;
    d((l1+1):(l1+l2),3) = side2+11;
    d(:,4) = 10;
    d(1:l1,5) = resp1+11;
    d((l1+1):(l1+l2),5) = resp2+11;
    d(1:l1,6) = confestimation_act{savemodel_act};
    d((l1+1):(l1+l2),6) = confestimation_obs{savemodel_obs};
    d(1:l1,7) = sim_act.resp==0;
    d((l1+1):(l1+l2),7) = sim_obs.resp==0;
    d(1:l1,10) = sim_act.rt;
    d((l1+1):(l1+l2),10) = sim_obs.rt;
    d(:,11) = 1500;
    d(:,13) = 6500;
    d(:,14) = 500;
    if ~exist(['sim/'   opts.type '_ro-' opts.readout])
        mkdir(['sim/'   opts.type '_ro-' opts.readout]);
    end
    dlmwrite(['sim/'   opts.type '_ro-' opts.readout '/' opts.subjects{s} '_sim_eegfmri_24102018_180000_timings.txt'],d);
    
end