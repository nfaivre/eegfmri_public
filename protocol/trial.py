'''
Created on Mar 14, 2014

@author: mpereira
'''

import random



class Trial():
	def __init__(self,side,fix_duration,stim_duration,fb_duration,t1_duration,t2_duration,iti_duration,qdir,qinit):
		self.TARGET_LEFT = 11
		self.TARGET_RIGHT = 12

		self.FIX = fix_duration
		self.STIM = stim_duration
		self.FB = fb_duration
		self.RT1 = -1
		self.T1 = t1_duration
		self.RT2 = -1
		self.T2 = t2_duration
		self.ITI = iti_duration

		
		self.bias = 0
		self.side = side
		self.state = -1

		self.resp1 = 0
		self.resp2 = 0
		self.error = -1

		self.Qinit = qinit
		self.Qdir = qdir

		self.disposition_left = []
		self.disposition_right = []

	# GET
	def getFix(self):
		return self.FIX

	def getStimOn1(self):
		return self.STIM
	
	def getStimOn2(self):
		return self.FB

	def getRt1(self):
		return self.RT1

	def getTiming1(self):
		return self.T1

	def getRt2(self):
		return self.RT2

	def getTiming2(self):
		return self.T2

	def getIti(self):
		return self.ITI
	
	def getBias(self):
		return self.bias
	
	def getSide(self):
		return self.side

	def getIntens(self):
		bias = self.bias
		if self.side == self.TARGET_LEFT:
			bias *= -1
		return bias

	def getResp1(self):
		return self.resp1

	def getResp2(self):
		return self.resp2

	def getError(self):
		return self.error

	def getState(self):
		return self.state

	def getDisposition(self):
		return [self.disposition_left,self.disposition_right]

	def getQinit(self):
		return self.Qinit

	def getQdir(self):
		return self.Qdir

	def setRt1(self,rt1):
		self.RT1 = rt1

	def setRt2(self,rt2):
		self.RT2 = rt2

	def setBias(self,bias):
		self.bias = bias

	def setResp1(self,r1):
		self.resp1 = r1
		if not (self.resp1 == self.side):
			self.error = 1
		else:
			self.error = 0
		print "   Response is: " + repr(r1) + "/" + repr(self.side) + ", error: " + repr(self.error)

	def setResp2(self,r2):
		self.resp2 = r2

	def setState(self,state):
		self.state = state
	
	def setDisposition(self,l,r):
		#print " -- Disposition: " + repr(len(l)) + "/" + repr(len(r))
		self.disposition_left = l
		self.disposition_right = r
