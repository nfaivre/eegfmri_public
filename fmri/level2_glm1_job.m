%-----------------------------------------------------------------------
% Job saved on 09-Dec-2018 12:17:09 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6685)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
function matlabbatch = level2_glm1_job(opts,type);

if strcmp(type,'glm1')
    basedir = opts.glm1dir;
else
    basedir = opts.glm2dir;
end
nsuj = length(opts.subjects);
outdir = [basedir '/level2/con' opts.con1 opts.con2];
matlabbatch{1}.spm.stats.factorial_design.dir{1} = outdir;
if ~exist(outdir,'dir');
    mkdir(outdir);
end
fprintf('Output dir is %s\n',outdir);
%%
matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(1).scans = cell(nsuj,1);
matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(2).scans = cell(nsuj,1);
for s=1:nsuj
    file1 = [basedir '/all/' opts.subjects{s} '_con_' opts.con1 '.nii'];
    file2 = [basedir '/all/' opts.subjects{s} '_con_' opts.con2 '.nii'];
    matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(1).scans{s} = [file1 ',1'];
    matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(2).scans{s} = [file2 ',1'];
    if ~exist(file1,'file')
        fprintf('File not found: %s\n',file1);
    end
     if ~exist(file2,'file')
        fprintf('File not found: %s\n',file2);
    end
end

matlabbatch{1}.spm.stats.factorial_design.des.anova.dept = 0;
matlabbatch{1}.spm.stats.factorial_design.des.anova.variance = 1;
matlabbatch{1}.spm.stats.factorial_design.des.anova.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.anova.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {''};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
matlabbatch{2}.spm.stats.review.spmmat(1) = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.review.display.matrix = 1;
matlabbatch{2}.spm.stats.review.print = 'ps';
matlabbatch{3}.spm.stats.fmri_est.spmmat(1) = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{3}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{3}.spm.stats.fmri_est.method.Classical = 1;
matlabbatch{4}.spm.stats.con.spmmat(1) = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{4}.spm.stats.con.consess{1}.tcon.name = 'conf-act+';
matlabbatch{4}.spm.stats.con.consess{1}.tcon.weights = [1];
matlabbatch{4}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{4}.spm.stats.con.consess{2}.tcon.name = 'conf-act-';
matlabbatch{4}.spm.stats.con.consess{2}.tcon.weights = [-1];
matlabbatch{4}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
if 1
matlabbatch{4}.spm.stats.con.consess{3}.tcon.name = 'conf-obs+';
matlabbatch{4}.spm.stats.con.consess{3}.tcon.weights = [0 1];
matlabbatch{4}.spm.stats.con.consess{3}.tcon.sessrep = 'none';
matlabbatch{4}.spm.stats.con.consess{4}.tcon.name = 'conf-obs-';
matlabbatch{4}.spm.stats.con.consess{4}.tcon.weights = [0 -1];
matlabbatch{4}.spm.stats.con.consess{4}.tcon.sessrep = 'none';
matlabbatch{4}.spm.stats.con.consess{5}.tcon.name = 'conf-act > conf-obs';
matlabbatch{4}.spm.stats.con.consess{5}.tcon.weights = [1 -1];
matlabbatch{4}.spm.stats.con.consess{5}.tcon.sessrep = 'none';
matlabbatch{4}.spm.stats.con.consess{6}.tcon.name = 'conf-obs > conf-act';
matlabbatch{4}.spm.stats.con.consess{6}.tcon.weights = [-1 1];
matlabbatch{4}.spm.stats.con.consess{6}.tcon.sessrep = 'none';
end
matlabbatch{4}.spm.stats.con.delete = 0;
matlabbatch{5}.spm.stats.results.spmmat(1) = cfg_dep('Contrast Manager: SPM.mat File', substruct('.','val', '{}',{4}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{5}.spm.stats.results.conspec.titlestr = '';
matlabbatch{5}.spm.stats.results.conspec.contrasts = 1;
matlabbatch{5}.spm.stats.results.conspec.threshdesc = 'none';
matlabbatch{5}.spm.stats.results.conspec.thresh = 0.001;
matlabbatch{5}.spm.stats.results.conspec.extent = 0;
matlabbatch{5}.spm.stats.results.conspec.conjunction = 1;
matlabbatch{5}.spm.stats.results.conspec.mask.none = 1;
matlabbatch{5}.spm.stats.results.units = 1;
matlabbatch{5}.spm.stats.results.print = 'ps';
matlabbatch{5}.spm.stats.results.write.none = 1;
