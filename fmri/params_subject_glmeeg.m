%-----------------------------------------------------------------------
% Job saved on 29-May-2017 13:23:41 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6685)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
function [matlabbatch,outputdir] = params_subject_glmeeg(params)
outputdir = [params.outputdir '/' params.subject '_' params.pmod '_' params.trial];
create = '';
if ~exist(outputdir,'dir')
    create = '[ CREATED ]';
    mkdir(outputdir);
end
fprintf('Outputdir is %s %s\n',outputdir,create);
matlabbatch{1}.spm.stats.fmri_spec.dir = {outputdir};
matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = params.tr;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = params.microtime;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = params.microtime/2;
%%
%% get functional images for each session
% get all session directories

% BAD TRIALS
bt = load('../misc/badtrials.mat');

eegtr1 = load([params.eegmatdir '/' params.subject  '_act_type2.mat']);
eegtr2 = load([params.eegmatdir '/' params.subject  '_mon_type2.mat']);

d1 = dir([params.datadir params.subject '/fmri/1_*_Run*_s4_*']);

irun = 0;
for f1=1:length(d1)
    % get all files in the session directory
    d2 = dir([params.datadir params.subject '/fmri/' d1(f1).name '/swr*.nii']);
    % count the number of volumes
    hdr = spm_data_hdr_read([params.datadir params.subject '/fmri/' d1(f1).name '/' d2(1).name]);
    
    % get timings
    split = strsplit(d1(f1).name,'_');
    run = lower(split{3});
    
    fprintf('[%s]',run);
    ncond = maketimings_glmeeg(params,run, bt, eegtr1, eegtr2);
    if all(ncond > 10)%all([nmot,nmon] >= 2) && (maxd < 30);
        
        irun = irun+1;
        % preallocate
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).scans = cell(length(hdr),1);
        
        % add files
        for f2 = 1:min(length(hdr),387)
            matlabbatch{1}.spm.stats.fmri_spec.sess(irun).scans{f2} = [params.datadir params.subject '/fmri/' d1(f1).name '/' d2(1).name ',' num2str(f2) ];
        end
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
        
        d3 = dir([params.timingdir '/' params.subject '_' run '_' params.trial '_' params.pmod '*.mat']);
        d4 = dir([params.datadir params.subject '/fmri/' d1(f1).name '/rp_*.txt']);
        filemulti = [params.timingdir '/' d3(1).name];
        filereg = [params.datadir '/' params.subject '/fmri/' d1(f1).name '/' d4(1).name];
        fprintf('\n');
        
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).multi = {filemulti};
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).regress = struct('name', {}, 'val', {});
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).multi_reg = {filereg};
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).hpf = 128;
    else
        fprintf(' - EXCLUDE\n');
       % fprintf('For %s, excluding (errors %d / %d, bad: %d)\n',run,nmot,nmon,nbad);
    end
end
    fprintf('\n');

%%

matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = params.deriv;
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
matlabbatch{1}.spm.stats.fmri_spec.mask = {''};
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';


matlabbatch{2}.spm.stats.review.spmmat(1) = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.review.display.matrix = 1;
matlabbatch{2}.spm.stats.review.print = 'ps';
matlabbatch{3}.spm.stats.fmri_est.spmmat(1) = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{3}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{3}.spm.stats.fmri_est.method.Classical = 1;
matlabbatch{4}.spm.stats.con.spmmat(1) = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));

%%
w = cell(1,8);
contrasts = cell(1,8);
%      mot e1 e2 r mon e1 e2 r
w{1} = [1  0  0  0  0  0  0  0 ]; contrasts{1} = 'Motor > 0';
w{2} = [0  0  0  0  1  0  0  0 ]; contrasts{2} = 'Monitoring > 0';
w{3} = [1  0  0  0 -1  0  0  0 ]; contrasts{3} = 'Motor > Monitoring';
w{4} = [-1 0  0  0  1  0  0  0 ]; contrasts{4} = 'Monitoring > Motor';
w{5} = [0  1  0  0  0  0  0  0 ]; contrasts{5} = 'Motor (eeg1) > 0';
w{6} = [0  0  0  0  0  1  0  0 ]; contrasts{6} = 'Monitoring (eeg1) > 0';
w{7} = [0  0  1  0  0  0  0  0 ]; contrasts{7} = 'Motor (eeg2) > 0';
w{8} = [0  0  0  0  0  0  1  0 ]; contrasts{8} = 'Monitoring (eeg2) > 0';
w{9} = [0  0  0  1  0  0  0  0 ]; contrasts{9} = 'Motor (rt) > 0';
w{10} =[0  0  0  0  0  0  0  1 ]; contrasts{10} = 'Monitoring (rt) > 0';
%w{9} =[0  1  0  0 -1   0 ]; contrasts{9} = 'Confidence Motor > Monitoring';
%w{10} =[0  0  1  0  0   -1 ]; contrasts{10} = 'RT Motor > Monitoring';

ctype = {'tcon','tcon','tcon','tcon','tcon','tcon','tcon','tcon','tcon','tcon'};%,'tcon','tcon'};

for c = 1:length(w)
    weights = upsample(w{c}.',length(params.deriv)+1).';
    matlabbatch{4}.spm.stats.con.consess{c}.(ctype{c}).weights = weights;
    matlabbatch{4}.spm.stats.con.consess{c}.(ctype{c}).sessrep = 'replsc';
    matlabbatch{4}.spm.stats.con.consess{c}.(ctype{c}).name = contrasts{c};
end
matlabbatch{4}.spm.stats.con.delete = 1;
