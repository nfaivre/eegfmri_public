function [epochs_to_use, confidence_values, labels_confidence, trialid, epochs_run, trialrun, epochs] = prepare_trials(EEG, data_path, subject, cond, remove_bad_trials, analyze_uncertainty)

    conditions = cellfun(@(x) x{1}, {EEG.epoch.eventcond});
    confidence_values = cellfun(@(x) x{1}, {EEG.epoch.eventconf});

    if (analyze_uncertainty)
        confidence_values = (abs(1-2*abs(confidence_values - 0.5)));
    end

    epochs_to_use = 1:length(EEG.epoch);
    epochs_run = cellfun(@(x)(x{1}),{EEG.epoch(:).eventrun});

    labels_confidence = zeros(size(confidence_values));
    labels_confidence(confidence_values<=0.5) = -1;
    labels_confidence(confidence_values>0.5) = 1;


    
    % remove bad trials according to those found during preprocessing
    % steps.
    if (remove_bad_trials)
        load([data_path '/badtrials.mat']);
        idx_bad_trials = find(allbadtrials{find(strcmp(allsubjects, ['s' num2str(subject)]))} ~= 0);
        trialid = cellfun(@(x) x{1}, {EEG.epoch.eventrealepoch}) + 1;
        run_idx = 48*(cellfun(@(x) x{1}, {EEG.epoch.eventrun})-1);
        trialid = trialid + run_idx;

        trials_to_remove = intersect(idx_bad_trials, trialid);
        trials_to_remove = arrayfun(@(x)(find(trialid == x)), trials_to_remove);

        epochs_to_use(trials_to_remove) = [];
        confidence_values(trials_to_remove) = [];
        labels_confidence(trials_to_remove) = [];
        conditions(trials_to_remove) = [];
        trialid(trials_to_remove) = [];
        epochs_run(trials_to_remove) = [];
    end

    % Find epochs to use from the data
    epochs_to_use(conditions ~= cond) = [];
    confidence_values(conditions ~= cond) = [];
    labels_confidence(conditions ~= cond) = [];
    trialid(conditions ~= cond) = [];
    epochs_run(conditions ~= cond) = [];
    trialrun = epochs_run + (subject==19);
    epochs = EEG.data(:,:,epochs_to_use);


end