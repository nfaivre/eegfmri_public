timings file columns: 
---
1) 	Trial ID
2) 	Trial type (0=motor, 1=monitoring, 11=monitoring but responded)
3)	Stimulus side
4) 	Stimulus bias (difference in #points/2)
5)	Type I response
6) 	Type II response
7)	Error (0=correct, 1=error)
---
8)	Time overall
9)	Time fixation cross 			
10)	Time for type I response (RT1)
11)	Time between stimulus onset and rating screen
12)	Time for type II response (RT2)
13)	Time between trials (ITI)
---
14)	Initial point of the type II rating scale
15)	Direction of the moving cursor for type II ratings
---