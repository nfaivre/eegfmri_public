function batch_subject(subject)

% whether to include the resting state data
params.dorest = 1; 
% whether to also compute timings using the EEG files
params.dotimings = 1;
% the subject's code
params.subject = subject; %'s1';
% the directory containing the raw fMRI data (all sessions)
params.datadir = 'Y:/Data/Exp/';
% the directory containing the SPM12 toolbox
params.spmdir = 'Z:/meaperei/home/git/EEGFMRI_analysis/toolboxes/spm12';
% how many preproc steps to do (max: 5 - recommended)
params.nsteps = 3;
% the number of slices
params.nslices = 64;
% the multiband factor
params.accel = 4;

params.dartel = 1;

params.dartel_template = 'E:\Data\eegfmri\DARTEL\Orig\Template_6.nii';
params.dartel_flowsdir = 'E:\Data\eegfmri\DARTEL\Orig\';
% the TR
if strcmp(params.subject,'z2') || strcmp(params.subject,'p2')
    params.tr = 1.3;
else
    params.tr = 1.28;
end
params.ta = params.tr  - params.tr / (params.nslices/params.accel) ;

% anatomical image
srchstr1 = [params.datadir params.subject '/fmri/1_*mprage*'];
d1 = dir(srchstr1);
if isempty(d1)
    fprintf('ERROR: could not find any files corresponding to %s\n',srchstr1);
end
srchstr2 = [params.datadir params.subject '/fmri/' d1(1).name '/*.nii'];
d2 = dir(srchstr2);
if isempty(d2)
    fprintf('ERROR: could not find any files corresponding to %s\n',srchstr2);
end
if length(d2) > 1
    [~,bestmatch] = min(cellfun(@length,{d2.name}));
    anatfile = d2(bestmatch).name;
    fprintf('WARNING: more than one file corresponding to %s\n',srchstr2);
    fprintf('-> selecting %s\n',anatfile);
else
    anatfile = d2(1).name;
end
params.anatfile = [params.datadir params.subject '/fmri/' d1(1).name '/' anatfile ',1'];

%% DARTEL
srchstr3 = [params.dartel_flowsdir '/u_rc1r' anatfile(1:end-4) '_Template.nii'];
d3 = dir(srchstr3); 
if isempty(d3)
    fprintf('ERROR: could not find any files corresponding to %s\n',srchstr3);
elseif length(d3)==1
    params.dartel_flowfile = [params.dartel_flowsdir '/' d3(1).name];
else
    fprintf('ERROR: more than one file found for %s\n',srchstr3);
end


addpath(params.spmdir);
if params.dartel
    matlabbatch = params_subject_dartel(params);
else
    matlabbatch = params_subject(params);
end
%%
spm_jobman('run',matlabbatch);

% if params.dartel
%     d1 = dir([params.datadir subject '/fmri/1_*_R*_s4_*']);
%     
%     for f1=1:length(d1)
%         % get all files in the session directory
%         d2 = dir([params.datadir subject '/fmri/' d1(f1).name '/rJ*.nii']);
%         
%         for f2 = 1:length(d2)
%             file = [params.datadir subject '/fmri/' d1(f1).name '/' d2(f2).name];
%             fprintf('Subject %s: removing %s\n',subject,file);
%             delete(file);
%         end
%     end
% end