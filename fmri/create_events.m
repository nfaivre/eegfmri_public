%%
config;
addpath(opts.bvatoolboxdir);
bt = load('../misc/badtrials.mat');

datadir = '/Scratch/meaperei/data';
origsubjects = {'s2','s4','s7','s8','s9','s10','s11',...
    's12','s13','s14','s15','s16','s17','s18','s19','s20',...
    's21','s22','s24','s26'};

%origsubjects = origsubjects(15:end);
%opts.subjects = opts.subjects(15:end);

%%
for s=[8,10]%1:length(opts.subjects)
%%
    orig = origsubjects{s};
    files = dir([datadir '/' orig '/eeg/*run*.vhdr']);
    fprintf('Transforming %s to %s\n',orig,opts.subjects{s});
    addtonext = 0;
    for run=1:length(files)
        %%
        nstim = 48;
        
        % for each run, load eeg files
        eegfile = [datadir '/' orig '/eeg/' files(run).name];
        [Fs,~,eeghdr] = bva_readheader(eegfile);
  
        i = strfind(files(run).name,'run');
        runname = files(run).name(i+(0:3));
        
        eegmarkerfile = files(run).name;
        eegmarkerfile(end+(-4:0)) = '.vmrk';
        marker = bva_readmarker2([datadir '/' orig '/eeg/' eegmarkerfile]);
        % remove bad markers
        bad = isnan(marker(1,:));
        marker(:,bad) = [];
        % convert from sample to time in seconds
        trtime = marker(2,marker(1,:)==1128)./Fs;
        % we know the tr so any two markers less than tr/2 apart should be removed
        df = find(diff(trtime) < opts.tr/2);
        trtime(df+1) = [];
        
        % define slice timing
        times = marker(2,:)./Fs;
        times = times - trtime(1);
        
        % trick
        trig = mod(marker(1,:),128);
        
        nscans = length(trtime);
        tr = mode(diff(trtime));
        
        %%
        % index of events
        istims = find(ismember(trig,11:12));
        bad = find(diff(times(istims)) < 2);
        istims(bad+1) = [];
        iresp = find(ismember(trig,7:8));
        bad = find(diff(times(iresp)) < 2);
        iresp(bad+1) = [];
        ifb = find(ismember(trig,17:18));
        bad = find(diff(times(ifb)) < 2);
        ifb(bad+1) = [];
        iresp2 = find(ismember(trig,27:28));
        bad = find(diff(times(iresp2)) < 2);
        iresp2(bad+1) = [];

        %%
        
        behavfile = dir([datadir '/' orig '/behav/' orig '_' runname '*_timings.txt']);
        behav = dlmread([datadir '/' orig '/behav/' behavfile(end).name]);
        
        if length(istims) < nstim
            disp([num2str(length(istims)) ' triggers, trimming, please check']);
            nstim = length(istims);
        end
        
        bCond = behav(1:nstim,2);
        bSide = behav(1:nstim,3);
        bDelta = behav(1:nstim,4);
        bType1 = behav(1:nstim,5);
        bType2 = behav(1:nstim,6);
        bErr = behav(1:nstim,7);
        bTrialOnset = behav(1:nstim,8)*1e-3;
        tFix = behav(1:nstim,9)*1e-3;
        tRT1 = behav(1:nstim,10)*1e-3;
        tRT2 = behav(1:nstim,12)*1e-3;
        
        
        %%
        isub = strcmp(bt.allsubjects,orig);
        irun = find(bt.allruns{isub}==str2double(runname(end)));
        if isempty(irun)
            fprintf('/!\\ run %d: replacing irun by %d, please double check\n',run,str2double(runname(end)));
            irun = str2double(runname(end));
            addtonext = 1;
        end
        if irun ~= str2double(runname(end))
            fprintf('/!\\ run %d: there seems to be a run (%d) missing in badtrials.mat\n',run,str2double(runname(end)));
        end
        idx = (1:nstim)+(irun-1)*nstim;
        badTr = bt.allbadtrials{isub}(idx) > 0;
        if numel(badTr) == 0
            badTr = false(nstim,1);
        end
        badTr = badTr | bType2<0 | bType1==0 | tRT1 < 0.2 | tRT1 > 0.5;
        if numel(badTr) == 0
            badTr = false(nstim,1);
        end
        
        
        id1 = (bCond==0) & (badTr==0);
        id2 = (bCond==1) & (badTr==0);
        
        id3 = (bCond==0) & (badTr==1);
        id4 = (bCond==1) & (badTr==1);
        
        
        onset = times(istims).';
        duration = times(istims).'*0;
        trial_type = cell(nstim.',1);
        for i=find(id1).'
            trial_type{i} = 'act';
        end
        for i=find(id2).'
            trial_type{i} = 'obs';
        end
        if sum(id3)
            for i=find(id3).'
                trial_type{i} = 'act-bad';
            end
        end
        if sum(id4)
            for i=find(id4).'
                trial_type{i} = 'obs-bad';
            end
        end
        response_time = tRT1;
        dots_difference = bDelta;
        stim_side = cell(nstim,1);
        for i=find(bSide==11).'
            stim_side{i} = 'left';
        end
        for i=find(bSide==12).'
            stim_side{i} = 'right';
        end
        choice_accuracy = cell(nstim,1);
        for i=find(bSide==bType1).'
            choice_accuracy{i} = 'cor';
        end
        for i=find(bSide~=bType1).'
            choice_accuracy{i} = 'err';
        end
        
        confidence = bType2;
        response_time2 = tRT2;
        
        
        t = table(onset,duration,trial_type,response_time,dots_difference, ...
            stim_side,choice_accuracy,confidence,response_time2);
        
        %%
        eventfile1 = [opts.dicomdir '/' opts.subjects{s} '/ses-001/func/' ...
            opts.subjects{s} '_ses-001_task-main_run-00' num2str(irun+addtonext) '_events.csv'...
            ];
        fprintf(' * writing %s\n',eventfile1);
        
        
        writetable(t,eventfile1,'Delimiter','\t');
        
        eventfile2 = [opts.dicomdir '/' opts.subjects{s} '/ses-001/func/' ...
            opts.subjects{s} '_ses-001_task-main_run-00' num2str(irun+addtonext) '_events.tsv'...
            ];
        movefile(eventfile1,eventfile2);
        
        %%
        
        continue

        eegheaderfile = [datadir '/' orig '/eeg/' files(run).name];
        eegmarkerfile = [datadir '/' orig '/eeg/' files(run).name];
        eegmarkerfile(end+(-4:0)) = '.vmrk';
        eegfile = [datadir '/' orig '/eeg/' files(run).name];
        eegfile(end+(-4:-1)) = '.eeg';
        eegfile(end) = [];
        
        file2 = [opts.dicomdir '/' opts.subjects{s} '/ses-001/eeg/' ...
            opts.subjects{s} '_ses-001_task-main_run-00' num2str(irun) '_eeg'];
        
        fprintf(' * Copying %s to %s\n',eegheaderfile,[file2 '.vhdr']);
        copyfile(eegheaderfile,[file2 '.vhdr']);
        fprintf(' * Copying %s to %s\n',eegmarkerfile,[file2 '.vmrk']);
        copyfile(eegmarkerfile,[file2 '.vmrk']);
        fprintf(' * Copying %s to %s\n',eegfile,[file2 '.eeg']);
        copyfile(eegfile,[file2 '.eeg']);
        
        %%
        f = dir([datadir '/' orig '/fmri/1_*Run' num2str(irun) '*/rp_*Run' num2str(irun) '*.txt']);
        if ~isempty(f)
        rpfile = [f(1).folder '/' f(1).name];
        rpfile2 = [opts.dicomdir '/' opts.subjects{s} '/ses-001/func/' ...
            opts.subjects{s} '_ses-001_task-main_run-00' num2str(irun) '_headmove.txt'];
        fprintf(' * Copying %s to %s\n',rpfile,rpfile2);
        copyfile(rpfile,rpfile2);
        
        f = dir([datadir '/' orig '/fmri/1_*Run' num2str(irun) '*/swr*Run' num2str(irun) '*.nii']);
        niftifile = [f(1).folder '/' f(1).name];
        niftifile2 = [opts.dicomdir '/' opts.subjects{s} '/ses-001/func/' ...
            opts.subjects{s} '_ses-001_task-main_run-00' num2str(irun) '_preproc.nii'];
        fprintf(' * Copying %s to %s\n',niftifile,niftifile2);
        copyfile(niftifile,niftifile2);
        end
        
    end
end