'''
Created on Mar 14, 2014

@author: mpereira
'''
import os, time

from datetime import date
from ctypes import *
#from pylink import *

import pygame
from pygame.locals import *
import ConfigParser
from shutil import copyfile
#import SerialWriter

class Interface():
	def __init__(self,directory):
		pygame.init()
		self.directory = directory
		self.d = date.today()
		self.t = time.strftime("%H%M%S")

	def exit(self):
		self.close_parport()
		self.close_eyetracker()
		self.close_file()

	def init_parport(self,parport):
		self.parport = parport
		if self.parport == 1:
			self.lpt = windll.inpout32
			self.lptaddress = 0xD020
		elif self.parport==2:
			self.lpt = pylpttrigger
			self.lpt.open(0,10,0)
		# Arduino
		elif self.parport==3:
			self.lpt = SerialWriter.SerialWriter("/dev/ttyACM0")
			self.lpt.write(0)
		else:
			print "=============================================================="
			print "=    /!\ You are starting the protocol without triggers      ="
			print "=============================================================="

	def close_parport(self):
		if self.parport>=1:
			self.lpt.close()
		
	def sendtrigger(self,level):
		if self.parport==1:
			self.lpt.Out32(self.lptaddress, int(level)) # sets corresponding pins to on
		elif self.parport ==2:
			self.lpt.signal(level) 
		elif self.parport ==3:			
			self.lpt.write(level) 

	def load_config_file(self):
		
		config_file = "protocol_configuration.ini"
		config = ConfigParser.RawConfigParser(allow_no_value=True)
		config.read("./" + config_file)
		
		return config

	def copy_config_file(self,filename):
		copyfile("./protocol_configuration.ini",filename + '_config.txt')

	def dump_files(self,filename,num_trials_run,array_timings,array_stimuli):
		print "Dumping timings in " + filename + "_timings.txt..."
		f = open(filename + '_timings.txt', 'w')
		for x in range (0, num_trials_run):
			f.write( str(x) + ',' + str(array_timings[0,x]) + ',' + str(array_timings[1,x]) + \
					',' + str(array_timings[2,x]) + ',' + str(array_timings[3,x]) + \
					',' + str(array_timings[4,x]) + ',' + str(array_timings[5,x]) + \
					',' + str(array_timings[6,x]) + ',' + str(array_timings[7,x]) + \
					',' + str(array_timings[8,x]) + ',' + str(array_timings[9,x]) + \
					',' + str(array_timings[10,x]) + ',' + str(array_timings[11,x]) + \
					',' + str(array_timings[12,x]) + ',' + str(array_timings[13,x]) + \
					',' + str(array_timings[14,x]) + ',' + str(array_timings[15,x]) + '\n')
		f.close()

		print "Dumping stimuli in " + filename + "_stimuli.txt..."
		f = open(filename + '_stimuli.txt', 'w')
		for x in range (0, num_trials_run):
			for j in array_stimuli[:,x]:
				f.write( str(j) + ',')
			f.write('\n')
		f.close()


	def dump_file_state(self,filename,bias):
		print "Dumping state in " + filename + ".txt..."
		f = open(filename + '.txt', 'w')
		f.write( str(bias) + '\n')
		f.close()

	def load_file_state(self,filename):
		print "Loading state from " + filename + ".txt..."
		if not(os.path.isfile(filename + '.txt')):
			print "File not found, skipping"
			return -1
		f = open(filename + '.txt','r')#'timings.txt', 'r')
		l = [ map(float,line.split(',')) for line in f ]
		f.close()
		return l[0][0]

	def load_sound(self,filename):
		class NoneSound:
			def play(self): pass
		if not pygame.mixer or not pygame.mixer.get_init():
			print "Cannot find mixer"
			return NoneSound()
		fullname = os.path.join('media', filename)
		try:
			sound = pygame.mixer.Sound(fullname)
		except pygame.error, message:
			print 'Cannot load sound:', fullname
			raise SystemExit, message
		return sound

	# ====================================
	#		EyeLink Eyetracker
	# ====================================
	def eyetracker_newtrial(self,nbtr):
		if self.eyetracker:
			# EYE-TRACKER SEND TRIAL TRIGGER
			#This supplies the title at the bottom of the eyetracker display
			message ="record_status_message 'Trial %d'"%(nbtr)
			getEYELINK().sendCommand(message)           
			#Always send a TRIALID message before starting to record.
			#EyeLink Data Viewer defines the start of a trial by the TRIALID message.  
			#This message is different than the start of recording message START that is logged when the trial recording begins. 
			#The Data viewer will not parse any messages, events, or samples, that exist in the data file prior to this message.
			msg = "TRIALID %s"%(nbtr)
			getEYELINK().sendMessage(msg)

	def eyetracker_endtrial(self):
		if self.eyetracker:
			getEYELINK().sendMessage("TRIAL OK")
	
	def eyetracker_sync(self,synctime):
		if self.eyetracker:
			getEYELINK().sendMessage("SYNCTIME " + repr(synctime))

	def eyetracker_driftcorr(self,screen_rect):
		if self.eyetracker:
			if not getEYELINK().isConnected():
				print "eyelink tracker disconnected"
			else:
				mode = self.eyetracker_getmode()
				screen_width = screen_rect[0]
				screen_height = screen_rect[1]
			#getEYELINK().startDriftCorrect(int(screen_width/2),int(screen_height/2))
			#getEYELINK().waitForModeReady(500)
				print "calling drift correction in mode " + repr(mode)
			
				error = getEYELINK().doDriftCorrect(int(screen_width/2),int(screen_height/2),1,1)
				if (error == 27): 
					getEYELINK().doTrackerSetup()
			#except:
		#		getEYELINK().doTrackerSetup()

	def eyetracker_getmode(self):
		if self.eyetracker:
			return getEYELINK().getCurrentMode()

	def init_eyetracker(self,eyetracker,screen_rect):    
		# eyeconf is [saccade_velocity_threshold saccade_acceleration_threshold saccade_motion_threshold saccade_pursuit_fixup]
		self.eyetracker = eyetracker
		if self.eyetracker:
			# Need to add graphics using pyQt or pygame
			screen_width = screen_rect[0]
			screen_height = screen_rect[1]

			# Initialization of the eyetracker
			self.eyelinktracker = EyeLink(None)

			pylink.openGraphics((screen_width,screen_height),32)
			#Opens the EDF file.
			self.edfFileName = "jmerror.edf"
			getEYELINK().openDataFile(self.edfFileName)

			pylink.flushGetkeyQueue()
			getEYELINK().setOfflineMode()
			#Gets the display surface and sends a mesage to EDF file;
			getEYELINK().sendCommand("screen_pixel_coords =  0 0 %d %d" %(screen_width, screen_height))
			getEYELINK().sendMessage("DISPLAY_COORDS  0 0 %d %d" %(screen_width, screen_height))

			tracker_software_ver = 0
			eyelink_ver = getEYELINK().getTrackerVersion()
			print " ++ Found Eyelink software version: " + repr(eyelink_ver)
			if eyelink_ver == 3:
				tvstr = getEYELINK().getTrackerVersionString()
				vindex = tvstr.find("EYELINK CL")
				tracker_software_ver = int(float(tvstr[(vindex + len("EYELINK CL")):].strip())) 

			if eyelink_ver>=2:
				getEYELINK().sendCommand("select_parser_configuration 0")
				if eyelink_ver == 2: #turn off scenelink camera stuff
					getEYELINK().sendCommand("scene_camera_gazemap = NO")
			else:
				getEYELINK().sendCommand("saccade_velocity_threshold = 35")
				getEYELINK().sendCommand("saccade_acceleration_threshold = 9500")

			getEYELINK().sendCommand("file_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON,INPUT")
			if tracker_software_ver>=4:
				getEYELINK().sendCommand("file_sample_data  = LEFT,RIGHT,GAZE,AREA,GAZERES,STATUS,HTARGET,INPUT")
			else:
				getEYELINK().sendCommand("file_sample_data  = LEFT,RIGHT,GAZE,AREA,GAZERES,STATUS,INPUT")

			getEYELINK().sendCommand("calibration_area_proportion = 0.5 0.6")
			getEYELINK().sendCommand("validation_area_proportion = 0.4 0.5")

			# set link data (used for gaze cursor) 
			getEYELINK().sendCommand("link_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,BUTTON,INPUT")
			if tracker_software_ver>=4:
				getEYELINK().sendCommand("link_sample_data  = LEFT,RIGHT,GAZE,GAZERES,AREA,STATUS,HTARGET,INPUT")
			else:
				getEYELINK().sendCommand("link_sample_data  = LEFT,RIGHT,GAZE,GAZERES,AREA,STATUS,INPUT")

			getEYELINK().sendCommand("button_function 5 'accept_target_fixation'")

			print "Adjusting eye tracker parameters..."

			if(getEYELINK().isConnected() and not getEYELINK().breakPressed()):
				print "Trying to start eye tracker calibration..."
				getEYELINK().doTrackerSetup()
				print "Eye Tracker calibration done"
				pylink.closeGraphics()
			else:
				print "Eye Tracker connection problem"
			error = self.eyelinktracker.startRecording(1,1,0,0)
			#if error:
			print "eyetracker returned " + repr(error)
		else:
			print "no eye tracker enabled in config file"

	def close_eyetracker(self):
		if self.eyetracker:
			self.eyelinktracker.stopRecording()
			print "Eye tracker stopped"
			# File transfer and cleanup!
			getEYELINK().setOfflin
			msecDelay(500)
			#Close the file and transfer it to Display PC
			getEYELINK().closeDataFile()
			getEYELINK().receiveDataFile("jmerror.edf", self.filename + ".edf")
			getEYELINK().close();

			print "File transfered"
			#Close the experiment graphics  
			pylink.closeGraphics()
