from PyQt4 import QtGui, QtCore
import math, random, os
import numpy as np
class GuiFunc:
	
	def __init__(self,w,h,pxmm,d,alpha_e,alpha_s,alpha_c, cw, alpha_h, full_screen,col_sel,col_unsel_dark,col_unsel_light,col_toolate, used_filled_scale,photodiode=0):
		

		self.photodiode = photodiode
		
		self.color_diode = QtGui.QColor(0,0,0)	
		self.color_diode_white = True

		self.used_filled_scale = used_filled_scale

		self.col_sel = col_sel
		self.col_unsel_light = col_unsel_light
		self.col_unsel_dark = col_unsel_dark
		self.col_toolate = col_toolate

		self.alpha_e = math.radians(alpha_e)/2
		self.alpha_s = math.radians(alpha_s)/2
		self.alpha_c = math.radians(alpha_c)/2
		self.alpha_h = math.radians(alpha_h)/2

		self.w = w
		self.h = h
		self.x = self.w/2
		self.y = self.h/2
		self.d = d

		self.dx = d*math.tan(self.alpha_e)/pxmm
		print "d is " + repr(d) + " mm, so e is " + repr(d*math.tan(self.alpha_e)) + " mm = " + repr(self.dx) + " px (tan(" + repr(self.alpha_e) + ") = " + repr(math.tan(self.alpha_e)) + ")"
		self.dy = 0
		self.t = 2*(d*math.tan(self.alpha_s)/pxmm - self.dx)

		self.cross_width = cw
		self.cross_length = 2*d*math.tan(self.alpha_c)/pxmm

		self.hand = d*math.tan(self.alpha_h)/pxmm
		


		self.npt = 100
		self.nptline = 10
		self.onpts_left = [0]*(self.npt)
		self.onpts_right = [0]*(self.npt)
		self.pen = QtGui.QPen(self.col_sel, 9, QtCore.Qt.SolidLine)
		# self.color_uncertainty = [ QtGui.QColor(255.0,0.0,0.0,255.0), \
		# 		QtGui.QColor(255.0,144.0,2.0,255.0), \
		# 		QtGui.QColor(255.0,212.0,12.0,255.0), \
		# 		QtGui.QColor(122.0,248.0,7.0,255.0), \
		# 		QtGui.QColor(0.0,255.0,0.0,255.0)]
		self.color_uncertainty = [ \
				QtGui.QColor(152.0,21.0,0.0,255.0), \
				QtGui.QColor(199.0,126.0,122.0,255.0), \
				QtGui.QColor(255.0,255.0,255.0,255.0), \
				QtGui.QColor(116.0,183.0,130.0,255.0), \
				QtGui.QColor(0.0,118.0,8.0,255.0), \
				]

		#Fonts			
		self.qfont = QtGui.QFont('Ubuntu', 20)
		self.qfont.setStyle(QtGui.QFont.StyleNormal)
		self.qfont.setWeight(QtGui.QFont.Normal)

		self.qsmallfont = QtGui.QFont('Ubuntu', 12)
		self.qsmallfont.setStyle(QtGui.QFont.StyleNormal)
		self.qsmallfont.setWeight(QtGui.QFont.Normal)

		print "Setting up GUI (" + repr(self.w) + " x " + repr(self.h) + " with " + repr(pxmm) + " px/mm) with stimulus at " + repr(self.dx) + " px (" + repr(alpha_e) + " deg), cross at " + repr(self.cross_length) + " px (" + repr(alpha_c) + " deg)"
	def StimDefine(self,nptleft,nptright):
		#print " -- Define stimulus: " + repr(nptleft) + "/" + repr(nptright)
		self.onpts_left = [0]*(self.npt-nptleft) + [1]*nptleft
		random.shuffle(self.onpts_left)
		self.onpts_right = [0]*(self.npt-nptright) + [1]*nptright
		random.shuffle(self.onpts_right)
		return [self.onpts_left,self.onpts_right]

	def StimLoad(self,left,right):
		self.onpts_left = left
		self.onpts_right = right
	
	def StimLeft(self,qp,on):
		
		basex = self.x - self.dx - self.t/2.0
		basey = self.y - self.dy - self.t/2.0

		qp.setBrush(self.col_unsel_dark)
		qp.drawRect( basex, basey , self.t, self.t)
		if on:
			tstep = self.t/20
			diam = tstep*1.5
			for p in range(0,self.npt):
				px = basex  + tstep + (p % self.nptline)*tstep*2 - diam*0.5
				py = basey  + tstep +  math.floor(p/10)*tstep*2 - diam*0.5
				if self.onpts_left[p]:
					qp.setBrush(self.col_sel)
				else:
					qp.setBrush(self.col_unsel_dark)
				qp.drawEllipse( px,py, diam,diam)
		# PHOTODIODE
		if self.photodiode:
			if (on):
				self.color_diode = QtGui.QColor(255,255,255)	
			else:
				self.color_diode = QtGui.QColor(0,0,0)	

			qp.setBrush(self.color_diode)
			qp.drawRect( 500, 500 , 100, 100)
	
	def StimRight(self,qp,on):
		basex = self.x + self.dx - self.t/2.0
		basey = self.y - self.dy - self.t/2.0
		qp.setBrush(self.col_unsel_dark)
		qp.drawRect( basex, basey , self.t, self.t)
		if on:
			tstep = self.t/20
			diam = tstep*1.5
			for p in range(0,self.npt):
				px = basex + tstep + (p % self.nptline)*tstep*2 - diam*0.5
				py = basey + tstep + math.floor(p/10)*tstep*2 - diam*0.5
				if self.onpts_right[p]:
					qp.setBrush(self.col_sel)
				else:
					qp.setBrush(self.col_unsel_dark)
				qp.drawEllipse( px,py, diam, diam)

		# PHOTODIODE
		if self.photodiode:
			if (on):
				self.color_diode = QtGui.QColor(255,255,255)	
			else:
				self.color_diode = QtGui.QColor(0,0,0)	

			qp.setBrush(self.color_diode)
			qp.drawRect( 500, 500 , 100, 100)

	def StimMask(self,qp):
		
		basex_l = self.x - self.dx - self.t/2.0
		basex_r = self.x + self.dx - self.t/2.0
		basey = self.y - self.dy - self.t/2.0

		qp.setBrush(self.col_unsel_dark)
		qp.drawRect( basex_l, basey , self.t, self.t)
		qp.drawRect( basex_r, basey , self.t, self.t)

		tstep = self.t/20
		diam = tstep*1.5
		qp.setBrush(self.col_sel)
		for p in range(0,self.npt):
			px = basex_l  + tstep + (p % self.nptline)*tstep*2 - diam*0.5
			py = basey  + tstep +  math.floor(p/10)*tstep*2 - diam*0.5
			qp.drawEllipse( px,py, diam,diam)
			px = basex_r + tstep + (p % self.nptline)*tstep*2 - diam*0.5
			py = basey + tstep + math.floor(p/10)*tstep*2 - diam*0.5
			qp.drawEllipse( px,py, diam, diam)	


	def FixationCross(self,qp,on,rot=0):
	
		qp.setPen(QtCore.Qt.NoPen)

		# Cross

		if on:
			qp.setBrush(self.col_unsel_light)	
		else:
			qp.setBrush(self.col_unsel_dark)

		if rot==0:
			qp.drawRect(self.w/2.0 - self.cross_length/2.0, self.h/2.0  - self.cross_width/2.0, self.cross_length, self.cross_width)
			qp.drawRect(self.w/2.0 - self.cross_width/2.0, self.h/2.0 - self.cross_length/2.0, self.cross_width, self.cross_length)
		else:
			w = self.cross_width*2
			l = self.cross_length/2
			x0 = self.w/2.0
			y0 = self.h/2.0

			w = (self.cross_width)/math.sqrt(2)
			l1 =  self.cross_length/2
			l2 = l -  w
			poly = QtGui.QPolygonF()

			poly.append(QtCore.QPointF(x0-l1,y0-l2)) # left up branch 1
			poly.append(QtCore.QPointF(x0-l2,y0-l1)) # left up branch 2
			poly.append(QtCore.QPointF(x0,y0-w))
			poly.append(QtCore.QPointF(x0+l2,y0-l1)) # right up branch 1
			poly.append(QtCore.QPointF(x0+l1,y0-l2)) # right up branch 2
			poly.append(QtCore.QPointF(x0+w,y0))
			poly.append(QtCore.QPointF(x0+l1,y0+l2)) # right down branch 1
			poly.append(QtCore.QPointF(x0+l2,y0+l1)) # right down branch 2
			poly.append(QtCore.QPointF(x0,y0+w))
			poly.append(QtCore.QPointF(x0-l2,y0+l1)) # left down branch 1
			poly.append(QtCore.QPointF(x0-l1,y0+l2)) # left down branch 2
			poly.append(QtCore.QPointF(x0-w,y0))

			qp.drawPolygon(poly)

		#lse:

	def Question(self,qp,sel,pos=0,target=-1):


		qp.setFont(self.qfont)
		#if toolate:
		#	qp.setPen(QtGui.QColor(self.col_toolate))
		#else:
		qp.setPen(QtGui.QColor(self.col_unsel_light))
		# incentive text
		if target==-1:
			qp.drawText(self.x-300,self.y-200, 600, 50, QtCore.Qt.AlignCenter, 'Was the last action correct?')
			qp.setBrush(QtCore.Qt.NoBrush)
			qp.setPen(self.color_uncertainty[-1])
			qp.drawText(self.x-60,self.y-160, 120, 50, QtCore.Qt.AlignCenter, ' Correct ')
			qp.setPen(self.color_uncertainty[0])
			qp.drawText(self.x-60,self.y+110, 120, 50, QtCore.Qt.AlignCenter, 'Incorrect')
		else:
			qp.drawText(self.x-300,self.y-150, 600, 50, QtCore.Qt.AlignCenter, 'Press when cursor reaches white line')\
		

		qp.setPen(QtCore.Qt.NoPen)
		qp.setBrush(QtGui.QColor(20.0,20.0,20.0))
		#qp.drawRect(self.x-30,self.ConvertPos(1.00),60,37)
		#qp.drawRect(self.x-30,self.ConvertPos(0.00),60,37)
		qp.setBrush(QtCore.Qt.NoBrush)
		qp.setPen(QtGui.QColor(self.col_unsel_light))
		qp.drawRect(self.x-30,self.ConvertPos(1.00),60,self.ConvertPos(0.00)-self.ConvertPos(1.00))

		#qfont.setWeight(QtGui.QFont.Bold)
		qp.setFont(self.qsmallfont)
		cursor = self.ConvertPos(pos)
		idx = 0
		cursorcolor = QtGui.QColor(self.color_uncertainty[2])
		for i in map(float,np.linspace(0,1,5)):
			color = QtGui.QColor(self.color_uncertainty[idx])
			qp.setPen(color)
			qp.drawText(self.x-100,self.ConvertPos(i)-25, 80, 50, QtCore.Qt.AlignCenter, repr(int(i*100))+"%")
			#qp.drawLine(self.x-30,self.ConvertPos(i),self.x+30,self.ConvertPos(i))
			
			idx = idx+1
		qp.setPen(cursorcolor)	
		qp.drawLine(self.x-30,self.ConvertPos(0.5),self.x+30,self.ConvertPos(0.5))

		if target >= 0:
			qp.drawLine(self.x-30,self.ConvertPos(target),self.x+100,self.ConvertPos(target))
		#ITURRATE: Change line for rectangle

		
		if (self.used_filled_scale):
			
			if (cursor>=self.ConvertPos(0.5)):
			# Incorrect
				#qp.setBrush(QtGui.QColor(50+(255-50)*(cursor-self.ConvertPos(0.5))/(self.ConvertPos(0.00)-self.ConvertPos(1.00)),0,0))	
				red = 2*float(cursor-self.ConvertPos(0.5))/(self.ConvertPos(0.00)-self.ConvertPos(1.00))
				if red > 1:
					red = 1
				elif red < 0:
					red = 0
				#print "red: " + repr(red)
				qp.setBrush(QtGui.QColor(255,255*(1-red),255*(1-red)))
			else:
			# Correct
				#qp.setBrush(QtGui.QColor(0,50+(255-50)*(self.ConvertPos(0.5)-cursor)/(self.ConvertPos(0.00)-self.ConvertPos(1.00)),0))	
				green = 2*float(self.ConvertPos(0.5)-cursor)/(self.ConvertPos(0.00)-self.ConvertPos(1.00))
				if green > 1:
					green = 1
				elif green < 0:
					green = 0
				#print "green: " + repr(green)
				qp.setBrush(QtGui.QColor(255*(1-green),255,255*(1-green)))	
			qp.setPen(QtCore.Qt.NoPen)
			qp.drawRect(self.x-29,self.ConvertPos(0.5),58, cursor-self.ConvertPos(0.5))

		else:
			qp.drawLine(self.x-30,cursor,self.x+30,cursor)


		

		rate = int(pos*100)
		if rate > 100:
			rate = 100

		qp.setPen(cursorcolor)	
		qp.drawText(self.x,cursor-25, 120, 50, QtCore.Qt.AlignCenter, repr(int(rate))+"%")

		#qfont = QtGui.QFont('Ubuntu', 12)
		#qfont.setStyle(QtGui.QFont.StyleNormal)
		#qfont.setWeight(QtGui.QFont.Normal)
		#qp.setFont(qfont)
		#for i in range(0,5):
		#	qp.drawText(self.x-35+(i-2)*50,self.y+150, 70, 20, QtCore.Qt.AlignCenter, txt1[i])
		#	qp.drawText(self.x-35+(i-2)*50,self.y+170, 70, 20, QtCore.Qt.AlignCenter, txt2[i])
		#	qp.drawText(self.x-35+(i-2)*50,self.y+190, 70, 20, QtCore.Qt.AlignCenter, txt3[i])
	def ConvertPos(self,pos):
		return int(self.y+(0.5-pos)*150)

	def QuestionError(self,qp,sel):
		# Text			
		qfont = QtGui.QFont('Ubuntu', 20)
		qfont.setStyle(QtGui.QFont.StyleNormal)
		qfont.setWeight(QtGui.QFont.Normal)
		qp.setFont(qfont)

		qp.setPen(QtGui.QColor(self.col_unsel_light))
		# incentive text
		qp.drawText(self.x-300,self.y-100, 600, 50, QtCore.Qt.AlignCenter, 'Did you just make an error?')
		qp.setBrush(QtCore.Qt.NoBrush)

	
		txt = ['No/??','Error']
		for i in range(0,2):
			qp.drawText(self.x-15+(i-1)*100+25,self.y+100, 80, 50, QtCore.Qt.AlignCenter, txt[i])

			if i==sel:
				qp.drawRect(self.x-15+(i-1)*100+25,self.y+100, 80, 50)			

	def DisplayText(self,qp,txt):
		# Text			
		qfont = QtGui.QFont('Ubuntu', 20)
		qfont.setStyle(QtGui.QFont.StyleNormal)
		qfont.setWeight(QtGui.QFont.Normal)
		qp.setFont(qfont)
		qp.setPen(QtGui.QColor(self.col_unsel_light))
		qp.drawText(self.x-300,self.y-200, 600, 50, QtCore.Qt.AlignCenter, txt)

	def SpeedIncentive(self,qp,txt1,txt2):
		# Text			
		qfont = QtGui.QFont('Ubuntu', 20)
		qfont.setStyle(QtGui.QFont.StyleNormal)
		qfont.setWeight(QtGui.QFont.Normal)
		qp.setFont(qfont)
		qp.setPen(QtGui.QColor(self.col_unsel_light))
		
		self.DisplayText(qp,'10 s rest')
		# incentive text
		qp.drawText(self.x-300,self.y-100, 600, 50, QtCore.Qt.AlignCenter, txt1)
		# trials left text
		qp.drawText(self.x-200,self.y+100, 400, 50, QtCore.Qt.AlignCenter, txt2)

		#qp.drawText(self.x-300,self.y+400, 600, 50, QtCore.Qt.AlignCenter, 'Press left or right when ready')

	def DrawHand(self,qp):
		

		hand_left_pixmap = QtGui.QPixmap(os.path.join('media','hand_l.png'))
		scale_h = self.hand/hand_left_pixmap.width()
		hand_left_pixmap_sc = hand_left_pixmap.scaledToWidth(self.hand)
		hand_left = QtGui.QLabel(qp)
		hand_left.setPixmap(hand_left_pixmap_sc)
		hlx = self.w / 2 - self.dx - hand_left_pixmap.width()*scale_h
		hly = self.h / 2 #- self.t
		hlw = hand_left_pixmap.width()
		hlh = hand_left_pixmap.height()
		hand_left.setGeometry(hlx,hly,hlw*scale_h,hlh*scale_h)
		hand_left.setVisible(0)

		nohand_left_pixmap = QtGui.QPixmap(os.path.join('media','nohand_l.png'))
		scale_h = self.hand/nohand_left_pixmap.width()
		nohand_left_pixmap_sc = nohand_left_pixmap.scaledToWidth(self.hand)
		nohand_left = QtGui.QLabel(qp)
		nohand_left.setPixmap(nohand_left_pixmap_sc)
		nohand_left.setGeometry(hlx,hly,hlw*scale_h,hlh*scale_h)
		nohand_left.setVisible(0)

		hand_right_pixmap = QtGui.QPixmap(os.path.join('media','hand_r.png'))
		scale_h = self.hand/hand_right_pixmap.width()
		hand_right_pixmap_sc = hand_right_pixmap.scaledToWidth(self.hand)
		hand_right = QtGui.QLabel(qp)
		hand_right.setPixmap(hand_right_pixmap_sc)
		hrx = self.w / 2 + self.dx
		hry = self.h / 2 # - self.t
		hrw = hand_right_pixmap.width()
		hrh = hand_right_pixmap.height()
		hand_right.setGeometry(hrx,hry,hrw*scale_h,hrh*scale_h)
		hand_right.setVisible(0)
		
		nohand_right_pixmap = QtGui.QPixmap(os.path.join('media','nohand_r.png'))
		scale_h = self.hand/nohand_right_pixmap.width()
		nohand_right_pixmap_sc = nohand_right_pixmap.scaledToWidth(self.hand)
		nohand_right = QtGui.QLabel(qp)
		nohand_right.setPixmap(nohand_right_pixmap_sc)
		nohand_right.setGeometry(hrx,hry,hrw*scale_h,hrh*scale_h)
		nohand_right.setVisible(0)

		return [hand_left,hand_right,nohand_left,nohand_right]

	# LEGACY: 
	def QuestionDiscrete(self,qp,sel,pos=0):
		# Text			
		qfont = QtGui.QFont('Ubuntu', 20)
		qfont.setStyle(QtGui.QFont.StyleNormal)
		qfont.setWeight(QtGui.QFont.Normal)
		qp.setFont(qfont)
		#if toolate:
		#	qp.setPen(QtGui.QColor(self.col_toolate))
		#else:
		qp.setPen(QtGui.QColor(self.col_unsel_light))
		# incentive text
		qp.drawText(self.x-300,self.y-100, 600, 50, QtCore.Qt.AlignCenter, 'Was the last action correct?')
		qp.setBrush(QtCore.Qt.NoBrush)
		qp.drawText(self.x-50-220,self.y+100, 120, 50, QtCore.Qt.AlignCenter, 'Incorrect')
		qp.drawText(self.x-50+200,self.y+100, 120, 50, QtCore.Qt.AlignCenter, 'Correct')
		
		txt1 = ['Sure','Might','No','Might','Sure']
		txt2 = ['it is','be','idea','be','it is']
		txt3 = ['wrong','wrong','','correct','correct']
		qfont.setWeight(QtGui.QFont.Bold)
		qp.setFont(qfont)
		for i in range(0,5):
			qp.setPen(QtGui.QColor(self.color_uncertainty[i]))
			qp.drawText(self.x-15+(i-2)*50,self.y+100, 30, 50, QtCore.Qt.AlignCenter, repr(i-2))
		
			if i==sel:
				qp.drawRect(self.x-15+(i-2)*50,self.y+100, 30, 50)