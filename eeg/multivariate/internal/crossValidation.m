function [ outputTrain, outputTest, indices ] = crossValidation( inputFeatures, numberOfFolds, indicesin )

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Calculating folds and classifying if requested
        if (nargin == 2)
            indices=crossvalind('Kfold',inputFeatures.labels,numberOfFolds);
        else
%             disp('Using previous indices');
            indices = indicesin;
        end
        for folds = 1:numberOfFolds
            test = (indices == folds); train = ~test;
            trainMatrix.labels = inputFeatures.labels(train);      
            trainMatrix.conf = inputFeatures.conf(train);      
			trainMatrix.features = inputFeatures.features(train, :);      
            
            testMatrix.labels = inputFeatures.labels(test);      
            testMatrix.conf = inputFeatures.conf(test);      
			testMatrix.features = inputFeatures.features(test, :);      
            
            outputTrain.(['fold' num2str(folds)]) = trainMatrix;
            outputTest.(['fold' num2str(folds)]) = testMatrix;
        end    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


end


