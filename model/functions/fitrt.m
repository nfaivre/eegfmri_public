%========================================================
%   fitrt.m fits a race model to rt and choice accuracy (rt)
%
%   Pereira, Faivre, Iturrate et al., 2019,
%   Disentangling the origins of confidence in speeded
%      perceptual judgments through multimodal imaging
%
%   preregistration: https://osf.io/a5qmv/
%   gitlab: https://gitlab.com/meta-eegfmri/
%
%   Michael Pereira <michael.pereira@epfl.ch>
%   25/09/2018
%========================================================
function [ params,stat,out] = fitrt( rt,resp, params, opts)

stat = NaN;
out = [];

% save original choice accuracy and rt for the plot
resp_plot = resp;
rt_plot = rt;

% censoring
bad = isnan(resp) | rt < opts.cens(1) | rt > opts.cens(2) | resp < 0 | resp > 1;
resp(bad) = [];
rt(bad) = [];

if opts.doplot
    glm = fitglm(rt,resp,'Distribution','binomial');
    rtlist = linspace(0,1,100).';
    glmvals = glm.predict(rtlist);
    acc = mean(resp);
end

% invert rt for errors (standard practice)
rt(resp==0) = -rt(resp==0);


%% Find which parameters to fit
fields = {'bound','drift','ndtime','ndtime_std','urg','startvar','decay','drift_std',};
p = 0;
param_val = [];
param_id = {};
for f=1:length(fields)
    if params.(fields{f}).fit
        p = p+1;
        % inverse transform of parameters
        param_val(p) = params.(fields{f}).itrans(params.(fields{f}).val);
        param_id{p} = fields{f};
    end
end

%% Fitting procedure
options = optimset('Display',opts.optimizationdisplay1,'FinDiffRelStep',1e-2);
[p, stat] = fminsearch(@fitfunc,param_val,options);

%% Transform parameters back
for f=1:length(param_id)
    params.(param_id{f}).val = params.(param_id{f}).trans(p(f));
end

    function stat = fitfunc(p)
        
        % Reset random number generator
        rng('default');
        
        % transform parameters back to "random-walk space"
        newparams = params;
        for f=1:length(param_id)
            newparams.(param_id{f}).val = newparams.(param_id{f}).trans(p(f));
        end
        
        % simulate random walks and corresponding choice accuracy, rt
        [sim,dv] = evacc(newparams,opts);
        
        % censor
        simbad = isnan(sim.resp) | sim.rt < opts.cens(1) | sim.rt > opts.cens(2);
        sim.rt(simbad) = [];
        sim.resp(simbad) = [];
        
        if opts.doplot
            simglm = fitglm(sim.rt,sim.resp,'Distribution','binomial');
            simglmvals = simglm.predict(rtlist);
            simacc = mean(sim.resp);
        end
        
        % invert rt for errors (similar to data)
        sim.rt(sim.resp==0) = -sim.rt(sim.resp==0);
        
        % estimate likelihood with Kolmogorov-Smirnov test
        [~,ks] = kstest2(sim.rt,rt);
        % negative log-likelihood
        stat = -log(ks);
        
        % save current state
        out.ll = stat;
        out.params = params;
        
        
        %% plot some stuff
        if opts.doplot
            
            t = linspace(0,opts.L,opts.L./opts.dt)+newparams.ndtime.val;
            figure(100); clf;
            subplot(131); hold on;
            [k,s] = ksdensity(rt_plot(resp_plot==0));
            plot(s,nanmean(resp_plot==0).*k,'r');
            [k,s] = ksdensity(rt_plot(resp_plot==1));
            plot(s,nanmean(resp_plot==1).*k,'g');
            if sum(sim.resp==0) && ~all(isnan(sim.rt(sim.resp==0)))
                [k,s] = ksdensity(-sim.rt(sim.resp==0));
                plot(s,nanmean(sim.resp==0).*k,'r--');
            end
            if sum(sim.resp==1) && ~all(isnan(sim.rt(sim.resp==1)))
                [k,s] = ksdensity(sim.rt(sim.resp==1));
                plot(s,nanmean(sim.resp==1).*k,'g--');
            end
            xlim([0,opts.L]);
            xlabel('Response time [s]');
            ylabel('Probability');
            grid on
            
            subplot(132); hold on;
            plot(t,mean(dv.dv(sim.resp==1,:,1),1),'r');
            plot(t,mean(dv.dv(sim.resp==1,:,2),1),'g');
            plot(t,mean(dv.dv(sim.resp==0,:,1),1),'r--');
            plot(t,mean(dv.dv(sim.resp==0,:,2),1),'g--');
            plot(xlim(),[0 0],'k','LineWidth',2);
            b = newparams.bound.val*newparams.startvar.val;
            fill([t(1)-0.025 t(1)+0.025 t(1)+0.025 t(1)-0.025 t(1)-0.025],[0,0,b,b,0],'k','FaceAlpha',0.2);
            bnd = newparams.bound.val-sim.urg;
            if length(bnd) == 1
                bnd = bnd*(t*0+1);
            end
            plot(t,bnd,'k--','LineWidth',2);
            
            xlim([0,opts.L]); ylim([-newparams.bound.val,4*newparams.bound.val])
            xlabel('Decision time [s]');
            ylabel('Mean decision variable');
            grid on
            title(sprintf('#NaNs: %d/%d',opts.N-length(sim.resp),opts.N));
            
            subplot(133); hold on;
            plot(rtlist,glmvals,'k','LineWidth',2);
            plot(rtlist,simglmvals,'k--','LineWidth',2);
            xlabel('RT');
            ylabel('Choice accuracy');
            title(sprintf('Accuarcy: data: %.2f sim: %.2f',acc,simacc));
            pause(0.01);
        end
    end

end

