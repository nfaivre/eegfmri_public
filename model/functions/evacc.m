%========================================================
%   evacc.m evidence accumulation
%   simulates a race accumulation
%
%   Pereira, Faivre, Iturrate et al., 2019, 
%   Disentangling the origins of confidence in speeded 
%      perceptual judgments through multimodal imaging
%
%   preregistration: https://osf.io/a5qmv/  
%   gitlab: https://gitlab.com/meta-eegfmri/  
%      
%   Michael Pereira <michael.pereira@epfl.ch>
%   25/09/2018
%========================================================
function [ sim, dvstim, dvresp ] = evacc( params, opts )
%EVACC
%   Desc
%   
%   Michael Pereira <michael.pereira@epfl.ch>
%   20/08/2018

%========================================================
%% Prepare parameters
%========================================================
K = round(opts.L/opts.dt);
K2 = round(opts.L2/opts.dt);

% extract parameters
v = params.drift.val;
B = params.bound.val;
tnd = params.ndtime.val;
tnd_var = params.ndtime_std.val;
zvar = params.startvar.val;

sim.irt = nan(opts.N,1);
sim.resp = nan(opts.N,1);

t = linspace(0,opts.L,K);

if nargout > 1
    % only allocate if needed
    dvstim = struct();
    dvstim.dv = nan(opts.N,round(opts.L/opts.dt),2);
    dvstim.dv_urg = nan(opts.N,round(opts.L/opts.dt),2);
end

if nargout > 2
    % only allocate if needed
    dvresp = struct();
    dvresp.dv = nan(opts.N,K2+1,2);
    dvresp.dv_urg = nan(opts.N,K2+1,2);
end

if params.urg.val ~= 0
    % generate urgency signal
    urg = B.*(1-exp(-(t).^2./params.urg.val.^2));
else
    urg = 0;
end

if params.decay.val ~= 0
    % generate decaying drift
    v = v.*exp(-(t./params.decay.val).^2);
end


if opts.doprint
    fprintf('B=%.2f, v=%.2f, v_std=%.2f, tnd=%.2f, stnd=%.3f, urg=%.3f, dec=%.3f, zvar=%.3f, confslope=%.2f, confbias=%.2f\n',...
        B,params.drift.val,params.drift_std.val,tnd,tnd_var,params.urg.val,params.decay.val,zvar,params.confslope.val,params.confbias.val);
end

for i=1:opts.N
    
    %========================================================
    %% DRIFT DIFFUSION
    %========================================================

    % generate a new drift value
    vnew = v + v.*params.drift_std.val*randn(1);
   
        
    % define a random starting point between 0 and B*Zvar
    % for each accumulator
    z = B*zvar*rand(1,2);
    % samples of drift+diffusion from a bi-variate normal distribution
    %samples(:,1) = -vnew + randn(1,K);
    %samples(:,2) = vnew + randn(1,K);
    samples = [-vnew + randn(1,K) ; vnew + randn(1,K)].';
    % d.v. is the cumulative sum of samples
    dv_ = bsxfun(@plus,z,cumsum(samples));
    
    % add urgency signal for collapsing bounds
    dvbnd_ = bsxfun(@plus,dv_,urg);
    
    % take the maximum of each accumulator
    [m,r] = max(dvbnd_,[],2);
    
    % subtract the boundary
    m = m-B;
    
    %========================================================
    %% BOUND CROSSING
    %========================================================

    
    % find when the maximum of each accumulator first reaches the bound
    rt_ = find(m>0,1,'first');
    
    if ~isempty(rt_)
        
        % save RT and response
        sim.irt(i) = rt_;
        sim.resp(i) = r(rt_);
        
        if nargout > 2
            % record d.v. time-locked to decision
            if rt_ < (K - K2)
                % we save both the d.v. and the d.v. + urgency signal (if
                % any)
                dvresp.dv(i,:,:) = dv_(rt_+(0:K2),:,:);
                dvresp.dv_urg(i,:,:) = dvbnd_(rt_+(0:K2),:,:);
            else
                sim.irt(i) = NaN;
                sim.resp(i) = NaN;
            end
        end
    end
    if nargout > 1
        % save d.v.
        dvstim.dv(i,:,:) = dv_;
        dvstim.dv_urg(i,:,:) = dvbnd_;
    end
    
end
%========================================================
%% POST-PROCESSING
%========================================================

% add non-decision time and random non-decisional time variability to
% response times
sim.rt = sim.irt*1e-3 + tnd + tnd_var.*randn(size(sim.irt));

% make response 0-1
sim.resp = sim.resp-1;

% save urgency signal template
sim.urg = urg;
end

