%=======================================
%   config.m
%
%   Michael Pereira <michael.pereira@epfl.ch>
%   2017
%=======================================

opts = struct();
opts.scriptsdir = '/Scratch/meaperei/eegfmri/analysis_public/fmri/';
opts.spmdir = '/Scratch/meaperei/toolboxes/spm12/';
opts.dicomdir = '/Scratch/meaperei/BIDS/eegfmri/derivatives/fmriprep/';
opts.bvatoolboxdir = '/Scratch/meaperei/toolboxes/bva/';

opts.glm1dir = '/Scratch/meaperei/eegfmri/results/glm1final/'; 
opts.glm2dir = '/Scratch/meaperei/eegfmri/results/glm2final/';
opts.eventdir = '/Scratch/meaperei/eegfmri/results/events/';
opts.eegdecodingdir = '/Scratch/meaperei/eegfmri/analysis_public/eeg/multivariate/results_decoding/';

opts.subjects = {'sub-02','sub-04','sub-07','sub-08','sub-09','sub-10','sub-11',...
    'sub-12','sub-13','sub-14','sub-15','sub-16','sub-17','sub-18','sub-19','sub-20',...
    'sub-21','sub-22','sub-24','sub-26'};


% how many preproc steps to do (max: 5 - recommended)
opts.nsteps = 3;
% the number of slices
opts.nslices = 64;
% the multiband factor
opts.accel = 4;

opts.tr = 1.28;

% whether to include the resting state data
opts.dorest = 1; 

opts.microtime = 16;
opts.deriv = [1 1];

%opts.timebase = (-200:4:600)./1000;
opts.earlywin = [0,0.2];
opts.latewin = [0.2,0.45];