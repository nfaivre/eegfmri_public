%-----------------------------------------------------------------------
% Job saved on 08-May-2017 10:46:42 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6685)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
%%
function matlabbatch = params_subject(params)
%%

nsess = 0;
%% get functional images for each session
% get all session directories
srchstr1 = [params.datadir params.subject '/fmri/1_*_Run*_s4_*'];
d1 = dir(srchstr1);
if isempty(d1)
    fprintf('ERROR: could not find any files corresponding to %s\n',srchstr1);
end
exclude = false(1,length(d1));
% preallocate
matlabbatch{1}.spm.spatial.realign.estwrite.data = cell(1,length(d1)+params.dorest);
for f1=1:length(d1)
    nsess = nsess+1;
    % get all files in the session directory
    srchstr2 = [params.datadir params.subject '/fmri/' d1(f1).name '/J*nii'];
    d2 = dir(srchstr2);
    if isempty(d2)
        fprintf('ERROR: could not find any files corresponding to %s\n',srchstr2);
    end
    % count the number of volumes
    hdr = spm_data_hdr_read([params.datadir params.subject '/fmri/' d1(f1).name '/' d2(1).name]);
    % preallocate
    matlabbatch{1}.spm.spatial.realign.estwrite.data{f1} = cell(length(hdr),1);
    % add files
    for f2 = 1:length(hdr)
        matlabbatch{1}.spm.spatial.realign.estwrite.data{f1}{f2} = [params.datadir params.subject '/fmri/' d1(f1).name '/' d2(1).name ',' num2str(f2) ];
    end
    
  
    fprintf(' + Adding %s (%d volumes)\n',d1(f1).name,length(hdr));
   
end
%% get functional images for rest
if params.dorest
    % get all session directories
    srchstr1 = [params.datadir params.subject '/fmri/1_*_Rest*_s4_*'];
    d1 = dir(srchstr1);
    if isempty(d1)
        fprintf('ERROR: could not find any files corresponding to %s\n',srchstr1);
    end
    for f1=1:length(d1)
        nsess = nsess+1;
        % get all files in the session directory
        srchstr2 = [params.datadir params.subject '/fmri/' d1(f1).name '/*.nii'];
        d2 = dir(srchstr2);
        if isempty(d2)
            fprintf('ERROR: could not find any files corresponding to %s\n',srchstr2);
        end
        % count the number of volumes
        hdr = spm_data_hdr_read([params.datadir params.subject '/fmri/' d1(f1).name '/' d2(1).name]);
        % preallocate
        matlabbatch{1}.spm.spatial.realign.estwrite.data{nsess} = cell(length(hdr),1);
        % add files
        for f2 = 1:length(hdr);
            matlabbatch{1}.spm.spatial.realign.estwrite.data{nsess}{f2} = [params.datadir params.subject '/fmri/' d1(f1).name '/' d2(1).name ',' num2str(f2) ];
        end
        fprintf(' + Adding %s (%d volumes)\n',d1(f1).name,length(hdr));
    end
end

%%
if params.nsteps >= 1
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.quality = 0.9;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.sep = 4;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.fwhm = 5;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.rtm = 1;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.interp = 2;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.wrap = [0 0 0];
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.weight = '';
matlabbatch{1}.spm.spatial.realign.estwrite.roptions.which = [2 1];
matlabbatch{1}.spm.spatial.realign.estwrite.roptions.interp = 4;
matlabbatch{1}.spm.spatial.realign.estwrite.roptions.wrap = [0 0 0];
matlabbatch{1}.spm.spatial.realign.estwrite.roptions.mask = 1;
matlabbatch{1}.spm.spatial.realign.estwrite.roptions.prefix = 'r';
end
% for s=1:nsess
%     matlabbatch{2}.spm.temporal.st.scans{s}(1) = cfg_dep(['Realign: Estimate & Reslice: Resliced Images (Sess ' num2str(s) ')'], substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','sess', '()',{s}, '.','rfiles'));
% end
% matlabbatch{2}.spm.temporal.st.nslices = 16;
% matlabbatch{2}.spm.temporal.st.tr = params.tr;
% matlabbatch{2}.spm.temporal.st.ta = 1.21875;
% matlabbatch{2}.spm.temporal.st.so = [959.99999999 0 639.99999998 79.99999999 720 160 799.99999999 239.99999999 880 399.99999999 1040.00000001 480.00000001 1120 560 1199.99999998 320.00000001 959.99999999 0 639.99999998 79.99999999 720 160 799.99999999 239.99999999 880 399.99999999 1040.00000001 480.00000001 1120 560 1199.99999998 320.00000001 959.99999999 0 639.99999998 79.99999999 720 160 799.99999999 239.99999999 880 399.99999999 1040.00000001 480.00000001 1120 560 1199.99999998 320.00000001 959.99999999 0 639.99999998 79.99999999 720 160 799.99999999 239.99999999 880 399.99999999 1040.00000001 480.00000001 1120 560 1199.99999998 320.00000001];
% matlabbatch{2}.spm.temporal.st.refslice = 0.65;
% matlabbatch{2}.spm.temporal.st.prefix = 'a';
if params.nsteps >= 2
matlabbatch{2}.spm.spatial.coreg.estwrite.ref(1) = cfg_dep('Realign: Estimate & Reslice: Mean Image', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','rmean'));
matlabbatch{2}.spm.spatial.coreg.estwrite.source = {params.anatfile};
matlabbatch{2}.spm.spatial.coreg.estwrite.other = {''};
matlabbatch{2}.spm.spatial.coreg.estwrite.eoptions.cost_fun = 'nmi';
matlabbatch{2}.spm.spatial.coreg.estwrite.eoptions.sep = [4 2];
matlabbatch{2}.spm.spatial.coreg.estwrite.eoptions.tol = [0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
matlabbatch{2}.spm.spatial.coreg.estwrite.eoptions.fwhm = [7 7];
matlabbatch{2}.spm.spatial.coreg.estwrite.roptions.interp = 4;
matlabbatch{2}.spm.spatial.coreg.estwrite.roptions.wrap = [0 0 0];
matlabbatch{2}.spm.spatial.coreg.estwrite.roptions.mask = 0;
matlabbatch{2}.spm.spatial.coreg.estwrite.roptions.prefix = 'r';
end
if params.nsteps >= 3
matlabbatch{3}.spm.spatial.preproc.channel.vols(1) = cfg_dep('Coregister: Estimate & Reslice: Resliced Images', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','rfiles'));
matlabbatch{3}.spm.spatial.preproc.channel.biasreg = 0.001;
matlabbatch{3}.spm.spatial.preproc.channel.biasfwhm = 60;
matlabbatch{3}.spm.spatial.preproc.channel.write = [1 1];
matlabbatch{3}.spm.spatial.preproc.tissue(1).tpm = {[params.spmdir '/tpm/TPM.nii,1']};
matlabbatch{3}.spm.spatial.preproc.tissue(1).ngaus = 1;
matlabbatch{3}.spm.spatial.preproc.tissue(1).native = [1 1];
matlabbatch{3}.spm.spatial.preproc.tissue(1).warped = [1 1];
matlabbatch{3}.spm.spatial.preproc.tissue(2).tpm = {[params.spmdir '/tpm/TPM.nii,2']};
matlabbatch{3}.spm.spatial.preproc.tissue(2).ngaus = 1;
matlabbatch{3}.spm.spatial.preproc.tissue(2).native = [1 1];
matlabbatch{3}.spm.spatial.preproc.tissue(2).warped = [1 1];
matlabbatch{3}.spm.spatial.preproc.tissue(3).tpm = {[params.spmdir '/tpm/TPM.nii,3']};
matlabbatch{3}.spm.spatial.preproc.tissue(3).ngaus = 2;
matlabbatch{3}.spm.spatial.preproc.tissue(3).native = [1 1];
matlabbatch{3}.spm.spatial.preproc.tissue(3).warped = [1 1];
matlabbatch{3}.spm.spatial.preproc.tissue(4).tpm = {[params.spmdir '/tpm/TPM.nii,4']};
matlabbatch{3}.spm.spatial.preproc.tissue(4).ngaus = 3;
matlabbatch{3}.spm.spatial.preproc.tissue(4).native = [1 1];
matlabbatch{3}.spm.spatial.preproc.tissue(4).warped = [1 1];
matlabbatch{3}.spm.spatial.preproc.tissue(5).tpm = {[params.spmdir '/tpm/TPM.nii,5']};
matlabbatch{3}.spm.spatial.preproc.tissue(5).ngaus = 4;
matlabbatch{3}.spm.spatial.preproc.tissue(5).native = [1 1];
matlabbatch{3}.spm.spatial.preproc.tissue(5).warped = [1 1];
matlabbatch{3}.spm.spatial.preproc.tissue(6).tpm = {[params.spmdir '/tpm/TPM.nii,6']};
matlabbatch{3}.spm.spatial.preproc.tissue(6).ngaus = 2;
matlabbatch{3}.spm.spatial.preproc.tissue(6).native = [0 0];
matlabbatch{3}.spm.spatial.preproc.tissue(6).warped = [0 0];
matlabbatch{3}.spm.spatial.preproc.warp.mrf = 1;
matlabbatch{3}.spm.spatial.preproc.warp.cleanup = 1;
matlabbatch{3}.spm.spatial.preproc.warp.reg = [0 0.001 0.5 0.05 0.2];
matlabbatch{3}.spm.spatial.preproc.warp.affreg = 'mni';
matlabbatch{3}.spm.spatial.preproc.warp.fwhm = 0;
matlabbatch{3}.spm.spatial.preproc.warp.samp = 3;
matlabbatch{3}.spm.spatial.preproc.warp.write = [0 1];
end
if params.nsteps >= 4
    for s=1:nsess
       
            matlabbatch{4}.spm.spatial.normalise.write.subj(s).def(1) = cfg_dep('Segment: Forward Deformations', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','fordef', '()',{':'}));
            matlabbatch{4}.spm.spatial.normalise.write.woptions.prefix = 'w';
       
        %matlabbatch{5}.spm.spatial.normalise.write.subj(s).resample(1) = cfg_dep(['Slice Timing: Slice Timing Corr. Images (Sess ' num2str(s) ')'], substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('()',{s}, '.','files'));
        matlabbatch{4}.spm.spatial.normalise.write.subj(s).resample(1) = cfg_dep(['Realign: Estimate & Reslice: Resliced Images (Sess ' num2str(s) ')'], substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','sess', '()',{s}, '.','rfiles'));
    end
    matlabbatch{4}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -70 ;  78 76 85];
    matlabbatch{4}.spm.spatial.normalise.write.woptions.vox = [2 2 2];
    matlabbatch{4}.spm.spatial.normalise.write.woptions.interp = 4;
    
end
if params.nsteps >= 5
    for s=1:nsess
        matlabbatch{4+s}.spm.spatial.smooth.data(1) = cfg_dep(['Normalise: Write: Normalised Images (Subj ' num2str(s) ')'], substruct('.','val', '{}',{4}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('()',{s}, '.','files'));
        matlabbatch{4+s}.spm.spatial.smooth.fwhm = [8 8 8];
        matlabbatch{4+s}.spm.spatial.smooth.dtype = 0;
        matlabbatch{4+s}.spm.spatial.smooth.im = 0;
        matlabbatch{4+s}.spm.spatial.smooth.prefix = 's';
    end
    matlabbatch{4+nsess+1}.spm.spatial.smooth.data(1) = cfg_dep('Segment: mwc1 Images', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','tiss', '()',{1}, '.','mwc', '()',{':'}));
    matlabbatch{4+nsess+1}.spm.spatial.smooth.fwhm = [8 8 8];
    matlabbatch{4+nsess+1}.spm.spatial.smooth.dtype = 0;
    matlabbatch{4+nsess+1}.spm.spatial.smooth.im = 0;
    matlabbatch{4+nsess+1}.spm.spatial.smooth.prefix = 's';
    matlabbatch{4+nsess+2}.spm.spatial.smooth.data(1) = cfg_dep('Segment: mwc2 Images', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','tiss', '()',{2}, '.','mwc', '()',{':'}));
    matlabbatch{4+nsess+2}.spm.spatial.smooth.fwhm = [8 8 8];
    matlabbatch{4+nsess+2}.spm.spatial.smooth.dtype = 0;
    matlabbatch{4+nsess+2}.spm.spatial.smooth.im = 0;
    matlabbatch{4+nsess+2}.spm.spatial.smooth.prefix = 's';
end
if params.nsteps >= 6
matlabbatch{4+nsess+3}.spm.util.render.extract.data(1) = cfg_dep('Segment: c1 Images', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','tiss', '()',{1}, '.','c', '()',{':'}));
matlabbatch{4+nsess+3}.spm.util.render.extract.mode = 3;
matlabbatch{4+nsess+3}.spm.util.render.extract.thresh = 0.5;
end
end