%%%%%%%%%%%%%%%%%%%
%% Load confidence decoding results
%%%%%%%%%%%%%%%%%%%
% In order to run this script you first need the results 
% from the decoding (confidence_decoding_computation.m)
% and the permutations (confidence_decoding_permutations.m)

close all, clear all, clc;

opts.subjects = {'sub-02','sub-04','sub-07','sub-08','sub-09','sub-10','sub-11',...
    'sub-12','sub-13','sub-14','sub-15','sub-16','sub-17','sub-18','sub-19','sub-20',...
    'sub-21','sub-22','sub-24','sub-26'};
% Folders with all the results
folder_decoding = 'results_decoding';
folder_permutations = 'results_permutations';

filename_decoding_suffix = '_eegdecode';
filename_perm_suffix = '_results_permutations';


% Should be the same time window as in the decoding
time_window = [-200:4:600] / 1000;

w1 = find(time_window >= 0.0 & time_window <= 0.200);
w2 = find(time_window >= 0.200 & time_window <= 0.450);

for s=1:length(opts.subjects)
    s
    act = load([folder_decoding '/' opts.subjects{s} '_act' filename_decoding_suffix '.mat']);
    obs = load([folder_decoding '/' opts.subjects{s} '_obs' filename_decoding_suffix '.mat']);
    
    actr2_reg(:,s) = act.r2_reg;
    obsr2_reg(:,s) = obs.r2_reg;
    
    actall(:,s) = act.cor.all;
    actpall(:,s) = act.cor.p_all;
    obsall(:,s) = obs.cor.all;
    obspall(:,s) = obs.cor.p_all;
    
    actcor(:,s) = act.cor.cor;
    actpcor(:,s) = act.cor.p_cor;
    obscor(:,s) = obs.cor.cor;
    obspcor(:,s) = obs.cor.p_cor;
    
    acterr(:,s) = act.cor.err;
    actperr(:,s) = act.cor.p_err;
    obserr(:,s) = obs.cor.err;
    obsperr(:,s) = obs.cor.p_err;
    
    [actpk1_,actipk1_] = findpeaks(actr2_reg(w1,s),'SortStr','descend');
    [actpk2_,actipk2_] = findpeaks(actr2_reg(w2,s),'SortStr','descend');
    [obspk1_,obsipk1_] = findpeaks(obsr2_reg(w1,s),'SortStr','descend');
    [obspk2_,obsipk2_] = findpeaks(obsr2_reg(w2,s),'SortStr','descend');  
        
    actpk1(s) = actpk1_(1); actipk1(s) = w1(1)-1+actipk1_(1);
    actpk2(s) = actpk2_(1); actipk2(s) = w2(1)-1+actipk2_(1);
    obspk1(s) = obspk1_(1); obsipk1(s) = w1(1)-1+obsipk1_(1);
    obspk2(s) = obspk2_(1); obsipk2(s) = w2(1)-1+obsipk2_(1);
    
    tact1(s) = time_window(actipk1(s));
    tact2(s) = time_window(actipk2(s));
    tobs1(s) = time_window(obsipk1(s));
    tobs2(s) = time_window(obsipk2(s));
    
    creg1_act(:,:,s) = corr([act.confidence_values ; act.rt ; act.delta].') - eye(3)        
    creg2_act(:,:,s) = corr([act.pred_reg_train(actipk1(s),:) ; act.pred_reg_train(actipk2(s),:) ; act.rt ; act.delta].') - eye(4)
    creg1_obs(:,:,s) = corr([obs.confidence_values ; obs.rt ; obs.delta].') - eye(3)    ;    
    creg2_obs(:,:,s) = corr([obs.pred_reg_train(obsipk1(s),:) ; obs.pred_reg_train(obsipk2(s),:) ; obs.rt ; obs.delta].') - eye(4)
    
    coract1(s) = actcor(actipk1(s),s);
    erract1(s) = acterr(actipk1(s),s);
    coract2(s) = actcor(actipk2(s),s);
    erract2(s) = acterr(actipk2(s),s);
    pcoract1(s) = actpcor(actipk1(s),s);
    perract1(s) = actperr(actipk1(s),s);
    pcoract2(s) = actpcor(actipk2(s),s);
    perract2(s) = actperr(actipk2(s),s);
    
    corobs1(s) = obscor(obsipk1(s),s);
    errobs1(s) = obserr(obsipk1(s),s);
    corobs2(s) = obscor(obsipk2(s),s);
    errobs2(s) = obserr(obsipk2(s),s);
    pcorobs1(s) = obspcor(obsipk1(s),s);
    perrobs1(s) = obsperr(obsipk1(s),s);
    pcorobs2(s) = obspcor(obsipk2(s),s);
    perrobs2(s) = obsperr(obsipk2(s),s);
    
    del1 = ~(mod(act.trialid,12)==0); 
    del2 = true(size(del1)); del2(find(del1==0)+1) = 0;% del1(end) = [];
    del2 = del2(1:length(del1));
    del2(1) = 0;
    del1(end) = 0; 
    
    
    conf = act.confidence_values;%(del2); 
    acc = act.err;%(del2); 
    
    confproxy = act.pred_reg_xval(actipk1(s),:);%del1); 
    [predact1(s,:),predact1_ci(s,:,:),~,~,predact1_st(s,:)]  = regress(conf.',[zscore(acc.') acc.'*0+1]);   
    [predact1eeg(s,:),predact1eeg_ci(s,:,:),~,~,predact1eeg_st(s,:)]  = regress(conf.',[zscore(acc.') zscore(confproxy.') acc.'*0+1]);
    confproxy = act.pred_reg_xval(actipk2(s),:);%del1); 
    [predact2(s,:),predact2_ci(s,:,:),~,~,predact2_st(s,:)]  = regress(conf.',[zscore(acc.') acc.'*0+1]);
    [predact2eeg(s,:),predact2eeg_ci(s,:,:),~,~,predact2eeg_st(s,:)]  = regress(conf.',[zscore(acc.') zscore(confproxy.') acc.'*0+1]);
    
   
    del1 = ~(mod(obs.trialid,12)==0); 
    del2 = true(size(del1)); del2(find(del1==0)+1) = 0;% del1(end) = [];
    del2 = del2(1:length(del1));
    del2(1) = 0;
    del1(end) = 0; 
    conf = obs.confidence_values;%(del2); 
    acc = obs.err;%(del2); 
    confproxy = obs.pred_reg_xval(obsipk1(s),:);%del1); 
    [predobs1(s,:),predobs1_ci(s,:,:),~,~,predobs1_st(s,:)]  = regress(conf.',[zscore(acc.') acc.'*0+1]);
   [predobs1eeg(s,:),predobs1eeg_ci(s,:,:),~,~,predobs1eeg_st(s,:)]  = regress(conf.',[zscore(acc.') zscore(confproxy.') acc.'*0+1]);
   
    confproxy = obs.pred_reg_xval(obsipk2(s),:);%del1); 
    [predobs2(s,:),predobs2_ci(s,:,:),~,~,predobs2_st(s,:)]  = regress(conf.',[zscore(acc.')  acc.'*0+1]);
   [predobs2eeg(s,:),predobs2eeg_ci(s,:,:),~,~,predobs2eeg_st(s,:)]  = regress(conf.',[zscore(acc.') zscore(confproxy.') acc.'*0+1]);
   
    
end

%%%%%%%%%%%%%%%%%%%
%% Load permutations
%%%%%%%%%%%%%%%%%%%

% Try to load pre-computed data
try
    load('../../misc/r2_permutations.mat');
    disp('Pre-loaded data');
catch
    disp('Re-computing!');
    act = load([folder_permutations '/s' num2str(subjects(1)) '_act' filename_perm_suffix '.mat']);
    num_permutations = size(act.pred_reg_xval,3);
    r2_obs_all = zeros(length(time_window), num_permutations, length(subjects));
    r2_act_all = zeros(length(time_window), num_permutations, length(subjects));
    for s=1:length(subjects)
        s
        act = load([folder_permutations '/s' num2str(subjects(s)) '_act' filename_perm_suffix '.mat']);
        obs = load([folder_permutations '/s' num2str(subjects(s)) '_obs' filename_perm_suffix '.mat']);
        r2_act_all(:,:,s) = act.r2_reg;
        r2_obs_all(:,:,s) = obs.r2_reg;
        
    end
    % Save the values so we don't have to recompute this all the time...
    save r2_permutations.mat r2_act_all r2_obs_all
end

%%%%%%%%%%%%%%%%%%%
%% Compute chance level from permutations
%%%%%%%%%%%%%%%%%%%

r2_act_all = mean(r2_act_all,3);
r2_obs_all = mean(r2_obs_all,3);

r2_act_all = squeeze(max(r2_act_all)');
r2_obs_all = squeeze(max(r2_obs_all)');
chance_act = prctile(r2_act_all,95);
chance_obs = prctile(r2_obs_all,95);


%%%%%%%%%%%%%%%%%%%
%% Plot r2 results with chance level thresholds
%%%%%%%%%%%%%%%%%%%

figure;
hold on
semshade(actr2_reg.',0.4,'r',time_window,'LineWidth',1);
semshade(obsr2_reg.',0.4,'c',time_window,'LineWidth',1);


t_significant = time_window;
y_significant = mean(actr2_reg,2);
t_significant(y_significant<=chance_act') = NaN;
y_significant(y_significant<=chance_act') = NaN;
plot(t_significant, y_significant, 'r', 'linewidth', 4);

t_significant = time_window;
y_significant = mean(obsr2_reg,2);
t_significant(y_significant<=chance_obs') = NaN;
y_significant(y_significant<=chance_obs') = NaN;
plot(t_significant, y_significant, 'c', 'linewidth', 4);

plot(get(gca,'xlim'), [chance_act chance_act], 'r--');
plot(get(gca,'xlim'), [chance_obs chance_obs], 'c--');


ylim([0.05 0.2]);
plot([0 0], get(gca, 'ylim'), 'k');

legend({'Active', 'obsitoring', 'Chance active', 'Chance obsitoring'}, 'Location', 'SouthEast');
legend boxoff
xlabel('Time (s)');
ylabel('R^2');

%%

stat = @(x) [mean(x) std(x)./sqrt(length(x)) 4*signtest(x)];
fprintf('Active condition @t=%.3f (%.3f): cor(w1) %.3f (%.3f p=%.3f) cor(w2) %.3f (%.3f p=%.3f)\n',mean(tact1),std(tact1)./sqrt(20),stat(coract1),stat(coract2));
fprintf('Active condition @t=%.3f (%.3f): err(w1) %.3f (%.3f p=%.3f) err(w2) %.3f (%.3f p=%.3f)\n',mean(tact2),std(tact2)./sqrt(20),stat(erract1),stat(erract2));
fprintf('Observation condition @t=%.3f (%.3f): cor(w1) %.3f (%.3f p=%.3f) cor(w2) %.3f (%.3f p=%.3f)\n',mean(tobs1),std(tobs1)./sqrt(20),stat(corobs1),stat(corobs2));
fprintf('Observation condition @t=%.3f (%.3f): err(w1) %.3f (%.3f p=%.3f) err(w2) %.3f (%.3f p=%.3f)\n',mean(tobs2),std(tobs2)./sqrt(20),stat(errobs1),stat(errobs2));

stat = @(x) [mean(x) std(x)./sqrt(length(x))];
fprintf('GLM 1: active: %.2f (%.2f) observation %.2f (%.2f)\n',stat(max(max(abs(creg1_act)))),stat(max(max(abs(creg1_obs)))));
fprintf('GLM 2: active: %.2f (%.2f) observation %.2f (%.2f)\n',stat(max(max(abs(creg2_act)))),stat(max(max(abs(creg2_obs)))));

subj = opts.subjects;
tbl = table(subj.',tact1.',tact2.',tobs1.',tobs2.','VariableNames', {'subj','tact1','tact2','tobs1','tobs2'});
writetable(tbl,'../../misc/eegdecode.csv');