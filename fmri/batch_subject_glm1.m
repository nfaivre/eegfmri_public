function batch_subject_glm1(subject)

config;

% the subject's code
opts.subject = subject;

addpath(opts.spmdir);
spm fmri

% directories
opts.outdir = [opts.glm1dir '/' opts.subject '/'];
opts.alldir = [opts.glm1dir '/all/' ];
if ~exist(opts.alldir,'dir')
    mkdir(opts.alldir);
end
if ~exist(opts.outdir,'dir')
    mkdir(opts.outdir);
end

% run glm analysis
matlabbatch =  params_subject_glm1(opts);
spm_jobman('run',matlabbatch);



d = dir([opts.outdir '/con_*']);
for i=1:length(d)
    fprintf('Copying %s to %s\n',[opts.outdir '/' d(i).name],[opts.alldir '/' opts.subject '_' d(i).name]);
    copyfile([opts.outdir '/' d(i).name],[opts.alldir '/' opts.subject '_' d(i).name]);
end

end