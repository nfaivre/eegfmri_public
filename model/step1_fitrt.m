%========================================================
%   step1_fitrt.m
%
%   Reads behavioral data files and fits a race model to
%   first order choice accuracy and response times
%
%   Pereira, Faivre, Iturrate et al., 2019, 
%   Disentangling the origins of confidence in speeded 
%      perceptual judgments through multimodal imaging
%
%   preregistration: https://osf.io/a5qmv/  
%   gitlab: https://gitlab.com/meta-eegfmri/  
%      
%   Michael Pereira <michael.pereira@epfl.ch>
%   25/09/2018
%========================================================

clear
close all
addpath('functions');


config; 

mkdir(['models/' opts.type]);
data = cell(length(opts.subjects),3);
s = 0;
for subj=opts.subjects
    s = s+1;
    
    %========================================================
    %% load behav data
    %========================================================
    basedir = [opts.data_path '/derivatives/fmriprep/' ...
        cell2mat(subj) '/ses-001/func/'];
    files = dir([basedir '/' cell2mat(subj) ... 
        '_ses-001_task-main_run-*_events.tsv']);
    data = readbehav(files,basedir,s);
    
    %========================================================
    %% Process behav data for active condition
    %========================================================
    rt = data{s,1}.rt;
    resp = data{s,1}.cor;
    
    %========================================================
    %% Fitting
    %========================================================
    
    params = paramset();
    
    % setting the non-decision time to slightly lower than the median
    % response time improved the speed of convergence
    params.ndtime.val = 0.8*median(rt);
    
    % loop across all decay init values
    zvarinit = opts.zvarinit;
    for v = 1:length( zvarinit)
        param{v} = params;
        param{v}.startvar.val = zvarinit(v);
        if (param{v}.startvar.val == 0)
            param{v}.startvar.fit=0;
        else
            param{v}.startvar.fit=1;
        end
        % fit RT and choice accuracy
        rng(1234);
        [ fitparams{v}, ll(v), out{v}] = fitrt(rt ,resp, param{v}, opts);
        if opts.doplot
            subplot(131);
            title(sprintf('%s zvarinit=%.2f, ll=%.4f',opts.subjects{s},zvarinit(v),ll(v)));
            print(['models/' opts.type '/' opts.subjects{s} '_race_' opts.type '_zvar' num2str(v) '.eps'],'-depsc');
        end
    end
    
    %========================================================
    %% Saving
    %========================================================

    save(['models/' opts.type '/' opts.subjects{s} '_race_' opts.type '.mat'],'fitparams','opts','out','ll');
    
end