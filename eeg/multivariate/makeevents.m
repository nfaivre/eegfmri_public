%%%%%%%%%%%%%%%%%%%%%%%
%% Set path and subjects
%%%%%%%%%%%%%%%%%%%%%%%

close all, clear all, clc;

addpath(genpath('./internal'));
addpath('/home/nfaivre/matlab/eeglab14_1_1b/');

%eeglab();

data_path = '/Scratch/meaperei/BIDS/eegfmri/derivatives/eegprep/';% path to the data

subjectsname = {'sub-02','sub-04','sub-07','sub-08','sub-09','sub-10','sub-11',...
    'sub-12','sub-13','sub-14','sub-15','sub-16','sub-17','sub-18','sub-19','sub-20',...
    'sub-21','sub-22','sub-24','sub-26'};
subjects = [10 2 4 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 24 26];

elecs = {'Fp1','Fp2','F3','F4','C3','C4','P3','P4','O1','O2','F7','F8','T7','T8',...
    'P7','P8','Fz','Cz','Pz','Oz','FC1','FC2','CP1','CP2','FC5','FC6','CP5','CP6',...
    'TP9','TP10','POz','FCz','F1','F2','C1','C2','P1','P2','AF3','AF4','FC3','FC4',...
    'CP3','CP4','PO3','PO4','F5','F6','C5','C6','P5','P6','AF7','AF8','FT7','FT8',...
    'TP7','TP8','PO7','PO8','FT9','FT10','Fpz','CPz'};



for s=1:length(subjects)
    %%
    % In the IC decoding, we load the previously computed weights and
    % ICs to retain.
    
        %%
        eegfile = [data_path '/' subjectsname{s} '/ses-001/eeg/' ...
            subjectsname{s} '_ses-001_task-main_eeg_preproc.set'];
        fprintf('%s: opening %s\n',subjectsname{s},eegfile);
        %load(eegfile, 'EEG');
        EEG = pop_loadset(eegfile);
       
    conditions = cellfun(@(x) x{1}, {EEG.epoch.eventcond});
    
    confidence_values = cellfun(@(x) x{1}, {EEG.epoch.eventconf});
    rt = cellfun(@(x) x{1}, {EEG.epoch.eventRT});

    epochs_to_use = 1:length(EEG.epoch);
    epochs_run = cellfun(@(x)(x{1}),{EEG.epoch(:).eventrun});

    % remove bad trials according to those found during preprocessing
    % steps.
        load(['../../misc//badtrials.mat']);
        idx_bad_trials = find(allbadtrials{find(strcmp(allsubjects, ['s' num2str(s)]))} ~= 0);
        trialid = cellfun(@(x) x{1}, {EEG.epoch.eventrealepoch}) + 1;
        run_idx = 48*(cellfun(@(x) x{1}, {EEG.epoch.eventrun})-1);
        trialid = trialid + run_idx;

        trials_to_remove = intersect(idx_bad_trials, trialid);
        trials_to_remove = arrayfun(@(x)(find(trialid == x)), trials_to_remove);
        fprintf(' * Check badtrials: %d / %d\n',length(trials_to_remove),length(unique([trials_to_remove ; find(rt>500).' ; find(rt<0).'])));
        trials_to_remove = unique([trials_to_remove ; find(rt>500).' ; find(rt<0).']);
        
        
        epochs_to_use(trials_to_remove) = [];
        confidence_values(trials_to_remove) = [];
        labels_confidence(trials_to_remove) = [];
        conditions(trials_to_remove) = [];
        trialid(trials_to_remove) = [];
        epochs_run(trials_to_remove) = [];
        rt(trials_to_remove) = [];
        
        
 
    % Find epochs to use from the data
    epochs_to_use(conditions ~= cond) = [];
    confidence_values(conditions ~= cond) = [];
    labels_confidence(conditions ~= cond) = [];
    trialid(conditions ~= cond) = [];
    epochs_run(conditions ~= cond) = [];
    rt(conditions ~= cond) = [];
    trialrun = epochs_run + (subject==19);
    epochs = EEG.data(:,:,epochs_to_use);
    
   
    
    
    match = false(1,64);
    for e=1:length(EEG.chanlocs)
        el = find(strcmp(EEG.chanlocs(e).labels,elecs));
        match(el) = 1;
    end
    
    
    
    
end
