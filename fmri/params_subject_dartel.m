%-----------------------------------------------------------------------
% Job saved on 08-May-2017 10:46:42 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6685)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
%%
function matlabbatch = params_subject_dartel(params)
%%

nsess = 0;
%% get functional images for each session
% get all session directories
srchstr1 = [params.datadir params.subject '/fmri/1_*_Run*_s4_*'];
d1 = dir(srchstr1);
if isempty(d1)
    fprintf('ERROR: could not find any files corresponding to %s\n',srchstr1);
end
exclude = false(1,length(d1));
% preallocate
im = 0;
for f1=1:length(d1)
    nsess = nsess+1;
    % get all files in the session directory
    srchstr2 = [params.datadir params.subject '/fmri/' d1(f1).name '/rJ*.nii'];
    d2 = dir(srchstr2);
    if isempty(d2)
        fprintf('ERROR: could not find any files corresponding to %s\n',srchstr2);
    end
     % add files
    imfile = [params.datadir params.subject '/fmri/' d1(f1).name '/' d2(1).name];
    matlabbatch{1}.spm.tools.dartel.mni_norm.data.subj.images{nsess,1} = imfile;

    % count the number of volumes
    hdr = spm_data_hdr_read(imfile);
   
    fprintf(' + Adding %s (%d volumes)\n',d1(f1).name,length(hdr));
    
end
%% get functional images for rest
if params.dorest
    % get all session directories
    srchstr1 = [params.datadir params.subject '/fmri/1_*_Rest*_s4_*'];
    d1 = dir(srchstr1);
    if isempty(d1)
        fprintf('ERROR: could not find any files corresponding to %s\n',srchstr1);
    end
    for f1=1:length(d1)
        nsess = nsess+1;
        % get all files in the session directory
        srchstr2 = [params.datadir params.subject '/fmri/' d1(f1).name '/rJ*.nii'];
        d2 = dir(srchstr2);
        if isempty(d2)
            fprintf('ERROR: could not find any files corresponding to %s\n',srchstr2);
        end
        % add files
        imfile = [params.datadir params.subject '/fmri/' d1(f1).name '/' d2(1).name];
        matlabbatch{1}.spm.tools.dartel.mni_norm.data.subj.images{nsess,1} = imfile;

        % count the number of volumes
        hdr = spm_data_hdr_read([params.datadir params.subject '/fmri/' d1(f1).name '/' d2(1).name]);
        fprintf(' + Adding %s (%d volumes)\n',d1(f1).name,length(hdr));
    end
end

%%

    matlabbatch{1}.spm.tools.dartel.mni_norm.template = {params.dartel_template};
    matlabbatch{1}.spm.tools.dartel.mni_norm.data.subj.flowfield = {params.dartel_flowfile};

    matlabbatch{1}.spm.tools.dartel.mni_norm.vox = [NaN NaN NaN];
    matlabbatch{1}.spm.tools.dartel.mni_norm.bb = [NaN NaN NaN
        NaN NaN NaN];
    matlabbatch{1}.spm.tools.dartel.mni_norm.preserve = 0;
    matlabbatch{1}.spm.tools.dartel.mni_norm.fwhm = [5 5 5];
    

end