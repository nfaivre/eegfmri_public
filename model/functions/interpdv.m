%========================================================
%   interpdv.m interpolate the decision variable in case of non-integer readout
%
%   Pereira, Faivre, Iturrate et al., 2019, 
%   Disentangling the origins of confidence in speeded 
%      perceptual judgments through multimodal imaging
%
%   preregistration: https://osf.io/a5qmv/  
%   gitlab: https://gitlab.com/meta-eegfmri/  
%      
%   Michael Pereira <michael.pereira@epfl.ch>
%   25/09/2018
%========================================================
function [ dvnew ] = interpdv( dv, t, opts)

    % transform from time to sample
    tconf = t/opts.dt;
    % get fraction
    tconf_w = tconf - floor(tconf);
    % floor to get integer sample
    tconf = floor(tconf);
    
    % interpolate
    dvnew = (1-tconf_w)*dv(:,tconf,:) + (tconf_w)*dv(:,tconf+1,:);

end

