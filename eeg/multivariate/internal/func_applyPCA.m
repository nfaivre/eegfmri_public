function [ trainData, testData, PCA ] = func_applyPCA( trainData, testData, debug, varargin )

    if (iscell(varargin{1}))
        varargin = varargin{1};
    end
    
    varianceToRetain = 0.95;
    PCA = {};
    for v=1:length(varargin)
        if (strcmp(varargin{v},'VarianceToRetain'))
            varianceToRetain = varargin{v+1};
        end
        if (strcmp(varargin{v},'PCA'))
            PCA = varargin{v+1};
        end
    end
    if (debug)
        disp(['Applying PCA retaining of variance: ' num2str(varianceToRetain)]);
    end
    
    if (isempty(PCA))
        if (debug)
            disp('PCA is not computed');
        end
        [coeffsPCA,s,latent] = princomp(trainData.features);
        explainedVariance = cumsum(latent)/sum(latent);
        dimensionsToRetain = find(explainedVariance>=varianceToRetain,1);
    else
        if (debug)
            disp('PCA is  computed');
        end
        dimensionsToRetain = PCA.dimensionsToRetain;
        coeffsPCA = PCA.coeffs;
        varianceToRetain = PCA.varianceToRetain;
    end
    
    if (isempty(dimensionsToRetain) || (dimensionsToRetain == 0))
        dimensionsToRetain
        error('Dimensions to retain wrong');
    end
    if (debug)        
        disp(['PCA. Retained dimensions ' num2str(dimensionsToRetain)]);
    end
    
    trainData.features = trainData.features * coeffsPCA(:,1:dimensionsToRetain);
    testData.features = testData.features * coeffsPCA(:,1:dimensionsToRetain);
    if (debug)        
        disp(['PCA. end ']);
    end
    
    PCA.coeffs = coeffsPCA;
    PCA.dimensionsToRetain = dimensionsToRetain;
    PCA.varianceToRetain = varianceToRetain;
    PCA.useR2Features = useR2Features;
    
end

