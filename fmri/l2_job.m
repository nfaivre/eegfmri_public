%-----------------------------------------------------------------------
% Job saved on 09-Dec-2018 12:40:54 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6685)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.stats.factorial_design.dir = {'/Scratch/meaperei/eegfmri/results/glm1/level2'};
%%
matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(1).scans = {
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-02_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-04_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-07_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-08_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-09_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-10_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-11_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-12_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-13_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-14_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-15_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-16_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-17_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-18_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-19_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-20_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-21_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-22_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-24_con_0005.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-26_con_0005.nii,1'
                                                                      };
%%
%%
matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(2).scans = {
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-02_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-04_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-07_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-08_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-09_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-10_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-11_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-12_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-13_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-14_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-15_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-16_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-17_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-18_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-19_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-20_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-21_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-22_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-24_con_0006.nii,1'
                                                                      '/Scratch/meaperei/eegfmri/results/glm1/all/sub-26_con_0006.nii,1'
                                                                      };
%%
matlabbatch{1}.spm.stats.factorial_design.des.anova.dept = 0;
matlabbatch{1}.spm.stats.factorial_design.des.anova.variance = 1;
matlabbatch{1}.spm.stats.factorial_design.des.anova.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.anova.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {''};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
matlabbatch{2}.spm.stats.review.spmmat(1) = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.review.display.matrix = 1;
matlabbatch{2}.spm.stats.review.print = 'ps';
matlabbatch{3}.spm.stats.fmri_est.spmmat(1) = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{3}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{3}.spm.stats.fmri_est.method.Classical = 1;
matlabbatch{4}.spm.stats.con.spmmat(1) = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{4}.spm.stats.con.consess{1}.tcon.name = 'ConfAct+';
matlabbatch{4}.spm.stats.con.consess{1}.tcon.weights = [1 0];
matlabbatch{4}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{4}.spm.stats.con.consess{2}.tcon.name = 'ConfAct-';
matlabbatch{4}.spm.stats.con.consess{2}.tcon.weights = [-1 0];
matlabbatch{4}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{4}.spm.stats.con.consess{3}.tcon.name = 'ConfObs+';
matlabbatch{4}.spm.stats.con.consess{3}.tcon.weights = [0 1];
matlabbatch{4}.spm.stats.con.consess{3}.tcon.sessrep = 'none';
matlabbatch{4}.spm.stats.con.consess{4}.tcon.name = 'ConfObs-';
matlabbatch{4}.spm.stats.con.consess{4}.tcon.weights = [0 -1];
matlabbatch{4}.spm.stats.con.consess{4}.tcon.sessrep = 'none';
matlabbatch{4}.spm.stats.con.delete = 0;
matlabbatch{5}.spm.stats.results.spmmat(1) = cfg_dep('Contrast Manager: SPM.mat File', substruct('.','val', '{}',{4}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{5}.spm.stats.results.conspec.titlestr = '';
matlabbatch{5}.spm.stats.results.conspec.contrasts = 1;
matlabbatch{5}.spm.stats.results.conspec.threshdesc = 'none';
matlabbatch{5}.spm.stats.results.conspec.thresh = 0.001;
matlabbatch{5}.spm.stats.results.conspec.extent = 0;
matlabbatch{5}.spm.stats.results.conspec.conjunction = 1;
matlabbatch{5}.spm.stats.results.conspec.mask.none = 1;
matlabbatch{5}.spm.stats.results.units = 1;
matlabbatch{5}.spm.stats.results.print = 'ps';
matlabbatch{5}.spm.stats.results.write.none = 1;
