#!/usr/bin/python
# -*- coding: utf-8 -*-
import ConfigParser

import os, sys, math, string, subprocess,random,gc,time
from PyQt4 import QtGui, QtCore
from ctypes import *
from datetime import datetime
import time
#import pygame.mixer
import numpy as np

import guifunc
import interface

from trial import Trial
from trials import Trials

class EEG_FMRI(QtGui.QWidget):
	def __init__(self):
		super(EEG_FMRI, self).__init__()

		# Init interface helper class
		self.iface = interface.Interface("data")
		# Load configuration file
		self.config = self.iface.load_config_file()
		# Process configuration file
		self.init_parameters()
		self.hand_left = 0
		self.hand_right = 0
		self.nohand_left = 0
		self.nohand_right = 0


		self.color_diode = QtGui.QColor(255,255,255)
		

		# Init eye-tracker
		self.iface.init_eyetracker(self.connect_eye,(self.screen_width,self.screen_height))
		# Init paralell port
		self.iface.init_parport(self.lpttriggers)

		# Make filename
		now = datetime.now().strftime("%y%m%d_%H%M%S")
		self.filename = os.path.join("data",self.config.get("protocol", "subject_id") + "_" + self.config.get("protocol", "name_experiment") + '_' + now)
		if self.session_type == self.TRAINING_SESSION:
			self.filename += "_staircase"
		now = datetime.now().strftime("%y%m%d")
		self.statefilename = os.path.join("data",self.config.get("protocol", "subject_id") + "_" + self.config.get("protocol", "name_experiment") + '_' + now + '_state')
		if not self.demo:
			self.iface.copy_config_file(self.filename)

		# Set execution mode to motor 
		self.execution_mode = self.MOTOR_TASK
		# Only load the (saved) bias if the staicase procedure has been accomplished
		if self.session_type==self.RECORDING_SESSION:
			param = self.iface.load_file_state(self.statefilename)
			if param == -1:
				param = self.config.getint("protocol", "stimulus_intensity_difference")
			print "Session type is standard. Loading bias: " + repr(param)
			self.bias = int(param)
			n = int(self.num_trials_run/2)
			self.doquestion = 1
		elif self.session_type == self.TRAINING_SESSION:
			print "Session type is training"
			n = self.num_trials_run
			self.doquestion = 0
			self.state_question_range = [0,0]
			self.bias = self.config.getint("protocol", "stimulus_intensity_difference")
		
		self.show_in_sec_monitor = self.config.getint("protocol", "show_in_second_screen")

		# prepare trials
		print "Preparing " + repr(n) + " trials"
		self.trials = Trials(n,self.state_fixation_range,self.stimulus_duration,self.feedback_duration,self.state_task1_duration,self.state_task2_duration,self.state_iti_duration)
		# Pointer to the current trial
		self.current_trial = self.trials.getTrial(0) 

		# Allocate memory to record trials
		self.array_timings = np.zeros((16,self.num_trials_run), dtype = float)
		self.array_stimuli = np.zeros((200,self.num_trials_run), dtype = int)

		# load sound
		self.sound = self.iface.load_sound("alarm.wav")

		# Init GUI
		#self.guifun = guifunc.GuiFunc(self.screen_width,self.screen_height,self.dist_border_x,self.dist_border_y, self.stimulus_size,self.full_screen,self.color_stimulus,self.color_stimulus_background,self.color_interface,self.color_target_error)
		self.guifun = guifunc.GuiFunc(self.screen_width,self.screen_height,self.pxmm,self.screen_distance_mm, self.stimulus_eccentricity, self.stimulus_maxangle, self.cross_angle, self.cross_width, self.hand_angle, self.full_screen,self.color_stimulus,self.color_stimulus_background,self.color_interface,self.color_target_error, self.used_filled_scale, self.photodiode)
		
		# size_ecran =   # donne les dimensions de l'ecran
		# print "La res es" + str(size_ecran.width())
		# width = GetSystemMetrics (0)
		# height = GetSystemMetrics (1)
		# print "Screen resolution = %dx%d" % (width, height)

		# Start GUI
		
		self.setWindowTitle('Error monitoring experimental protocol')
		self.setFocusPolicy(QtCore.Qt.StrongFocus)
		self.setFocus(True)
		self.show()

		# Iturrate: show the protocol GUI on monitor 1 or 2
		if (self.show_in_sec_monitor):
			self.setGeometry(QtGui.QDesktopWidget().screenGeometry().width(),0, self.screen_width, self.screen_height)
			print "Showing protocol in secondary screen"
		else:
			self.setGeometry(0,0, self.screen_width, self.screen_height)
			print "Showing protocol in main screen"

		if (self.full_screen):
			self.showFullScreen()
		

		QtCore.QCoreApplication.processEvents()
		QtCore.QCoreApplication.flush()
		
		#qgl_format = QtOpenGL.QGLFormat()
		#qgl_format.setSwapInterval(1)

		# Construct a QGLWidget using the above format
		#self.setFormat(qgl_format)

		# load hand images
		[self.hand_left,self.hand_right,self.nohand_left,self.nohand_right] = self.guifun.DrawHand(self)

		# Start protocol
		timer = QtCore.QTimer(self)
		timer.timeout.connect(self.update_loop)
		timer.start(self.cycle_time)

		self.timecounter = time.time()

	def init_parameters(self):

		# Init window parameters
		self.full_screen = self.config.getint("interface", "full_screen")
		if (self.full_screen):
			self.screen_width = QtGui.QDesktopWidget().screenGeometry().width()
			self.screen_height = QtGui.QDesktopWidget().screenGeometry().height()
			print "** screen ", self.screen_width, ", ", self.screen_height
		else:
			self.screen_width = self.config.getint("interface", "screen_width")
			self.screen_height = self.config.getint("interface", "screen_height")

		# Init constants		
		self.MONITORING_TASK = self.config.getint("constants", "MONITORING_TASK")
		self.MOTOR_TASK = self.config.getint("constants", "MOTOR_TASK")
		self.TRAINING_SESSION = self.config.getint("constants", "TRAINING_SESSION")
		self.RECORDING_SESSION = self.config.getint("constants", "RECORDING_SESSION")

		self.TARGET_LEFT = self.config.getint("constants", "TARGET_LEFT")
		self.TARGET_RIGHT = self.config.getint("constants", "TARGET_RIGHT")
		
		
		self.RESP1_LEFT_PRESSED = self.config.getint("constants", "RESP1_LEFT_PRESSED")
		self.RESP1_RIGHT_PRESSED = self.config.getint("constants", "RESP1_RIGHT_PRESSED")
		self.FB_LEFT = self.config.getint("constants", "FB_LEFT")
		self.FB_RIGHT = self.config.getint("constants", "FB_RIGHT")
		self.RESP2_LEFT_PRESSED = self.config.getint("constants", "RESP2_LEFT_PRESSED")
		self.RESP2_RIGHT_PRESSED = self.config.getint("constants", "RESP2_RIGHT_PRESSED")
		self.KEY_LEFT_PRESSED = self.config.getint("constants", "KEY_LEFT_PRESSED")
		self.KEY_RIGHT_PRESSED = self.config.getint("constants", "KEY_RIGHT_PRESSED")

		self.STATE_REST = self.config.getint("constants", "REST_STATE")
		self.STATE_FIXATION = self.config.getint("constants", "FIXATION_STATE")
		self.STATE_STIMULUS = self.config.getint("constants", "STIMULUS_STATE")
		self.STATE_QUESTION = self.config.getint("constants", "QUESTION_STATE")
		self.STATE_INCENTIVE = self.config.getint("constants", "INCENTIVE_STATE")
	
		# Init interface parameters    	
		self.demo = self.config.getint("protocol", "demo")

		self.used_filled_scale = self.config.getint("protocol", "used_filled_scale")

		self.session_type = self.config.getint("protocol", "session_type")

		self.color_background = QtGui.QColor(float(self.config.get("colors", "color_background").split(',')[0]), \
											float(self.config.get("colors", "color_background").split(',')[1]), \
											float(self.config.get("colors", "color_background").split(',')[2]))	

		self.color_stimulus = QtGui.QColor(float(self.config.get("colors", "color_stimulus").split(',')[0]), \
														float(self.config.get("colors", "color_stimulus").split(',')[1]), \
														float(self.config.get("colors", "color_stimulus").split(',')[2]))
		self.color_stimulus_background = QtGui.QColor(float(self.config.get("colors", "color_stimulus_background").split(',')[0]), \
													float(self.config.get("colors", "color_stimulus_background").split(',')[1]), \
													float(self.config.get("colors", "color_stimulus_background").split(',')[2]))
		self.color_interface = QtGui.QColor(float(self.config.get("colors", "color_interface").split(',')[0]), \
													float(self.config.get("colors", "color_interface").split(',')[1]), \
													float(self.config.get("colors", "color_interface").split(',')[2]))
		self.color_target_error = QtGui.QColor(float(self.config.get("colors", "color_target_error").split(',')[0]), \
													float(self.config.get("colors", "color_target_error").split(',')[1]), \
													float(self.config.get("colors", "color_target_error").split(',')[2]))
		self.color_red = QtGui.QColor(255.0,0.0,0.0)
		
		self.screen_width_mm = self.config.getfloat("interface", "screen_width_mm")
		self.screen_height_mm = self.config.getfloat("interface", "screen_height_mm")
		self.screen_distance_mm = self.config.getfloat("interface", "screen_distance_mm")
		self.stimulus_eccentricity = self.config.getfloat("interface", "stimulus_eccentricity")
		self.stimulus_maxangle = self.config.getfloat("interface", "stimulus_maxangle")
		self.cross_angle = self.config.getfloat("interface", "cross_angle")
		self.cross_width = self.config.getfloat("interface", "cross_width")
		self.hand_angle = self.config.getfloat("interface", "hand_angle")

		self.pxmm = self.screen_width_mm/self.screen_width

		self.cycle_time = self.config.getfloat("interface", "paint_cycle_time")

		self.state_fixation_range = [-1, -1]
		
		# Init protocol parameters
		self.badrt_sound_on = self.config.getint("protocol", "badrt_sound_on")
		self.staircase_type = self.config.getint("protocol", "staircase_type")
		self.type2answer_side = self.config.getint("protocol", "type2answer_side")
		self.type2answer_cursorspeed = self.config.getfloat("protocol", "type2answer_cursorspeed")
		self.type2answer_pingpong = self.config.getfloat("protocol", "type2answer_pingpong")
		self.respond_thr = round(self.config.getfloat("protocol", "respond_thr") / float(self.cycle_time))

		self.state_fixation_range[0] = round(float(self.config.get("protocol", "state_fixation_range").split(' ')[0]) / float(self.cycle_time))
		self.state_fixation_range[1] = round(float(self.config.get("protocol", "state_fixation_range").split(' ')[1]) / float(self.cycle_time))
		self.state_task1_duration = self.config.getint("protocol", "state_task1_duration") / float(self.cycle_time)
		self.state_task2_duration = self.config.getint("protocol", "state_task2_duration") / float(self.cycle_time)
		self.state_iti_duration = self.config.getint("protocol", "state_iti_duration") / float(self.cycle_time)
		self.state_start_duration = self.config.getint("protocol", "state_start_duration") / float(self.cycle_time)
		self.state_incentive_duration = self.config.getint("protocol", "state_incentive_duration") / float(self.cycle_time)
		
		self.stimulus_duration = self.config.getint("protocol", "stimulus_duration") / float(self.cycle_time)
		self.feedback_duration = self.config.getint("protocol", "feedback_duration") / float(self.cycle_time)

		self.num_trials_run = self.config.getint("protocol", "num_trials")
		self.state_rt_max = round(float(self.config.getint("protocol", "state_rt_max")) / float(self.cycle_time))
		self.num_trials_si = self.config.getint("protocol", "num_trials_speed_incentive")
		self.connect_eye = self.config.getint("protocol", "connect_eyetracker")

		self.lpttriggers = self.config.getint("protocol", "lpttriggers")
		self.photodiode = self.config.getint("protocol", "use_photodiode")
		self.no_condidence = self.config.getint("protocol", "no_condidence")

		# Demo mode
		if self.demo:
			if self.session_type == self.RECORDING_SESSION:
				self.num_trials_si = 4


		# Init internal variables
		self.action_executed = 0
		self.counter = 0
		self.counter_fixation = 0
		self.counter_stimulus= 0
		self.counter_feedback = 0
		self.counter_question = 0
		self.counter_rest = 0
		self.counter_speed_incentive = 0
		self.time_questionanswer = 0

		self.current_state = self.STATE_REST
		self.current_answer = 0
		self.chosen_stimulus_side = 0
		self.num_trials_executed = 0
		self.num_motor_executed = 0
		self.stop_protocol = 1
		self.counter_errors = 0
		self.counter_badrt = 0
		self.counter_goodtrial = 0
		self.synctime = 0 # for eyetracker sync
		self.got_scanner_trigger = 0 # whether we got the first scanner trigger
		self.nresp2 = 0 # to compute RT2

		self.bad_monitoring_trial = 0
		self.bad_motor_trial = 0
		self.trigger_fix_sent = 0
		self.trigger_stim_sent = 0
		self.trigger_fb_sent = 0

	def choose_next_stimulus_side(self):
		
		self.chosen_stimulus_side = self.current_trial.getSide()

		if (self.execution_mode == self.MONITORING_TASK):
			self.action_executed = self.current_trial.getResp1()
		
			
	def update_phase(self):		
		self.counter += 1
		# set special duration at the begining of the experiment
		if (self.num_trials_executed == 0): 
			restdur = self.state_start_duration
		else:
			restdur = self.current_trial.getIti()

		# ===================== REST =====================
		if (self.current_state == self.STATE_REST):		
			if self.got_scanner_trigger == 1:
				self.counter_rest += 1
			if (self.counter_rest == restdur):
				if (self.num_trials_executed == self.num_trials_run):
					print "** exiting after " + repr(self.num_trials_executed) + "/" + repr(self.num_trials_run) + " trials"
					self.exit_function()

				self.trigger_fix_sent = 0
				self.trigger_stim_sent = 0
				self.trigger_fb_sent = 0
				
				# otherwise pass to preparation phase
				self.current_state = self.STATE_FIXATION
				self.iface.eyetracker_newtrial(self.num_trials_executed)

				self.counter_rest = 0		

		# ===================== FIXATION CROSS =====================
		elif (self.current_state == self.STATE_FIXATION):
			self.counter_fixation += 1

			# ** TIMER **
			if (self.counter_fixation == self.current_trial.getFix()):
				#self.trials.debugTrialBalance()
				# get the next trial
				self.bad_monitoring_trial = 0
				self.bad_motor_trial = 0

				if self.execution_mode == self.MONITORING_TASK:
					if len(self.trials_monitoring) > 0:
						#print "   -> get monitoring trial"
						self.current_trial = self.trials_monitoring.pop()
						print "   ! RT=" + repr(self.current_trial.getRt1())
						if self.current_trial.getRt1() == -1:
							# if the subject did not respond during the motor task
							self.current_trial.setRt1(self.respond_thr+1)
							self.current_trial.setResp1(self.current_trial.getSide())
							self.bad_motor_trial = 20
				if self.execution_mode == self.MOTOR_TASK:
					#print "   -> get motor trial"
					self.current_trial = self.trials.getTrial(self.num_motor_executed)
					self.current_trial.setBias(self.bias)
					self.num_motor_executed += 1

				self.num_trials_executed += 1

				self.current_state = self.STATE_STIMULUS
				self.choose_next_stimulus_side()
				print "++ Trial " + repr(self.num_trials_executed) + "/" + repr(self.num_trials_run) + " (type: " + repr(self.execution_mode) + ":" + repr(self.session_type) + ")"
				#self.iface.sendtrigger(self.chosen_stimulus_side)
				self.synctime = int(round(time.time() * 1000))

				if self.execution_mode == self.MOTOR_TASK:
					bias = self.current_trial.getIntens()
					# make random stimulus
					[l,r] = self.guifun.StimDefine(50-bias,50+bias)
					# remember stimulus
					self.current_trial.setDisposition(l,r)
				else:
					[l,r] = self.current_trial.getDisposition()
					self.guifun.StimLoad(l,r)

				self.counter_fixation = 0
		# ===================== STIMULUS =====================
		elif (self.current_state == self.STATE_STIMULUS):
			self.counter_stimulus += 1
			#print str(self.counter_stimulus) + ', ' + str(self.state_stimulus_duration)
			if (self.execution_mode == self.MONITORING_TASK):
				# ** MONITORING **
				if (self.counter_stimulus == self.current_trial.getRt1()):
					if (self.counter_stimulus > self.respond_thr):
						if self.badrt_sound_on:
							self.sound.play()
							self.bad_motor_trial = 20
					
			if (self.counter_stimulus == self.state_rt_max) and (not self.action_executed) and self.badrt_sound_on:		
				self.sound.play()
				self.bad_motor_trial = 20
			elif (self.counter_stimulus == self.current_trial.getTiming1()):
				#print "action executed: " + repr(self.action_executed) + " / chosen_stimulus_side: " + repr(self.chosen_stimulus_side)
				
				self.current_state = self.STATE_QUESTION
				self.iface.sendtrigger(self.STATE_QUESTION)
				self.qpos = self.current_trial.getQinit()
				self.qdir = self.current_trial.getQdir()
				if self.no_condidence:
					self.targetqpos = random.random()
				else:
					self.targetqpos = -1
				#print "chose qpos=" + repr(self.qpos) + ", qdir=" + repr(self.qdir)
				self.current_answer = -1
				
				self.iface.eyetracker_endtrial()

		# ===================== QUESTION =====================
		elif (self.current_state == self.STATE_QUESTION):
			self.counter_question += 1	
			if self.type2answer_pingpong:
				if (self.qpos >= 1.00):
					self.qdir *= -1
					self.qpos = 1.00
				elif (self.qpos <= -0.00):
					self.qdir *= -1
					self.qpos = -0.00
			else:
				if (self.qpos >= 1.00):
					self.qpos = -0.00
				elif (self.qpos <= 0.00):
					self.qpos = 1.00
			if not self.nresp2:
				self.qpos += self.qdir/(self.cycle_time/self.type2answer_cursorspeed)
			#print "qpos: " + repr(self.qpos)
			if ((self.counter_question >= self.current_trial.getTiming2()) or (self.session_type == self.TRAINING_SESSION)):
				print "   Was error: " + repr(not (self.current_trial.getSide() == self.current_trial.getResp1()))
				print "   RT: " + repr(self.current_trial.getRt1() * self.cycle_time) + " / " + repr(self.respond_thr * self.cycle_time)

				if (self.execution_mode == self.MOTOR_TASK):
					if self.staircase_type > 0:
						if not (self.action_executed == self.chosen_stimulus_side):
							self.counter_errors += 1
							self.counter_goodtrial = 0
							if self.bias < 50:
								self.bias += 1#self.staircase_type
						else:
							self.counter_goodtrial += 1
							if (self.counter_goodtrial == self.staircase_type) and (self.bias > 1):
								self.bias -= 1		
								self.counter_goodtrial = 0
					

					ratio = float(self.counter_errors) / float(self.num_motor_executed)

					print "   Delta: " + repr(self.bias)
					print "   Errors: " + repr(self.counter_errors) + "/" + repr(self.num_motor_executed) + " = " + repr(ratio)
					
				
				if self.nresp2 == 0:
					self.current_trial.setResp2(-1)

				print "   Conf.: " + repr(self.current_trial.getResp2())
				self.current_trial.setState(self.execution_mode)
				self.current_state = self.STATE_REST
				self.iface.sendtrigger(self.STATE_REST)

				self.action_executed = 0
				self.chosen_stimulus_side = 0
				self.counter_fixation = 0
				self.counter_stimulus = 0
				self.counter_feedback = 0
				self.counter_question = 0
				self.counter_rest = 0
				self.time_questionanswer = 0
				self.trigger_to_send = 0
				self.nresp2 = 0
				# dump current trial
				tr = self.current_trial
				self.array_timings[0, self.num_trials_executed-1] = self.execution_mode + self.bad_monitoring_trial + self.bad_motor_trial 
				self.array_timings[1, self.num_trials_executed-1] = tr.getSide()#self.state_rest_duration * self.cycle_time
				self.array_timings[2, self.num_trials_executed-1] = tr.getBias()#self.counter_stimulus * self.cycle_time
				self.array_timings[3, self.num_trials_executed-1] = tr.getResp1()#self.state_feedback_duration * self.cycle_time
				self.array_timings[4, self.num_trials_executed-1] = tr.getResp2()#self.counter_question * self.cycle_time
				self.array_timings[5, self.num_trials_executed-1] = tr.getError()#self.chosen_stimulus_side
				self.array_timings[6, self.num_trials_executed-1] = self.counter * self.cycle_time #TIMING #self.action_executed
				self.array_timings[7, self.num_trials_executed-1] = tr.getFix()* self.cycle_time#self.action_executed
				self.array_timings[8, self.num_trials_executed-1] = tr.getRt1()* self.cycle_time#int(not (self.action_executed == self.chosen_stimulus_side))
				self.array_timings[9, self.num_trials_executed-1] = tr.getTiming1()* self.cycle_time#self.current_answer
				self.array_timings[10, self.num_trials_executed-1] = tr.getRt2()* self.cycle_time#self.bias
				self.array_timings[11, self.num_trials_executed-1] = tr.getTiming2()* self.cycle_time#self.bias
				self.array_timings[12, self.num_trials_executed-1] = tr.getIti()* self.cycle_time#self.bias
				self.array_timings[13, self.num_trials_executed-1] = tr.getQinit()#self.bias
				self.array_timings[14, self.num_trials_executed-1] = tr.getQdir()#self.bias
				self.array_timings[15, self.num_trials_executed-1] = self.targetqpos#self.bias

				[l,r] = tr.getDisposition()
				self.array_stimuli[0:100, self.num_trials_executed-1] = l
				self.array_stimuli[100:200, self.num_trials_executed-1] = r
				# ** TIMER **
				if (((self.num_trials_executed) % self.num_trials_si) == 0) & (self.num_trials_executed > 0):
					# pass to speed incentive phase
					print "** switch to speed incentive"
					self.current_state = self.STATE_INCENTIVE
					self.counter_goodtrial = 0
					self.iface.sendtrigger(self.STATE_INCENTIVE)
					#self.stop_protocol = 1
				
		# ===================== SPEED_INCENTIVE =====================
		elif (self.current_state == self.STATE_INCENTIVE):
			self.counter_speed_incentive += 1
			# ** TIMER **
			#print repr(self.counter_speed_incentive) + " : " + repr(self.state_incentive_duration)
			if (self.counter_speed_incentive == self.state_incentive_duration):
				self.current_state = self.STATE_REST
				self.iface.sendtrigger(self.STATE_REST)
				self.counter_speed_incentive = 0
				self.counter_badrt = 0

				if (self.execution_mode == self.MOTOR_TASK) and (self.session_type==self.RECORDING_SESSION):
					self.execution_mode = self.MONITORING_TASK
					self.trials_monitoring = self.trials.getMonitoring()
					print "** Get monitoring (" + repr(len(self.trials_monitoring)) + " trials)"
				else:
					self.execution_mode = self.MOTOR_TASK
		# ===================== DEFAULT =====================
		else:
			print "Unknown phase. Exiting"
			exit(-1)

	def update_loop(self):
		self.repaint()
		# I put the triggers here so they are as close as possible to the refresh
		if (self.current_state == self.STATE_FIXATION) and (self.trigger_fix_sent==0):
			self.iface.sendtrigger(self.STATE_FIXATION)
			self.trigger_fix_sent = 1
		if (self.current_state == self.STATE_STIMULUS)  and (self.trigger_stim_sent==0):
			self.iface.sendtrigger(self.chosen_stimulus_side)
			self.trigger_stim_sent = 1
		if (self.current_state == self.STATE_STIMULUS) and (self.execution_mode == self.MONITORING_TASK) and (self.counter_stimulus > self.current_trial.getRt1()) and (self.trigger_fb_sent==0):
			if (self.action_executed == self.TARGET_LEFT):
				self.iface.sendtrigger(self.FB_LEFT)
			elif (self.action_executed == self.TARGET_RIGHT):
				self.iface.sendtrigger(self.FB_RIGHT)
			self.trigger_fb_sent = 1

		#self.updateGL()
		now = time.time()
		looptime = now - self.timecounter
		self.timecounter = now
		self.timeincrease = looptime
		#print "time: " + repr(looptime*1000) + " ms"
		if (not self.stop_protocol):
			self.update_phase()
			
			
	def paintEvent(self, e):
		
		qp = QtGui.QPainter()
		qp.begin(self)
		self.paintInterface(qp)
		qp.end()
		

	def paintInterface(self, qp):
		if self.hand_left:
			self.hand_left.setVisible(0)
		if self.hand_right:
			self.hand_right.setVisible(0)
		if self.nohand_left:
			self.nohand_left.setVisible(0)
		if self.nohand_right:
			self.nohand_right.setVisible(0)
		self.color_diode = QtGui.QColor(0,0,0)
		qp.setRenderHint(QtGui.QPainter.Antialiasing, True)

		# Background
		qp.setPen(QtCore.Qt.NoPen)		
		qp.fillRect( 0, 0, self.screen_width, self.screen_height, self.color_background )
		
		if self.demo:
			defaultFont = qp.font()
			newFont = defaultFont
			newFont.setPointSize(newFont.pointSize()*4)
			qp.setFont(newFont)
			qp.setPen(QtGui.QColor(255,0,0))	
			qp.drawText(0,0, 400, 400, QtCore.Qt.AlignCenter, 'DEMO MODE')
			qp.setPen(QtCore.Qt.NoPen)
			qp.setFont(defaultFont)

		if self.got_scanner_trigger == 0:
			self.guifun.DisplayText(qp,'Starting soon...')

		if not((self.current_state == self.STATE_QUESTION) and self.doquestion):
			self.guifun.StimLeft(qp,(self.current_state == self.STATE_STIMULUS) and (self.counter_stimulus < self.current_trial.getStimOn1()))
			self.guifun.StimRight(qp,(self.current_state == self.STATE_STIMULUS) and (self.counter_stimulus < self.current_trial.getStimOn1()))
			if self.execution_mode == self.MOTOR_TASK:
				if (self.current_state == self.STATE_STIMULUS) and ((self.action_executed) or (self.counter_stimulus > self.state_rt_max)):
					self.guifun.StimMask(qp)
			else:
				if (self.current_state == self.STATE_STIMULUS) and ((self.counter_stimulus > self.current_trial.getRt1()) or (self.counter_stimulus > self.state_rt_max)):
					self.guifun.StimMask(qp)

			# Feedback left (if in monitoring mode)
			if (self.execution_mode == self.MONITORING_TASK) and (self.action_executed == self.TARGET_LEFT) and (self.current_state == self.STATE_STIMULUS) and (self.counter_stimulus > self.current_trial.getRt1()) and (self.counter_stimulus < (self.current_trial.getRt1()+self.current_trial.getStimOn2())):
				qp.setBrush(QtCore.Qt.NoBrush)
				#self.guifun.FeedbackLeft(qp)
				self.hand_left.setVisible(1)
				self.color_diode = QtGui.QColor(255,255,255)	

			# Feedback right (if in monitoring mode)
			if (self.execution_mode == self.MONITORING_TASK) and (self.action_executed == self.TARGET_RIGHT) and (self.current_state == self.STATE_STIMULUS) and (self.counter_stimulus > self.current_trial.getRt1()) and (self.counter_stimulus < (self.current_trial.getRt1()+self.current_trial.getStimOn2())):
				qp.setBrush(QtCore.Qt.NoBrush)
				#self.guifun.FeedbackRight(qp)
				self.hand_right.setVisible(1)
				self.color_diode = QtGui.QColor(255,255,255)	

	
			if (self.current_state == self.STATE_INCENTIVE):
				# incentive text
				
				if (self.execution_mode == self.MOTOR_TASK):
					if self.session_type == self.TRAINING_SESSION:
						txt1 = ''
					else:
						txt1 = 'Then, you will have to observe'
				else:
					txt1 = 'Then, you will have to answer'

				# trials left text
				txt2 = 'Trial ' + repr(self.num_trials_executed) + '/' + repr(self.num_trials_run)

				self.guifun.SpeedIncentive(qp,txt1,txt2)
			
				if (self.execution_mode == self.MOTOR_TASK) and (self.session_type==self.RECORDING_SESSION):
					# then warn the subject he shouldn't answer
					self.nohand_left.setVisible(1)
					self.nohand_right.setVisible(1)
				else:
					self.hand_left.setVisible(1)
					self.hand_right.setVisible(1)
			# Show fixation cross
		
			on = (self.current_state == self.STATE_FIXATION) or (self.current_state == self.STATE_STIMULUS)
			rot = self.execution_mode == self.MONITORING_TASK
			if ((self.current_state == self.STATE_INCENTIVE) and (self.session_type == self.RECORDING_SESSION)):
				rot = not rot
			self.guifun.FixationCross(qp, on, rot)
		
		else:
			#negfb = (self.execution_mode == self.MOTOR_TASK)*
			self.guifun.Question(qp,self.current_answer,self.qpos,self.targetqpos)
			
		if (self.synctime > 0) and (self.connect_eye):
			self.iface.eyetracker_sync(int(round(time.time() * 1000)) - self.synctime)
			self.synctime = 0
			print "   sent sync to eye tracker"

		# PHOTODIODE
		if self.photodiode and (self.execution_mode == self.MONITORING_TASK) and (self.current_state == self.STATE_STIMULUS) and (self.counter_stimulus > self.current_trial.getRt1()): 
			qp.setBrush(self.color_diode)
			qp.drawRect( 500, 500 , 100, 100)


	def exit_function(self):
		print "** Exit procedure"
		print "======= " + repr(self.counter_errors) + " errors ======="
		self.stop_protocol = 1
		if not self.demo:
			self.iface.dump_files(self.filename,self.num_trials_run,self.array_timings,self.array_stimuli)
			self.iface.dump_file_state(self.statefilename,self.bias)
		self.iface.close_eyetracker()
		sys.exit()

	def keyPressEvent(self, event):
		triggersent = 0
		key = event.key()
		if (key == QtCore.Qt.Key_A) or (key == QtCore.Qt.Key_1) or (key == QtCore.Qt.Key_9):	# Key_9
			self.got_scanner_trigger = 1
			self.stop_protocol = 0
			if (self.current_state == self.STATE_STIMULUS) and (not self.action_executed) and (not self.counter_stimulus > self.state_rt_max):
				if (self.execution_mode == self.MOTOR_TASK):
					self.iface.sendtrigger(self.RESP1_LEFT_PRESSED)
					triggersent = 1
					self.action_executed = self.TARGET_LEFT
					self.current_trial.setResp1(self.TARGET_LEFT)
					self.current_trial.setRt1(self.counter_stimulus)

					if (self.counter_stimulus > self.respond_thr):
						if self.badrt_sound_on:
							self.sound.play()
						self.counter_badrt += 1

					
				else:
					self.bad_monitoring_trial = 10
			if (self.current_state == self.STATE_QUESTION) and (self.type2answer_side==11):
				if self.nresp2 == 0:
					self.current_trial.setRt2(self.counter_question)
					self.current_answer = float(int(self.qpos*100))/100
					self.current_trial.setResp2(self.current_answer)
					self.iface.sendtrigger(self.RESP2_LEFT_PRESSED)
					triggersent = 1
				self.nresp2 += 1
				self.time_questionanswer = self.counter_question
			if not triggersent:
				self.iface.sendtrigger(self.KEY_LEFT_PRESSED)
		elif (key == QtCore.Qt.Key_L) or (key == QtCore.Qt.Key_6) or (key == QtCore.Qt.Key_8): # Key_8
			
			self.stop_protocol = 0
			if (self.current_state == self.STATE_STIMULUS) and (not self.action_executed) and (not self.counter_stimulus > self.state_rt_max):# and (not self.counter_stimulus > self.current_trial.getTiming1()):
				if (self.execution_mode == self.MOTOR_TASK):
					self.iface.sendtrigger(self.RESP1_RIGHT_PRESSED)
					triggersent = 1
					self.action_executed = self.TARGET_RIGHT
					self.current_trial.setResp1(self.TARGET_RIGHT)
					self.current_trial.setRt1(self.counter_stimulus)
					if (self.counter_stimulus > self.respond_thr):
						if self.badrt_sound_on:
							self.sound.play()
						self.counter_badrt += 1
				else:
					self.bad_monitoring_trial = 10
			if (self.current_state == self.STATE_QUESTION) and (self.type2answer_side==12):
				if self.nresp2 == 0:
					self.current_trial.setRt2(self.counter_question)
					self.current_answer = float(int(self.qpos*100))/100
					self.current_trial.setResp2(self.current_answer)
					self.iface.sendtrigger(self.RESP2_RIGHT_PRESSED)
					triggersent = 1
				self.nresp2 += 1
				self.time_questionanswer = self.counter_question
			if not triggersent:
				self.iface.sendtrigger(self.KEY_RIGHT_PRESSED)


		elif (key == QtCore.Qt.Key_5):
			# wait for scanner
			self.got_scanner_trigger = 1
			self.stop_protocol = 0

		elif (key == QtCore.Qt.Key_Space):
			self.stop_protocol = not self.stop_protocol	
			print "** pause: " + repr(self.stop_protocol)

		elif (key == QtCore.Qt.Key_F):
			self.full_screen = not self.full_screen
			print "** toggle full screen to " + repr(self.full_screen)
			if (self.full_screen):
				self.showFullScreen()
				self.screen_width = QtGui.QDesktopWidget().screenGeometry().width()
				self.screen_height = QtGui.QDesktopWidget().screenGeometry().height()
			else:
				self.showNormal()
				self.screen_width = self.config.getint("interface", "screen_width")
				self.screen_height = self.config.getint("interface", "screen_height")
			print "** screen ", self.screen_width, ", ", self.screen_height
		elif (key == QtCore.Qt.Key_Escape) or  (key == QtCore.Qt.Key_Q):	
			self.exit_function()
		
def main():
	
	app = QtGui.QApplication(sys.argv)
	ex = EEG_FMRI()
	sys.exit(app.exec_())

if __name__ == '__main__':
    main()