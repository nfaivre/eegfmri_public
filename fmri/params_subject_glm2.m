%-----------------------------------------------------------------------
% Job saved on 29-May-2017 13:23:41 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6685)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
function [matlabbatch] = params_subject_glm1(params)

fprintf('params.outdir is %s\n',params.outdir);
matlabbatch{1}.spm.stats.fmri_spec.dir = {params.outdir};
matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = params.tr;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = params.microtime;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = params.microtime/2;
%%
%% get functional images for each session
%
niftidir = [params.dicomdir '/' params.subject '/ses-001/func/' ...
    params.subject '_ses-001_task-main_run-00*.nii'];
fprintf('Looking for Nifti files in %s\n',niftidir);
d1 = dir(niftidir);
figure(100); clf;
irun = 0;

% load eeg-decoded confidence for active
eegact = load([params.eegdecodingdir '/' params.subject '_act_eegdecode.mat']);
% load eeg-decoded confidence for observation
eegobs = load([params.eegdecodingdir '/' params.subject '_obs_eegdecode.mat']);

for f1=1:length(d1)
    
    
    file = [d1(f1).folder '/' d1(f1).name];
    dir_orig = d1(f1).folder;%(1:end-8);
    % count the number of volumes
    hdr = spm_data_hdr_read(file);
    
    split = strsplit(d1(f1).name,'_run-00');
    run = str2double(split{2}(1));
    
    fprintf('[run %d]',run);

        irun = irun+1;
        % preallocate
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).scans = cell(length(hdr),1);
        
        % add files
        for f2 = 1:min(length(hdr),387)
            matlabbatch{1}.spm.stats.fmri_spec.sess(irun).scans{f2} = [file ',' num2str(f2) ];
        end
        
        %% create events
        
        % load event file
        d3 = dir([dir_orig '/' params.subject '_ses-001_task-main_run-00' num2str(run) '_events.tsv']);
        eventfile = [d3(1).folder '/' d3(1).name];
        copyfile(eventfile,[eventfile(1:end-3) '.csv']);
        events = readtable([eventfile(1:end-3) '.csv']);
        delete([eventfile(1:end-3) '.csv']);
        
        % create additional two event vectors for eeg-decoded confidence
        eegconf1 = zeros(size(events.confidence,1),1);
        eegconf2 = zeros(size(events.confidence,1),1);
        matchrt = zeros(size(events.confidence,1),1);
                
        % define early time window
        idx1 = find(eegact.time_windows(:,1)*1e-3 >= params.earlywin(1) & eegact.time_windows(:,1)*1e-3 <= params.earlywin(2));
        % find peak r2 inside that window
        [max1,imax1_] = findpeaks(eegact.r2_reg(idx1));
        [~,best] = max(max1);
        imax1 = imax1_(best);
        % get eeg-decoded confidence at peak r2 for current run
        ipeak_act1 = idx1(1)+imax1-1;
        eegvals_act1 = eegact.pred_reg_train(ipeak_act1,eegact.trialrun == run);
        
        % define late time window
        idx2 = find(eegact.time_windows(:,1)*1e-3 >= params.latewin(1) & eegact.time_windows(:,1)*1e-3 <= params.latewin(2));
        % find peak r2 inside that window
        [max2,imax2_] = findpeaks(eegact.r2_reg(idx2));
        [~,best] = max(max2);
        imax2 = imax2_(best);
        % get eeg-decoded confidence at peak r2 for current run
        ipeak_act2 = idx2(1)+imax2-1;
        eegvals_act2 = eegact.pred_reg_train(ipeak_act2,eegact.trialrun == run);
        
        % assign 
        if  strcmp(params.subject,'sub-19')
            eegconf1(eegact.trialid(eegact.trialrun==run)-(run-2)*48) = eegvals_act1;
            eegconf2(eegact.trialid(eegact.trialrun==run)-(run-2)*48) = eegvals_act2;
            matchrt(eegact.trialid(eegact.trialrun==run)-(run-2)*48) = eegact.rt(eegact.trialrun == run);
        else
            eegconf1(eegact.trialid(eegact.trialrun==run)-(run-1)*48) = eegvals_act1;
            eegconf2(eegact.trialid(eegact.trialrun==run)-(run-1)*48) = eegvals_act2;
            matchrt(eegact.trialid(eegact.trialrun==run)-(run-1)*48) = eegact.rt(eegact.trialrun == run);
        end
              
        % find peak r2 inside early window
        [max1,imax1_] = findpeaks(eegobs.r2_reg(idx1));
        [~,best] = max(max1);
        imax1 = imax1_(best);
        % get eeg-decoded confidence at peak r2 for current run
        ipeak_obs1 = idx1(1)+imax1-1;
        eegvals_obs1 = eegobs.pred_reg_train(ipeak_obs1,eegobs.trialrun == run);
        
        % find peak r2 inside late window
        [max2,imax2_] = findpeaks(eegobs.r2_reg(idx2));
        [~,best] = max(max2);
        imax2 = imax2_(best);
        % get eeg-decoded confidence at peak r2 for current run
        ipeak_obs2 = idx2(1)+imax2-1;
        eegvals_obs2 = eegobs.pred_reg_train(ipeak_obs2,eegobs.trialrun == run);
        
        % assign
        if strcmp(params.subject,'sub-19')
            eegconf1(eegobs.trialid(eegobs.trialrun==run)-(run-2)*48) = eegvals_obs1;
            eegconf2(eegobs.trialid(eegobs.trialrun==run)-(run-2)*48) = eegvals_obs2;
            matchrt(eegobs.trialid(eegobs.trialrun==run)-(run-2)*48) = eegobs.rt(eegobs.trialrun == run);
        else
            eegconf1(eegobs.trialid(eegobs.trialrun==run)-(run-1)*48) = eegvals_obs1;
            eegconf2(eegobs.trialid(eegobs.trialrun==run)-(run-1)*48) = eegvals_obs2;
            matchrt(eegobs.trialid(eegobs.trialrun==run)-(run-1)*48) = eegobs.rt(eegobs.trialrun == run);
        end
        
        % if confidence is zero for active => bad trial
        badact = find((eegconf1==0 | eegconf2==0) & strcmp(events.trial_type,'act'));
        % update events
        for i=1:length(badact)
            events.trial_type{badact(i)} = 'act-bad';
        end
        % same for observation
        badobs = find((eegconf1==0 | eegconf2==0) & strcmp(events.trial_type,'obs'));
        for i=1:length(badobs)
            events.trial_type{badobs(i)} = 'obs-bad';
        end
        
        % cound conditions (depends on number of bad trials)
        name = unique(events.trial_type);
        nreg = length(name);
        
        % define cells
        durations = cell(1,nreg);
        onsets = cell(1,nreg);
        names = {'act','obs'};
        pmod = [];
        
        % find indices of active condition
        act_trials = strcmp(events.trial_type,'act');
        % assign onsets
        onsets{1} = events.onset(act_trials);
        % assign durations
        durations{1} = events.duration(act_trials);
        
        % build parametric regressors
        % first is eeg-decoded confidence for early window
        pmod(1).name{1} = 'eegconf1-act';
        pmod(1).param{1} = eegconf1(act_trials);
        pmod(1).poly{1} = 1;
        % second is eeg-decoded confidence for late window
        pmod(1).name{2} = 'eegconf2-act';
        pmod(1).param{2} = eegconf2(act_trials);
        pmod(1).poly{2} = 1;
        % third is response time
        pmod(1).name{3} = 'rt1-act';
        pmod(1).param{3} = events.response_time(act_trials);
        pmod(1).poly{3} = 1;
        % fourth is difficulty: difference in the number of dots
        pmod(1).name{4} = 'dots-act';
        pmod(1).param{4} = events.dots_difference(act_trials);
        pmod(1).poly{4} = 1;
        
        % same for observation
        obs_trials = strcmp(events.trial_type,'obs');
        onsets{2} = events.onset(obs_trials);
        durations{2} = events.duration(obs_trials);
     
        pmod(2).name{1} = 'eegconf1-obs';
        pmod(2).param{1} = eegconf1(obs_trials);
        pmod(2).poly{1} = 1;
        pmod(2).name{2} = 'eegconf2-obs';
        pmod(2).param{2} = eegconf2(obs_trials);
        pmod(2).poly{2} = 1;
        pmod(2).name{3} = 'rt1-obs';
        pmod(2).param{3} = events.response_time(obs_trials);
        pmod(2).poly{3} = 1;
        pmod(2).name{4} = 'dots-obs';
        pmod(2).param{4} = events.dots_difference(obs_trials);
        pmod(2).poly{4} = 1;
        
        % put bad trials in separate regressors
        badact_trials = strcmp(events.trial_type,'act-bad');
        if sum(badact_trials) > 0
            names{3} = 'act-bad';
            onsets{3} =  events.onset(badact_trials);
            durations{3} = events.duration(badact_trials);
       end
         
        badobs_trials = strcmp(events.trial_type,'obs-bad');
        if sum(badobs_trials) > 0
             names{nreg} = 'obs-bad';
            onsets{nreg} =  events.onset(badobs_trials);
            durations{nreg} = events.duration(badobs_trials);
        end
        %%
        if 0;% run <= 6
        figure(100); 
        subplot(3,6,run); hold on;
        plot(1e-3*matchrt(act_trials | badact_trials),'r','LineWidth',2)
        plot(events.response_time(act_trials | badact_trials),'ro','LineWidth',2)
        plot(1e-3*matchrt(obs_trials | badobs_trials),'c','LineWidth',2)
        plot(events.response_time(obs_trials | badobs_trials),'co','LineWidth',2)
        ylim([0,0.6]);
        title(sprintf('RT match (%d)',run));
        %xlabel('Trial id');
        %ylabel('RT');
        eegrt{run,1} = 1e-3*matchrt(act_trials | badact_trials);
        behrt{run,1} = events.response_time(act_trials | badact_trials);
        eegrt{run,2} = 1e-3*matchrt(obs_trials | badobs_trials);
        behrt{run,2} = events.response_time(obs_trials | badobs_trials);
        
        subplot(3,6,6+run); hold on;
        plot(eegact.confidence_values(eegact.trialrun==run),'ro','LineWidth',2)
        plot(eegconf2(act_trials),'r-','LineWidth',2)
        plot(eegobs.confidence_values(eegobs.trialrun==run),'co','LineWidth',2)
        plot(eegconf2(obs_trials),'c-','LineWidth',2)
        title(sprintf('Conf. dec. (%d)',run));
        %xlabel('Trial id');
        %ylabel('Confidence');
        subplot(3,1,3); hold on;
        plot(eegact.time_windows(:,1),eegact.r2_reg,'r','LineWidth',2);
        plot(eegact.time_windows(:,1),eegobs.r2_reg,'c','LineWidth',2);
        plot(eegact.time_windows(ipeak_act1,1),eegact.r2_reg(ipeak_act1),'ko','LineWidth',2);
        plot(eegact.time_windows(ipeak_obs1,1),eegobs.r2_reg(ipeak_obs1),'ko','LineWidth',2);
        plot(eegact.time_windows(ipeak_act2,1),eegact.r2_reg(ipeak_act2),'ko','LineWidth',2);
        plot(eegact.time_windows(ipeak_obs2,1),eegobs.r2_reg(ipeak_obs2),'ko','LineWidth',2);
        plot(eegact.time_windows(min(idx1),1)*[1 1],ylim(),'k');
        plot(eegact.time_windows(max(idx1),1)*[1 1],ylim(),'k');
        plot(eegact.time_windows(min(idx2),1)*[1 1],ylim(),'k');
        plot(eegact.time_windows(max(idx2),1)*[1 1],ylim(),'k');
        title(params.subject);
        xlabel('Time [s]');
        ylabel('R^2');
        pause(0.2);
        end
        
        print(sprintf('plots/%s.eps',params.subject),'-depsc');
        condfile = [params.eventdir '/' params.subject '_ses-001_task-main_run-00' num2str(run) '_events.mat'];
        save(condfile,'names','onsets','durations','pmod');
       
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).multi = {condfile};
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).regress = struct( ...
            'name', {}, 'val', {});

        d4 = dir([dir_orig '/' params.subject '_ses-001_task-main_run-00' num2str(run) '_headmove.txt']);
        filereg = [d4(1).folder '/' d4(1).name];
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).multi_reg = {filereg};
        matlabbatch{1}.spm.stats.fmri_spec.sess(irun).hpf = 128;        
        
end

fprintf('\n');

matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = params.deriv;
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
matlabbatch{1}.spm.stats.fmri_spec.mask = {''};
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';


matlabbatch{2}.spm.stats.review.spmmat(1) = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.review.display.matrix = 1;
matlabbatch{2}.spm.stats.review.print = 'ps';
matlabbatch{3}.spm.stats.fmri_est.spmmat(1) = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{3}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{3}.spm.stats.fmri_est.method.Classical = 1;
matlabbatch{4}.spm.stats.con.spmmat(1) = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));

%%
w = cell(1,16);
contrasts = cell(1,16);
%      act c1 c2 rt d obs c1 c2 rt d
w{1} = [1  0  0  0  0  0  0  0  0  0 ]; contrasts{1} = 'act > 0';
w{2} = [0  0  0  0  0  1  0  0  0  0 ]; contrasts{2} = 'obs > 0';
w{3} = [1  0  0  0  0 -1  0  0  0  0 ]; contrasts{3} = 'act > obs';
w{4} = [-1 0  0  0  0  1  0  0  0  0 ]; contrasts{4} = 'obs > act';
w{5} = [0  1  0  0  0  0  0  0  0  0 ]; contrasts{5} = 'eegconf1 act > 0';
w{6} = [0  0  0  0  0  0  1  0  0  0 ]; contrasts{6} = 'eegconf1 obs > 0';
w{7} = [0  0  1  0  0  0  0  0  0  0 ]; contrasts{7} = 'eegconf2 act > 0';
w{8} = [0  0  0  0  0  0  0  1  0  0 ]; contrasts{8} = 'eegconf2 obs > 0';
w{9} = [0  0  0  1  0  0  0  0  0  0 ]; contrasts{9} = 'rt act > 0';
w{10} =[0  0  0  0  0  0  0  0  1  0 ]; contrasts{10} = 'rt obs > 0';
w{11} =[0  0  0  0  1  0  0  0  0  0 ]; contrasts{11} = 'dots act > 0';
w{12} =[0  0  0  0  0  0  0  0  0  1 ]; contrasts{12} = 'dots obs > 0';
w{13} =[0  1  0  0  0  0 -1  0  0  0 ]; contrasts{13} = 'eegconf1 act > obs';
w{14} =[0  0  1  0  0  0  0 -1  0  0 ]; contrasts{14} = 'eegconf2 act > obs';
w{15} =[0  0  0  1  0  0  0  0 -1  0 ]; contrasts{15} = 'rt act > obs';
w{16} =[0  0  0  0  1  0  0  0  0  -1]; contrasts{16} = 'dots act > obs';

for i=1:length(w)
    ctype{i} = 'tcon';
end
for c = 1:length(w)
    weights = upsample(w{c}.',length(params.deriv)+1).';
    matlabbatch{4}.spm.stats.con.consess{c}.(ctype{c}).weights = weights;
    matlabbatch{4}.spm.stats.con.consess{c}.(ctype{c}).sessrep = 'replsc';
    matlabbatch{4}.spm.stats.con.consess{c}.(ctype{c}).name = contrasts{c};
end
matlabbatch{4}.spm.stats.con.delete = 1;
