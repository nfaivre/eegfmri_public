function batch_subject_glm3(subject)
% the subject's code
params.subject = subject;%'z2';
% the directory containing the raw fMRI data (all sessions)
% the directory containing the SPM12 toolbox

params.spmdir = '/Scratch/meaperei/toolboxes/spm12/';
params.bvatoolbox = '/Scratch/meaperei/toolboxes/bva/';
params.datadir = '/Scratch/meaperei/data/';
params.outputdir = '/Scratch/meaperei/eegfmri/results/glm_level1/glm3/';
params.timingdir = '/Scratch/meaperei/eegfmri/results/glm_timings/';
params.eegmatdir = '/Scratch/meaperei/eegfmri/analysis/eeg/';

params.pmod = 'conf'; % 'nopmod','conf','absconf','erp'
params.trial = 'none'; % 'none','rt1','resp','rt2','scale'
% the TR
if strcmp(params.subject,'z2') || strcmp(params.subject,'p2')
    params.tr = 1.3;
else
    params.tr = 1.28;
end
% params for timing (EEG)
% the directory containing the toolbox to load BrainProduct files
% the duration of the scale
params.t_scale = 6.5;
params.t_resp = 1.5;



params.microtime = 16;
params.deriv = [1 1];

addpath(params.bvatoolbox);
addpath(params.spmdir);


%%
%spm('defaults','fmri');
%spm_jobman('initcfg');

matlabbatch =  params_subject_glm3(params);
spm_jobman('run',matlabbatch);

outdir = [params.outputdir '/' params.subject '_' params.pmod '_' params.trial];
alldir = [params.outputdir '/' params.pmod '_' params.trial];
if ~exist(alldir,'dir')
    mkdir(alldir);
end

d = dir([outdir '/con_*']);
for i=1:length(d)
    fprintf('Copying %s to %s\n',[outdir '/' d(i).name],[alldir '/' params.subject '_' d(i).name]);
    copyfile([outdir '/' d(i).name],[alldir '/' params.subject '_' d(i).name]);
end
d = dir([outdir '/spmT_*']);
for i=1:length(d)
    fprintf('Copying %s to %s\n',[outdir '/'  d(i).name],[alldir '/' params.subject '_' d(i).name]);
    copyfile([outdir '/' d(i).name],[alldir '/' params.subject '_' d(i).name]);
end
end