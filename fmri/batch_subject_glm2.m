function batch_subject_glm2(subject)

config;

% the subject's code
opts.subject = subject;

addpath(opts.spmdir);
%spm fmri

% directories
opts.outdir = [opts.glm2dir '/' opts.subject '/'];
opts.alldir = [opts.glm2dir '/all/' ];
if ~exist(opts.alldir,'dir')
    mkdir(opts.alldir);
end
if ~exist(opts.outdir,'dir')
    mkdir(opts.outdir);
end

% run glm analysis
matlabbatch =  params_subject_glm2(opts);
if 1
spm_jobman('run',matlabbatch);

% make a copy of the contrasts files in alldir so it's easy to make level2 glms
d = dir([opts.outdir '/con_*']);
for i=1:length(d)
    fprintf('Copying %s to %s\n',[opts.outdir '/' d(i).name],[opts.alldir '/' opts.subject '_' d(i).name]);
    copyfile([opts.outdir '/' d(i).name],[opts.alldir '/' opts.subject '_' d(i).name]);
end
end
end