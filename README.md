**Analysis scripts from EEG-fMRI study by Pereira, Faivre, Iturrate et al., 2018**  

https://www.biorxiv.org/content/10.1101/496877v1  

https://osf.io/a5qmv/  

*Content:* 

behav: analysis of behavioral results using behavioral_analysis.Rmd and simulated results using simul_analysis.Rmd (see race)  

eeg: univariate analysis of preprocessed eeg data using R multivariate analysis to decode confidence and fuse with fmri data (matlab)  

fmri: first, second level GLM analysis and EEG-fMRI fusion  

misc: contains the list of trials to exclude based on EEG and fMRI criteria  

model: race models of response times and confidence  

protocol: presentation code for the experimental paradigm
