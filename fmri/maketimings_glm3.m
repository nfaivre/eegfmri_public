function ncond = maketimings_glm3(params,run, bt, tr1, tr2)
addpath toolboxes
eegfile = dir([params.datadir params.subject '/eeg/' params.subject '_' run '*.vhdr']);
[Fs,~,eeghdr] = bva_readheader([params.datadir params.subject '/eeg/' eegfile(1).name]);
eegmarkerfile = eegfile(1).name; eegmarkerfile(end+(-4:0)) = '.vmrk';
marker = bva_readmarker2([params.datadir params.subject '/eeg/' eegmarkerfile]);
bad = isnan(marker(1,:));
marker(:,bad) = [];

trtime = marker(2,marker(1,:)==1128)./Fs;
df = find(diff(trtime) < params.tr/2);
trtime(df+1) = [];
times = marker(2,:)./Fs;
trig = mod(marker(1,:),128);
nscans = length(trtime);
tr = mode(diff(trtime));

% index of events
istims = find(ismember(trig,11:12));
bad = find(diff(times(istims)) < 2);
istims(bad+1) = [];
iresp = find(ismember(trig,7:8));
bad = find(diff(times(iresp)) < 2);
iresp(bad+1) = [];
ifb = find(ismember(trig,17:18));
bad = find(diff(times(ifb)) < 2);
ifb(bad+1) = [];
iresp2 = find(ismember(trig,27:28));
bad = find(diff(times(iresp2)) < 2);
iresp2(bad+1) = [];
pauses = find(ismember(trig,44));
bad = find(diff(times(pauses)) < 2);
pauses(bad+1) = [];



%fprintf(' + Adding %s (%d volumes / %d triggers (first at %.1f s, TR=%.2f)\n',filename,nscans,length(trtime),trtime(1),tr);
% load behav
behavfile = dir([params.datadir params.subject '/behav/' params.subject '_' run '*_timings.txt']);
behav = dlmread([params.datadir params.subject '/behav/' behavfile(1).name]);

nstim = 48;
if length(istims) < nstim
    warning([num2str(length(istims)) ' triggers, trimming, please check']);
    nstim = length(istims);
end
% if nstim > 48
%     warning([num2str(nstim) ' stimuli, trimming to 48, please check']);
%     nstim = 48;
%     istims = istims(1:nstim);
%     iresp = iresp(1:floor(nstim/2));
%     ifb = ifb(1:floor(nstim/2));
%     iresp2 = iresp2(1:nstim);
% end

bCond = behav(1:nstim,2);
bSide = behav(1:nstim,3);
bDelta = behav(1:nstim,4);
bType1 = behav(1:nstim,5);
bType2 = behav(1:nstim,6);
bErr = behav(1:nstim,7);
bTrialOnset = behav(1:nstim,8)*1e-3;
tFix = behav(1:nstim,9)*1e-3;
tRT1 = behav(1:nstim,10)*1e-3;
tRT2 = behav(1:nstim,12)*1e-3;
% if size(behav,2) >= 18
%     badness = behav(1:nstim,18);
% end
%time_start = times(find(trig==0,1,'first'));
times = times - trtime(1);
% 
% rt1 = zeros(1,nstim);
% rt2 = zeros(1,nstim);
% for r=1:nstim
%     if bCond(r)==0
%         idx = iresp;
%     elseif bCond(r)==1
%         idx = ifb;
%     end
%     rt1(r) = min(abs(times(idx) - times(istims(r))));
%     rt2(r) = min(abs(times(iresp2) - times(istims(r))));
% end
% bid = find(strcmp(bt.allsubjects,params.subject));
% allbad = bt.allbadtrials{bid};
% r = str2double(run(4:end));
% badTr = allbad((r-1)*48+(1:48)) > 0;

%badTr = ~ismember(bType1,[11,12]) | bType2 == -1;
%badTr((rt1>0.5) | (rt2>6.5)) = 1;

%rt1(rt1>0.5) = NaN;
%rt2(rt2>6.5) = NaN;

r = str2double(run(4:end));
eeg = zeros(48,1);
sel1 = tr1.trialrun==r;
seltr1 = mod(tr1.trialid(sel1)-1,48)+1;
eeg(seltr1) = tr1.pred(sel1);

sel2 = tr2.trialrun==r;
seltr2 = mod(tr2.trialid(sel2)-1,48)+1;
eeg(seltr2) = tr2.pred(sel2);

badTr = eeg==0;

id1 = (bCond==0) & (badTr==0);
id2 = (bCond==1) & (badTr==0);

id3 = (bCond==0) & (badTr==1);
id4 = (bCond==1) & (badTr==1);
confidence = 2*(bType2-0.5);
delta = bDelta;
confneg_mot = sum(id1 & (confidence < -0.0));
confneg_mon = sum(id2 & (confidence < -0.0));
%fprintf('Run %s: error perceived: %f (%d)/ %f (%d), delta: [%d,%d] bad: %d / %d',run, ...
%    confneg_mot./sum((bCond==0) & (badTr==0)),confneg_mot, ...
%    confneg_mon./sum((bCond==1) & (badTr==0)),confneg_mon, ...
%    min(bDelta),max(bDelta),sum((bCond==0) &(badTr==1)), ...
%    sum((bCond==1) &(badTr==1)));

confidence = abs(confidence);
rt1 = tRT1;

%%
clear names onsets durations 

names = {'mot','mon'};
onsets{1} = times(istims(id1));
onsets{2} = times(istims(id2));

if sum(id3)
    names{length(onsets)+1} = 'bad_mot';
    onsets{length(onsets)+1} = times(istims(id3));
end
if sum(id4)
    names{length(onsets)+1} = 'bad_mon';
    onsets{length(onsets)+1} = times(istims(id4));
end

clear pmod pmod_unsig pmod_abs

pmod(1).name{1} = 'conf_mot';
pmod(1).param{1} = confidence(id1);
pmod(1).poly{1} = 1;
pmod(1).name{2} = 'cert_mot';
pmod(1).param{2} = (confidence(id1)-0.5).^2;
pmod(1).poly{2} = 1;
pmod(1).name{3} = 'rt1_mot';
pmod(1).param{3} = rt1(id1);
pmod(1).poly{3} = 1;

pmod(2).name{1} = 'conf_mon';
pmod(2).param{1} = confidence(id2);
pmod(2).poly{1} = 1;
pmod(2).name{2} = 'cert_mon';
pmod(2).param{2} = (confidence(id2)-0.5).^2;
pmod(2).poly{2} = 1;
pmod(2).name{3} = 'rt1_mon';
pmod(2).param{3} = rt1(id2);
pmod(2).poly{3} = 1;

% ============ DIRAC ============
durations = cell(1,2);
durations{1} = ones(1,sum(id1)).*0;
durations{2} = ones(1,sum(id2)).*0;
if sum(id3)
    durations{length(durations)+1} = ones(1,sum(id3)).*0;
end
if sum(id4)
    durations{length(durations)+1} = ones(1,sum(id4)).*0;
end

file = sprintf('%s/%s_%s_none_conf_conditions.mat',params.timingdir,params.subject,run);
%disp(['    - saving ' file]);
save(file,'names','onsets','durations','pmod');

% ============ RT1 ============
durations = cell(1,2);
durations{1} = ones(1,sum(id1)).*rt1((id1));
durations{2} = ones(1,sum(id2)).*rt1((id2));
if sum(id3)
    durations{length(durations)+1} = ones(1,sum(id3)).*0.5;
end
if sum(id4)
    durations{length(durations)+1} = ones(1,sum(id4)).*0.5;
end

file = sprintf('%s/%s_%s_rt1_conf_conditions.mat',params.timingdir,params.subject,run);
%disp(['    - saving ' file]);
save(file,'names','onsets','durations','pmod');


% ============ TYPE I TIME ============
durations = cell(1,2);
durations{1} = ones(1,sum(id1)).*(params.t_resp);
durations{2} = ones(1,sum(id2)).*(params.t_resp);
if sum(id3)
    durations{length(durations)+1} = ones(1,sum(id3)).*(params.t_resp);
end
if sum(id4)
    durations{length(durations)+1} = ones(1,sum(id4)).*(params.t_resp);
end

file = sprintf('%s/%s_%s_resp_conf_conditions.mat',params.timingdir,params.subject,run);
%disp(['    - saving ' file]);
save(file,'names','onsets','durations','pmod');

% % ============ RT2 ============
% durations = cell(1,2);
% durations{1} = ones(1,sum(id1)).*rt2((id1));
% durations{2} = ones(1,sum(id2)).*rt2((id2));
% if sum(id3)
%     durations{length(durations)+1} = ones(1,sum(id3)).*rt2((id3));
% end
% if sum(id4)
%     durations{length(durations)+1} = ones(1,sum(id4)).*rt2((id4));
% end
% 
% file = sprintf('%s/%s_%s_rt2_conf_conditions.mat',params.timingdir,params.subject,run);
% %disp(['    - saving ' file]);
% save(file,'names','onsets','durations','pmod');

% ============ SCALE ============
durations = cell(1,2);
durations{1} = ones(1,sum(id1)).*(params.t_resp+params.t_scale);
durations{2} = ones(1,sum(id2)).*(params.t_resp+params.t_scale);
if sum(id3)
    durations{length(durations)+1} = ones(1,sum(id3)).*(params.t_resp+params.t_scale);
end
if sum(id4)
    durations{length(durations)+1} = ones(1,sum(id4)).*(params.t_resp+params.t_scale);
end

file = sprintf('%s/%s_%s_scale_conf_conditions.mat',params.timingdir,params.subject,run);
%disp(['    - saving ' file]);
save(file,'names','onsets','durations','pmod');


%%

ncond = [sum(id1) sum(id2)];