function checkmove( subject )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
params.datadir = 'Y:/Data/Exp/';

d1 = dir([params.datadir subject '/fmri/1_*_Run*_*s4_*']);

move = [];

for f1=1:length(d1);
    % get all files in the session directory
    [tm,m,hm] = HeadMotion([params.datadir subject '/fmri/' d1(f1).name '/'],0);

    
    d2 = dir([params.datadir subject '/fmri/' d1(f1).name '/rp*.txt']);
    
    move_ = dlmread([params.datadir subject '/fmri/' d1(f1).name '/' d2(1).name]);
    move = [move ; move_];
end
figure(100);clf;
subplot(211); hold on;
plot(move(:,1:3));
plot(xlim(),[1 1],'k');
plot(xlim(),-[1 1],'k');
ylim([-3,3]);

subplot(212); hold on;
plot(move(:,3+(1:3)));
plot(xlim(),[1 1]*pi/180,'k');
plot(xlim(),-[1 1]*pi/180,'k');
ylim([-0.05,0.05]);

