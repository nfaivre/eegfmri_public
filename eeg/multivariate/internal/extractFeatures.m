function [ outputMatrix ] = extractFeatures( EEG, epochs, timeWindow, channels, downsampling, labels, confidence_values )
   
    idx_time(1) = find(EEG.times>=timeWindow(1),1);
    idx_time(2) = find(EEG.times>=timeWindow(2),1);
    
    time_vector = idx_time(1) : downsampling : idx_time(2);
    
    outputMatrix.features = zeros(size(epochs, 3), length(time_vector) * length(channels));
    outputMatrix.labels = NaN.*zeros(1,size(epochs, 3));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Calculating features for each type of event
        for ep=1:size(epochs,3)
            example = epochs(:,:,ep);
            example = example(channels, time_vector);        
            example = example(:);
        
            outputMatrix.labels(ep) = labels(ep);
            outputMatrix.conf(ep) = confidence_values(ep);
            outputMatrix.features(ep,:) = example';
            
        end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
end

