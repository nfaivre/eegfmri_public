'''
Created on Mar 14, 2014

@author: mpereira
'''
import random
import numpy as np
from trial import Trial

class Trials():
	def __init__(self,n,fix_intval,stim_dur,fb_dur,t1_dur,t2_dur,iti_dur):
		self.TARGET_LEFT = 11
		self.TARGET_RIGHT = 12

		trial_type = [self.TARGET_LEFT]*(n/2) + [self.TARGET_RIGHT]*(n/2)
		unif = map(float,np.linspace(0,1,n/4))
		print "len(unif)=" + repr(len(unif))
		qinit = unif*4
		qdir = [-1,1]*(n/4) + [-1,1]*(n/4)
		qinit = qinit[0:n]
		qdir = qdir[0:n]
		
		fix = map(int,np.linspace(fix_intval[0], fix_intval[1]+1, n))
		print "n=" + repr(n) + ", " + repr(len(trial_type)) + ", " + repr(len(fix)) + ", " + repr(len(qdir)) + ", " + repr(len(qinit)) 
		combined = zip(trial_type,qinit,qdir,fix)
		random.shuffle(combined)
		trial_type[:],qinit[:],qdir[:],fix[:] = zip(*combined)

		print "trials (" + repr(n) + ") type balance: " + repr(np.mean(np.array(trial_type))-11) 
		#print "shuffle trials: " + repr(trial_type)
		#print "shuffle qinit: " + repr(qinit)
		#print "shuffle qdir: " + repr(qdir)
		self.trials = []
		print "n=" + repr(n) + ", " + repr(len(trial_type)) + ", " + repr(len(fix)) + ", " + repr(len(qdir)) + ", " + repr(len(qinit)) 
		for i in range(0,n):
			tr = Trial(trial_type[i],fix[i],stim_dur,fb_dur,t1_dur,t2_dur,iti_dur,qdir[i],qinit[i])
			self.trials.append(tr)
	
	def debugTrialBalance(self):
		a = 0
		n = 0
		l = list()
		for tr in self.trials:
			a += tr.getSide() - 11
			n += 1
			l.append(tr.getSide())
		print " ?? trial balance: " + repr(float(a)/float(n))
		print " ?? " + repr(l)

	def getTrial(self,trid=-1):
		if trid < 0:
			ret = self.trials[len(self.trials)-1]
		else:
			ret = self.trials[trid]
			#print "   retrieving trial " + repr(trid)  + " of " + repr(self.trials)
		return ret

	def addTrial(self,tr):
		self.trials.append(tr)

	def shuffle(self):
		random.shuffle(self.trials)

	def getSubset(self,trids):
		sublist = []
		for trid in trids:
			tr = self.getTrial(trid)
			sublist.append(tr)
		random.shuffle(sublist)
		return sublist

	def getMonitoring(self):
		sublist = []
		i = 0
		for trid in self.trials:
			#print repr(i) + ") state = " + repr(trid.getState())
			if trid.getState() == 0:
				sublist.append(trid)
			i += 1
		random.shuffle(sublist)
		return sublist