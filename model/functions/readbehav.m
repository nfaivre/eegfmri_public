%========================================================
%   readbehav.m reads behavioral data
%
%   Pereira, Faivre, Iturrate et al., 2019,
%   Disentangling the origins of confidence in speeded
%      perceptual judgments through multimodal imaging
%
%   preregistration: https://osf.io/a5qmv/
%   gitlab: https://gitlab.com/meta-eegfmri/
%
%   Michael Pereira <michael.pereira@epfl.ch>
%   25/09/2018
%========================================================
function [ data ] = readbehav( files, data_path, s)
data_ = [];
cond = {'act','obs'};
tr = [0 0];
for f=1:length(files)
    copyfile([data_path '/' files(f).name],'tmp.csv');
    tbl = readtable('tmp.csv');
    for c=1:length(cond)
        sel = strncmp(tbl.trial_type,cond{c},3);
        n = sum(sel);
        data{s,c}.side(tr(c)+(1:n)) = strcmp(tbl.stim_side(sel),'right');
        data{s,c}.cor(tr(c)+(1:n)) = strcmp(tbl.choice_accuracy(sel),'cor');
        data{s,c}.rt(tr(c)+(1:n)) = tbl.response_time(sel);
        data{s,c}.conf(tr(c)+(1:n)) = tbl.confidence(sel);
        tr(c) = tr(c)+n;
    end
end
end

