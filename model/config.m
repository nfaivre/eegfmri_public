%========================================================
%   Configuration
%   
%   Pereira, Faivre, Iturrate et al., 2019, 
%   Disentangling the origins of confidence in speeded 
%      perceptual judgments through multimodal imaging
%
%   preregistration: https://osf.io/a5qmv/  
%   gitlab: https://gitlab.com/meta-eegfmri/  
%      
%   Michael Pereira <michael.pereira@epfl.ch>
%   25/09/2018
%========================================================
opts = struct();

% The directory containing the behavioral files 
%opts.data_path = '/Users/michaelpereira/Dropbox/eegfmri/data/behav/subjects/';
opts.data_path = '/Volumes/MichaelData/data/eegfmri/';


% The list of subjects to fit
%opts.subjects =  {'s2','s4','s7','s8','s9','s10','s11','s12','s13','s14','s15','s16','s17','s18','s19','s20','s21','s22','s24','s26'};
opts.subjects =  {'sub-02','sub-04','sub-07','sub-08','sub-09','sub-10','sub-11','sub-12','sub-13','sub-14','sub-15','sub-16','sub-17','sub-18','sub-19','sub-20','sub-21','sub-22','sub-24','sub-26'};

% Set a  name to the fit
opts.type = 'zvar'; 

% Either 'dv' or 'dv_urg', whether we want to use the standard decision
% variable (d.v.) or the d.v. + urgency signal (if the later is fitted)
opts.dvtype = 'dv';

% If 'eeg', then the timing of the readout is taken from the eeg decoding times. 
% The file '../misc/eegdecode.csv produced by the multivariate eeg analysis scripts is 
% loaded for that purpose. Set to 'group' to use a fixed value for every
% subject. 
opts.readout = 'eeg';

% Time between decision and motor output (Van den Berg et al., 2016).
opts.motordelay = 0.08;

% Since it was difficult to fit the decay value, we used 10 different seed
% values
%opts.decayinit = 0.1:0.1:1;
opts.zvarinit = 0:0.1:1;

% The number of seconds to simulate after the stimulus (doesn't take
% non-decision time into account so if opts.L=2 and ndtime is fitted to 0.3 will
% simulate RTs up to 2.3 seconds after stimulus.
opts.L = 2;

% The minimum number of seconds to have for post-decisional processes
% opts.L2=1 means that we time-lock 0.5 seconds long epochs, time-locked to
% the decision
opts.L2 = 1;

% Censoring of RTs for the first-order fit (RT + choice), we do not censor RTs that are
% higher than 2 s. 
opts.cens = [0.2,2]; 

% Censoring of RTs for the second-order fit (confidence), here we censor
% RTs > 500 ms. 
opts.cens2 = [0.2,0.5];

% Set time granularity for the random walk
opts.dt = 1e-3; 

% Set number of simulated trials for the first-order fit
opts.N = 1000; 
% Set number of simulated trials for the second-order fit
opts.N2 = 1000; 

% Whether to plot/print fitting information
opts.doplot = 0;
opts.doprint = 1;
opts.optimizationdisplay1 = 'None';
opts.optimizationdisplay2 = 'None';

% Support for RT and confidence histograms
opts.confsupport_hist = linspace(0,1,21);
opts.rtsupport_hist = 0.01+linspace(0,1,51);
